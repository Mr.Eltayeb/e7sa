<?php

namespace App\Application\Requests\Admin\Training;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestTraining extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => "required|integer",
            "name" => "min:4|required",
            "university" => "min:3|required",
            "college" => "min:4|required",
            "specialty" => "",
            "phone" => "min:10|max:10|required",
            "email" => "email",
            "type_training" => "",
            "type_college" => "",
            "departments" => "",
            "competence" => "",
        ];
    }
}
