<?php

namespace App\Application\Requests\Admin\Requestdata;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class UpdateRequestRequestdata extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
            "name" => "min:3|required",
			"type_identity" => "",
			"Identity" => "min:3",
			"occupation" => "required",
			"qualification" => "required",
			"phone" => "max|required",
			"fax" => "",
			"email" => "email",
			"data_usage" => "",
			"company_name" => "required",
			"type_Company" => "",
			"Data_usage_area" => "required",
			"data_details" => "required",
			
        ];
    }
}
