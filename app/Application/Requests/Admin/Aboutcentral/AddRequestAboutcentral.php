<?php

namespace App\Application\Requests\Admin\Aboutcentral;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestAboutcentral extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title.*" => "required",
			"body.*" => "required",
			
        ];
    }
}
