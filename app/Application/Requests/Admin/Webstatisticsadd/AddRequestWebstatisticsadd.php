<?php

namespace App\Application\Requests\Admin\Webstatisticsadd;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestWebstatisticsadd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "webstatistics_id" => "required|integer",
            "title.*" => "required",
            "body.*" => "required",
            "file_ar.*" => "required",
            "file_en.*" => "",
        ];
    }
}
