<?php

namespace App\Application\Requests\Admin\Directory;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestDirectory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "directorycategories_id" => "required|integer",
            "title.*" => "required",
            "body.*" => "required",
            "file.*" => "required",
            "file_en.*" => "",
        ];
    }
}
