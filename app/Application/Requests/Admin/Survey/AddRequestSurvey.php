<?php

namespace App\Application\Requests\Admin\Survey;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestSurvey extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title.*" => "required",
			"answers1.*" => "",
			"answers2.*" => "",
			"answers3.*" => "",
			
        ];
    }
}
