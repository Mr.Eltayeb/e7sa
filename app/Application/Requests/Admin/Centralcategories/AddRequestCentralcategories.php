<?php

namespace App\Application\Requests\Admin\Centralcategories;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestCentralcategories extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title.*" => "required",
            "body.*" => "required",
            "file.*" => "required",
            "file_en.*" => "",
        ];
    }
}
