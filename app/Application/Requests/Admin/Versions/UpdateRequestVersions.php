<?php

namespace App\Application\Requests\Admin\Versions;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class UpdateRequestVersions extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
            "cateversions_id" => "required|integer",
            "title" => "required",
            "body" => "",
            "image.*" => "image",
            "file.*" => "",
            "file_en.*" => "",
            "year.*" => "",
        ];
    }
}
