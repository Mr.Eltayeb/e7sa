<?php

namespace App\Application\Requests\Website\User;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestUser
{

    public function rules()
    {
        $id = Route::input('id');
        return [
            'name' => 'required|min:4|max:40',
            'lname' => 'required|min:4|max:40',
            'address' => 'required|min:4',
            'phone' => 'required|min:10|max:20',
            'email' => 'email|unique:users',
            'password' => 'required'
        ];
    }
}
