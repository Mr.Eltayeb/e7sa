<?php

namespace App\Application\Requests\Website\User;


class ApiAddRequestUser
{

    public function rules()
    {
        return [
            'name' => 'required|min:4|max:40',
            'lname' => 'required|min:4|max:40',
            'address' => 'required|min:4',
            'phone' => 'required|min:10|max:20',
            'email' => 'email|unique:users',
            'password' => 'required'
        ];
    }
}
