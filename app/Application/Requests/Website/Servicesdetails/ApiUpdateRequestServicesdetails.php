<?php

namespace App\Application\Requests\Website\Servicesdetails;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestServicesdetails
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "requiredimage",
			"body.*" => "required",
			
        ];
    }
}
