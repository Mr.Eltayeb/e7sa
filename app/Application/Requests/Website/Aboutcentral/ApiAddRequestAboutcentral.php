<?php

namespace App\Application\Requests\Website\Aboutcentral;


class ApiAddRequestAboutcentral
{
    public function rules()
    {
        return [
            "title.*" => "required",
			"body.*" => "required",
			
        ];
    }
}
