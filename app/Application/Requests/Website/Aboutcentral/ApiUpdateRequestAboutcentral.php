<?php

namespace App\Application\Requests\Website\Aboutcentral;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestAboutcentral
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "required",
			"body.*" => "required",
			
        ];
    }
}
