<?php

namespace App\Application\Requests\Website\Fullnews;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestFullnews
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "requiredimage",
			"body.*" => "required",
			
        ];
    }
}
