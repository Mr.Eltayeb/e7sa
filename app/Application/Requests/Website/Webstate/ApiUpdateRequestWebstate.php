<?php

namespace App\Application\Requests\Website\Webstate;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestWebstate
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "requiredimage.*",
			"body.*" => "required",
			
        ];
    }
}
