<?php

namespace App\Application\Requests\Website\Webproject;


class ApiAddRequestWebproject
{
    public function rules()
    {
        return [
            "title.*" => "required",
			"body.*" => "required",
			
        ];
    }
}
