<?php

namespace App\Application\Requests\Website\Webproject;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestWebproject
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "required",
			"body.*" => "required",
			
        ];
    }
}
