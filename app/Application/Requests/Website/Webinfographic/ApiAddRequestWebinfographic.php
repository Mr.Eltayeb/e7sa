<?php
 namespace App\Application\Requests\Website\Webinfographic;
  class ApiAddRequestWebinfographic
{
    public function rules()
    {
        return [
        	"webcategorie_id" => "required|integer",
            "title.*" => "min:3|max:35|required",
   "image.*" => "required|image",
            ];
    }
}
