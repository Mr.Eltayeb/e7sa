<?php
 namespace App\Application\Requests\Website\Webinfographic;
 use Illuminate\Foundation\Http\FormRequest;
 class AddRequestWebinfographic extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        	"webcategorie_id" => "required|integer",
            "title.*" => "min:3|max:35|required",
   "image.*" => "required|image",
            ];
    }
}
