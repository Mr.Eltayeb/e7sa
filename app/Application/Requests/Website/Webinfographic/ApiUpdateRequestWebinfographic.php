<?php
 namespace App\Application\Requests\Website\Webinfographic;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestWebinfographic
{
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"webcategorie_id" => "required|integer",
            "title.*" => "min:3|max:35|required",
   "image.*" => "required|image",
            ];
    }
}
