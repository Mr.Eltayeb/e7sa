<?php

namespace App\Application\Requests\Website\Cateversions;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestCateversions
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "required",
			
        ];
    }
}
