<?php
 namespace App\Application\Requests\Website\Directory;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestDirectory
{
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"directorycategories_id" => "required|integer",
            "title.*" => "requiredfile.*",
   "file_en.*" => "",
            ];
    }
}
