<?php
 namespace App\Application\Requests\Website\Directory;
 use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;
 class UpdateRequestDirectory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"directorycategories_id" => "required|integer",
            "title.*" => "required",
   "file.*" => "required",
   "file_en.*" => "",
            ];
    }
}
