<?php

namespace App\Application\Requests\Website\Webcategorie;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestWebcategorie
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "min:3|max:35|required",
			
        ];
    }
}
