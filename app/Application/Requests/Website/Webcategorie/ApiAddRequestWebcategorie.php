<?php

namespace App\Application\Requests\Website\Webcategorie;


class ApiAddRequestWebcategorie
{
    public function rules()
    {
        return [
            "title.*" => "min:3|max:35|required",
			
        ];
    }
}
