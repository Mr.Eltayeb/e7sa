<?php

namespace App\Application\Requests\Website\Elibrary;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestElibrary
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "requiredimage.*",
			"file_en" => "nullable",
			
        ];
    }
}
