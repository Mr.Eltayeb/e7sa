<?php

namespace App\Application\Requests\Website\Elibrary;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestElibrary extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title.*" => "required",
			"image.*" => "required|image",
			"file" => "required",
			"file_en" => "nullable",
			
        ];
    }
}
