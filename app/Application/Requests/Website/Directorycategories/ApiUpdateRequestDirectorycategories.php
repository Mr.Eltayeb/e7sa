<?php
 namespace App\Application\Requests\Website\Directorycategories;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestDirectorycategories
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "required",
            ];
    }
}
