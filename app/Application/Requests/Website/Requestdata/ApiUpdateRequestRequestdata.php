<?php

namespace App\Application\Requests\Website\Requestdata;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestRequestdata
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "name" => "min:3|requiredtype:identity",
			"data_details" => "required",
			
        ];
    }
}
