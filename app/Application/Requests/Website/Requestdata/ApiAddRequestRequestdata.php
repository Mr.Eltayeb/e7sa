<?php

namespace App\Application\Requests\Website\Requestdata;


class ApiAddRequestRequestdata
{
    public function rules()
    {
        return [
            "name" => "min:3|requiredtype:identity",
			"data_details" => "required",
			
        ];
    }
}
