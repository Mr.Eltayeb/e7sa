<?php

namespace App\Application\Requests\Website\Requestdata;

use Illuminate\Foundation\Http\FormRequest;

class AddRequestRequestdata extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "min:3|required",
			"type_identity" => "",
			"Identity" => "",
			"occupation" => "required",
			"qualification" => "required",
			"phone" => "required",
			"fax" => "",
			"email" => "email",
			"data_usage" => "",
			"company_name" => "",
			"type_Company" => "",
			"Data_usage_area" => "required",
			"data_details" => "required",
			
        ];
    }
}
