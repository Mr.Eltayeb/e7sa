<?php

namespace App\Application\Requests\Website\Webnews;


class ApiAddRequestWebnews
{
    public function rules()
    {
        return [
            "title.*" => "min:3|max:150|required",
			"url.*" => "nullable|url",
			
        ];
    }
}
