<?php

namespace App\Application\Requests\Website\Webnews;

use Illuminate\Support\Facades\Route;

class ApiUpdateRequestWebnews
{
    public function rules()
    {
        $id = Route::input('id');
        return [
            "title.*" => "min:3|max:150|required",
			"url.*" => "nullable|url",
			
        ];
    }
}
