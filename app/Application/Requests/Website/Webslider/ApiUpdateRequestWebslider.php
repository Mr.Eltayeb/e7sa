<?php
 namespace App\Application\Requests\Website\Webslider;
 use Illuminate\Support\Facades\Route;
 class ApiUpdateRequestWebslider
{
    public function rules()
    {
        $id = Route::input('id');
        return [
        	"webcategorie_id" => "required|integer",
            "title.*" => "min:3|max:25|requiredimage.*",
   "url.*" => "required|url",
            ];
    }
}
