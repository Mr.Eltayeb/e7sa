<?php
 namespace App\Application\Requests\Website\Webslider;
  class ApiAddRequestWebslider
{
    public function rules()
    {
        return [
        	"webcategorie_id" => "required|integer",
            "title.*" => "min:3|max:25|requiredimage.*",
   "url.*" => "required|url",
            ];
    }
}
