<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class WebstatisticsaddTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en"),
			"file_ar" => getLangValue($modelOrCollection->file_ar , "en"),
			"file_en" => getLangValue($modelOrCollection->file_en , "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar"),
			"file_ar" => getLangValue($modelOrCollection->file_ar , "ar"),
			"file_en" => getLangValue($modelOrCollection->file_en , "ar")

        ];
    }

}