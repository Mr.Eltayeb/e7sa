<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class VersionsTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "title" => $modelOrCollection->title,
			"image" => getLangValue($modelOrCollection->image, "en"),
			"file" => getLangValue($modelOrCollection->file, "en"),
			"file_en" => getLangValue($modelOrCollection->file_en, "en"),
			"year" => getLangValue($modelOrCollection->year, "en")
        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "title" => $modelOrCollection->title,
			"image" => getLangValue($modelOrCollection->image, "ar"),
			"file" => getLangValue($modelOrCollection->file, "ar"),
			"file_en" => getLangValue($modelOrCollection->file_en, "ar"),
			"year" => getLangValue($modelOrCollection->year, "ar")

        ];
    }

}