<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class TrainingTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "name" => $modelOrCollection->name,
            "university" => $modelOrCollection->university,
            "college" => $modelOrCollection->college,
            "specialty" => $modelOrCollection->specialty,
            "phone" => $modelOrCollection->phone,
            "email" => $modelOrCollection->email,
            "type_training" => $modelOrCollection->type_training,
            "type_college" => $modelOrCollection->type_college,
            "departments" => $modelOrCollection->departments,
            "competence" => $modelOrCollection->competence,

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "name" => $modelOrCollection->name,
            "university" => $modelOrCollection->university,
            "college" => $modelOrCollection->college,
            "specialty" => $modelOrCollection->specialty,
            "phone" => $modelOrCollection->phone,
            "email" => $modelOrCollection->email,
            "type_training" => $modelOrCollection->type_training,
            "type_college" => $modelOrCollection->type_college,
            "departments" => $modelOrCollection->departments,
            "competence" => $modelOrCollection->competence

        ];
    }

}