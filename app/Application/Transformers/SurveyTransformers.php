<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class SurveyTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en")
			"answers1" => getLangValue($modelOrCollection->answers1 , "en")
			"answers2" => getLangValue($modelOrCollection->answers2 , "en"),
			"answers3" => getLangValue($modelOrCollection->answers3 , "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar")
			"answers1" => getLangValue($modelOrCollection->answers1 , "ar")
			"answers2" => getLangValue($modelOrCollection->answers2 , "ar"),
			"answers3" => getLangValue($modelOrCollection->answers3 , "ar")

        ];
    }

}