<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class WebnewsTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en"),
			"url" => getLangValue($modelOrCollection->url , "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar"),
			"url" => getLangValue($modelOrCollection->url , "ar")

        ];
    }

}