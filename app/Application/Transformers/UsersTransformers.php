<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class UsersTransformers extends AbstractTransformer
{
    public function transformModel(Model $modelOrCollection)
    {
        return [
            'id' => $modelOrCollection->id,
            'name' => $modelOrCollection->name,
            'lname' => $modelOrCollection->lname,
            'address' => $modelOrCollection->address,
            'phone' => $modelOrCollection->phone,
            'email' => $modelOrCollection->email,
            'token' => $modelOrCollection->api_token,
        ];
    }
}