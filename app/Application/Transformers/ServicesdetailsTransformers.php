<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class ServicesdetailsTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "title" => getLangValue($modelOrCollection->title, "en"),
            "image" => $modelOrCollection->image,
            "body" => getLangValue($modelOrCollection->body, "en"),
            "url" => getLangValue($modelOrCollection->url, "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
            "title" => getLangValue($modelOrCollection->title, "ar"),
            "image" => $modelOrCollection->image,
            "body" => getLangValue($modelOrCollection->body, "ar"),
            "url" => getLangValue($modelOrCollection->url, "ar")

        ];
    }

}