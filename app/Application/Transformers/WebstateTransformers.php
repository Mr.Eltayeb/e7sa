<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class WebstateTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en"),
			"image" => getLangValue($modelOrCollection->image , "en"),
			"body" => getLangValue($modelOrCollection->body , "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar"),
			"image" => getLangValue($modelOrCollection->image , "ar"),
			"body" => getLangValue($modelOrCollection->body , "ar")

        ];
    }

}