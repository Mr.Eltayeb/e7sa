<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class ElibraryTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en")
			"image" => getLangValue($modelOrCollection->image , "en")
			"file" => $modelOrCollection->file,
			"file_en" => $modelOrCollection->file_en

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar")
			"image" => getLangValue($modelOrCollection->image , "ar")
			"file" => $modelOrCollection->file,
			"file_en" => $modelOrCollection->file_en

        ];
    }

}