<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class RequestdataTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"name" => $modelOrCollection->name
			"type_identity" => $modelOrCollection->type_identity
			"Identity" => $modelOrCollection->Identity
			"occupation" => $modelOrCollection->occupation
			"qualification" => $modelOrCollection->qualification
			"phone" => $modelOrCollection->phone
			"fax" => $modelOrCollection->fax
			"email" => $modelOrCollection->email
			"data_usage" => $modelOrCollection->data_usage
			"company_name" => $modelOrCollection->company_name
			"type_Company" => $modelOrCollection->type_Company
			"Data_usage_area" => $modelOrCollection->Data_usage_area,
			"data_details" => $modelOrCollection->data_details

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"name" => $modelOrCollection->name
			"type_identity" => $modelOrCollection->type_identity
			"Identity" => $modelOrCollection->Identity
			"occupation" => $modelOrCollection->occupation
			"qualification" => $modelOrCollection->qualification
			"phone" => $modelOrCollection->phone
			"fax" => $modelOrCollection->fax
			"email" => $modelOrCollection->email
			"data_usage" => $modelOrCollection->data_usage
			"company_name" => $modelOrCollection->company_name
			"type_Company" => $modelOrCollection->type_Company
			"Data_usage_area" => $modelOrCollection->Data_usage_area,
			"data_details" => $modelOrCollection->data_details

        ];
    }

}