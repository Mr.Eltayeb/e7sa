<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class CentralcategoriesTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en")
			"file" => getLangValue($modelOrCollection->file , "en"),
			"file_en" => getLangValue($modelOrCollection->file_en , "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar")
			"file" => getLangValue($modelOrCollection->file , "ar"),
			"file_en" => getLangValue($modelOrCollection->file_en , "ar")

        ];
    }

}