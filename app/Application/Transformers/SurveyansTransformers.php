<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class SurveyansTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"answers" => $modelOrCollection->answers

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"answers" => $modelOrCollection->answers

        ];
    }

}