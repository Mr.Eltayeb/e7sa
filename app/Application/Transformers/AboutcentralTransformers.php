<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class AboutcentralTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en"),
			"body" => getLangValue($modelOrCollection->body , "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar"),
			"body" => getLangValue($modelOrCollection->body , "ar")

        ];
    }

}