<?php

namespace App\Application\Transformers;

use Illuminate\Database\Eloquent\Model;

class WebsliderTransformers extends AbstractTransformer
{

    public function transformModel(Model $modelOrCollection)
    {
        return [
            "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "en"),
            "image" => url(env('UPLOAD_PATH'). '/'.$modelOrCollection->image),
			"url" => getLangValue($modelOrCollection->url , "en")

        ];
    }

    public function transformModelAr(Model $modelOrCollection)
    {
        return [
           "id" => $modelOrCollection->id,
			"title" => getLangValue($modelOrCollection->title , "ar"),
            "image" => url(env('UPLOAD_PATH'). '/'.$modelOrCollection->image),
			"url" => getLangValue($modelOrCollection->url , "ar")

        ];
    }

}