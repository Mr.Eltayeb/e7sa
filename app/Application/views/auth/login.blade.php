@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <a href="https://twitter.com/CbsSudan"></a>


                <div class="panel-heading">{{getDir() =='rtl' ? "تسجيل الدخول" : "Login"}} <i class="fa fa-user"
                                                                                              style="color: #145399; "></i>

                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{getDir() =='rtl' ? "البريد الإلكتروني" : "E-mail Address"}}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{getDir() =='rtl' ? "كلمة المرور" : "Password"}}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{getDir() =='rtl' ? "تذكرني" : "Remember Me"}}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{getDir() =='rtl' ? "تسجيل دخول" : "Login"}}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{getDir() =='rtl' ? "نسيت كلمة المرور؟" : "Forgot Your Password?"}}
                                </a>   <a class="btn btn-link" href="{{asset('/register')}}">
                                    {{getDir() =='rtl' ? " عضو جديد" : "New Member"}}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
