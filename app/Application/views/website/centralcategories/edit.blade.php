@extends('layouts.app')
 @section('title')
    {{ trans('centralcategories.centralcategories') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
<div class="container">
         @include('layouts.messages')
         <a href="{{ url('centralcategories') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('centralcategories/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
               <div class="form-group">
   <label for="title">{{ trans("centralcategories.title")}}</label>
    {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "centralcategories") !!}
   </label>
  </div>
  <div class="form-group">
   <label for="file">{{ trans("centralcategories.file")}}</label>
    @if(isset($item) && $item->file != "")
    <br>
    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}" class="thumbnail" alt="" width="200">
    <br>
    @endif
    <input type="file" name="file" >
   </label>
  </div>
  <div class="form-group">
   <label for="file_en">{{ trans("centralcategories.file_en")}}</label>
    @if(isset($item) && $item->file_en != "")
    <br>
    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" class="thumbnail" alt="" width="200">
    <br>
    @endif
    <input type="file" name="file_en" >
   </label>
  </div>
             <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.centralcategories') }}
                </button>
            </div>
        </form>
</div>
@endsection
