@extends('layouts.app')
@section('title')
    {{ trans('centralcategories.centralcategories') }} {{ trans('home.view') }}
@endsection
@section('content')
    <div class="container">
        <a href="{{ url('centralcategories') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            </tr>
            <tr>
                <th>{{ trans("centralcategories.title") }}</th>
                @php $type =  getFileType("title" , $item->title) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("centralcategories.file") }}</th>
                @php $type =  getFileType("file" , $item->file) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file) }}">{{ $item->file }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->file)) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("centralcategories.file_en") }}</th>
                @php $type =  getFileType("file_en" , $item->file_en) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}">{{ $item->file_en }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->file_en)) }}</td>
                @endif</tr>
            </tr>
        </table>

    </div>
@endsection
