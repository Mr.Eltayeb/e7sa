@extends('layouts.app')

@section('title')
    {{ trans('centralcategories.centralcategories') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.centralcategories') }}</h1></div>
        <div><a href="{{ url('centralcategories/item') }}" class="btn btn-default"><i
                        class="fa fa-plus"></i> {{ trans('website.centralcategories') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("centralcategories.title") }}</th>
                <th>{{ trans("centralcategories.file") }}</th>
                <th>{{ trans("centralcategories.file_en") }}</th>
                <th>{{ trans("centralcategories.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>
                            <img src="{{ url(env("UPLOAD_PATH")."/".$d->file)}}" width="80"/>
                        </td>
                        <td>
                            <img src="{{ url(env("UPLOAD_PATH")."/".$d->file_en)}}" width="80"/>
                        </td>
                        <td>@include("website.centralcategories.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
