@extends('layouts.app')

@section('title')
     {{ trans('servicesdetails.servicesdetails') }} {{ trans('home.control') }}
@endsection

@section('content')
<div class="container">
    <div><h1>{{ trans('website.servicesdetails') }}</h1></div>
     {{--<div><a href="{{ url('servicesdetails/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.servicesdetails') }}</a><br></div>--}}
 <table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>{{ trans("servicesdetails.title") }}</th>
				<th>{{ trans("servicesdetails.image") }}</th>
				<th>{{ trans("servicesdetails.body") }}</th>
{{--				<th>{{ trans("servicesdetails.edit") }}</th>--}}
				<th>{{ trans("servicesdetails.show") }}</th>
{{--				<th>{{ trans("servicesdetails.delete") }}</th>--}}
				</thead>
		<tbody>
		@if(count($items) > 0)
			@foreach($items as $d)
				<tr>
					<td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
									<td>
				<img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}"  width="80" />
					</td>
<td>{{ str_limit(getDefaultValueKey($d->body) , 20) }}</td>
{{--				<td>@include("website.servicesdetails.buttons.edit", ["id" => $d->id ])</td>--}}
					<td>@include("website.servicesdetails.buttons.view", ["id" => $d->id ])</td>
{{--					<td>@include("website.servicesdetails.buttons.delete", ["id" => $d->id ])</td>--}}
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	@include("layouts.paginate" , ["items" => $items])
		
</div>
@endsection
