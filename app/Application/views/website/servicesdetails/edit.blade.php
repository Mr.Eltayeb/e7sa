@extends('layouts.app')

@section('title')
    {{ trans('servicesdetails.servicesdetails') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
<div class="container">
         @include('layouts.messages')
         <a href="{{ url('servicesdetails') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('servicesdetails/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
             		<div class="form-group">
			<label for="title">{{ trans("servicesdetails.title")}}</label>
				{!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "servicesdetails") !!}
			</label>
		</div>
		<div class="form-group">
			<label for="image">{{ trans("servicesdetails.image")}}</label>
				@if(isset($item) && $item->image != "")
				<br>
				<img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
				<br>
				@endif
				<input type="file" name="image" >
			</label>
		</div>
		<div class="form-group">
			<label for="body">{{ trans("servicesdetails.body")}}</label>
				{!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , "textarea" , "servicesdetails") !!}
			</label>
		</div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.servicesdetails') }}
                </button>
            </div>
        </form>
</div>
@endsection
