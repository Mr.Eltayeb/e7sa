@extends('layouts.app')

@section('title')
    {{ trans('webnews.webnews') }} {{ trans('home.view') }}
@endsection

@section('content')
    <div class="container">
        <a href="{{ url('webnews') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            </tr>
            <tr>
                <th>{{ trans("webnews.title") }}</th>
                @php $type =  getFileType("title" , $item->title) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("webnews.url") }}</th>
                @php $type =  getFileType("url" , $item->url) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->url) }}">{{ $item->url }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->url) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->url)) }}</td>
                @endif</tr>
            </tr>
        </table>
    </div>
@endsection
