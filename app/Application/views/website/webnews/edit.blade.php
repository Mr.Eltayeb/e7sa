@extends('layouts.app')

@section('title')
    {{ trans('webnews.webnews') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
<div class="container">
         @include('layouts.messages')
         <a href="{{ url('webnews') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('webnews/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
             		<div class="form-group">
			<label for="title">{{ trans("webnews.title")}}</label>
				{!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "webnews") !!}
			</label>
		</div>
		<div class="form-group">
			<label for="url">{{ trans("webnews.url")}}</label>
				{!! extractFiled("url" , isset($item->url) ? $item->url : old("url") , "text" , "webnews") !!}
			</label>
		</div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.webnews') }}
                </button>
            </div>
        </form>
</div>
@endsection
