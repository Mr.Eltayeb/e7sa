@extends('layouts.app')

@section('title')
    {{ trans('webnews.webnews') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.webnews') }}</h1></div>
        <div><a href="{{ url('webnews/item') }}" class="btn btn-default"><i
                        class="fa fa-plus"></i> {{ trans('website.webnews') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("webnews.title") }}</th>
                <th>{{ trans("webnews.url") }}</th>
                <th>{{ trans("webnews.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>{{ str_limit(getDefaultValueKey($d->url) , 20) }}</td>
                        <td>@include("website.webnews.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
