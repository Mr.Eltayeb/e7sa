@extends('layouts.app')

@section('title')
    {{ trans('directory.directory') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.directory') }}</h1></div>

        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("directory.title") }}</th>
                <th>{{ trans("directory.file") }}</th>
                <th>{{ trans("directory.file_en") }}</th>
                <th>{{ trans("directory.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>
                            <a href="{{ url(env("UPLOAD_PATH")."/".$d->file)}}" ><span style="color: brown" class="fa fa-3x fa-download"></span> </a>
                        </td>
                        <td>
                            <a href="{{ url(env("UPLOAD_PATH")."/".$d->file_en)}}" ><span style="color: burlywood" class="fa fa-3x fa-download"></span> </a>
                        </td>
                        <td>@include("website.directory.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
