@extends('layouts.app')
@section('title')
    {{ trans('directory.directory') }} {{ trans('home.view') }}
@endsection
@section('content')
    <div class="container">
        <a href="{{ url('directory') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped webFont">
            @include("website.directory.relation.directorycategories.show")
            <tr>
                <th>{{ trans("directory.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("directory.body") }}</th>

                <td>{!!  getDefaultValueKey(nl2br($item->body)) !!}</td>
            </tr>
            <tr>
                <th>{{ trans("directory.file") }}</th>

                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file) }}"><span style="color: brown" class="fa fa-3x fa-download"></span></a></td>
              </tr>
            <tr>
                <th>{{ trans("directory.file_en") }}</th>

                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"><span style="color: #f1a899" class="fa fa-3x fa-download"></span></a></td>
               </tr>
            </tr>
        </table>

    </div>
@endsection
