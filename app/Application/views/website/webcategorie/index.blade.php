@extends('layouts.app')

@section('title')
     {{ trans('webcategorie.webcategorie') }} {{ trans('home.control') }}
@endsection

@section('content')
<div class="container">
    <div><h1>{{ trans('website.webcategorie') }}</h1></div>
     <div><a href="{{ url('webcategorie/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.webcategorie') }}</a><br></div>
 <table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>{{ trans("webcategorie.title") }}</th>
				<th>{{ trans("webcategorie.edit") }}</th>
				<th>{{ trans("webcategorie.show") }}</th>
				<th>{{ trans("webcategorie.delete") }}</th>
				</thead>
		<tbody>
		@if(count($items) > 0)
			@foreach($items as $d)
				<tr>
					<td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
				<td>@include("website.webcategorie.buttons.edit", ["id" => $d->id ])</td>
					<td>@include("website.webcategorie.buttons.view", ["id" => $d->id ])</td>
					<td>@include("website.webcategorie.buttons.delete", ["id" => $d->id ])</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	@include("layouts.paginate" , ["items" => $items])
		
</div>
@endsection
