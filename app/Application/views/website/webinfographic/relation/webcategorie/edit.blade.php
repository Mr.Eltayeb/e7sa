		<div class="form-group">
			<label for="webcategorie">{{ trans( "webcategorie.webcategorie") }}</label>
			@php $webcategories = App\Application\Model\Webcategorie::pluck("title" ,"id")->all()  @endphp
			@php  $webcategorie_id = isset($item) ? $item->webcategorie_id : null @endphp
			<select name="webcategorie_id"  class="form-control" >
			@foreach( $webcategories as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $webcategorie_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</label>
			@endforeach
			</select>
		</div>
