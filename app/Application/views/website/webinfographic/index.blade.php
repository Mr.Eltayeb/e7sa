@extends('layouts.app')

@section('title')
    {{ trans('webinfographic.webinfographic') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.webinfographic') }}</h1></div>
        <div><a href="{{ url('webinfographic/item') }}" class="btn btn-default"><i
                        class="fa fa-plus"></i> {{ trans('website.webinfographic') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("webinfographic.title") }}</th>
                <th>{{ trans("webinfographic.image") }}</th>
                <th>{{ trans("webinfographic.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>
                            <img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}" width="80"/>
                        </td>
                        <td>@include("website.webinfographic.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
