@extends('layouts.app')
@section('title')
    {{ trans('webinfographic.webinfographic') }} {{ trans('home.view') }}
@endsection
@section('content')
    <div class="container">
        <a href="{{ url('webinfographic') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            @include("website.webinfographic.relation.webcategorie.show")
            <tr>
                <th>{{ trans("webinfographic.title") }}</th>
                <th>{{ trans("webinfographic.url") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                <td><a href="{{ getDefaultValueKey(nl2br($item->url)) }}"><span class="fa fa-eye"></span></a></td>
            </tr>
            <tr>
                <th>{{ trans("webinfographic.image") }}</th>
                <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
            </tr>
        </table>
    </div>
@endsection
