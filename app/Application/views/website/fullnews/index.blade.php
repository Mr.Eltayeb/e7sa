@extends('layouts.app')

@section('title')
    {{ trans('fullnews.fullnews') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.fullnews') }}</h1></div>
        <div><a href="{{ url('fullnews/item') }}" class="btn btn-default"><i
                        class="fa fa-plus"></i> {{ trans('website.fullnews') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("fullnews.title") }}</th>
                <th>{{ trans("fullnews.image") }}</th>
                <th>{{ trans("fullnews.body") }}</th>
                <th>{{ trans("fullnews.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>
                            <img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}" width="80"/>
                        </td>
                        <td>{!!  str_limit(getDefaultValueKey($d->body) , 40) !!}</td>
                        <td>@include("website.fullnews.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
