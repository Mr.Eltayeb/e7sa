@extends('layouts.app')

@section('title')
    {{ trans('fullnews.fullnews') }} {{ trans('home.view') }}
@endsection

@section('content')
    <div class="container">
        <a href="{{ url('fullnews') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("fullnews.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("fullnews.image") }}</th>

                <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
            </tr>
            <tr>
                <th>{{ trans("fullnews.body") }}</th>

                <td>{!! getDefaultValueKey(nl2br($item->body)) !!}</td>
            </tr>
        </table>

    </div>
@endsection
