@extends('layouts.app')
@section('title')
    {{ trans('webslider.webslider') }} {{ trans('home.view') }}
@endsection
@section('content')
    <div class="container">
        <a href="{{ url('webslider') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            @include("website.webslider.relation.webcategorie.show")
            <tr>
                <th>{{ trans("webslider.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webslider.image") }}</th>
                <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
            </tr>
            <tr>
                <th>{{ trans("webslider.url") }}</th>

                <td><a href="{{  getDefaultValueKey(nl2br($item->url)) }}"><span class="fa fa-eye"></span></a></td>
            </tr>
        </table>

    </div>
@endsection
