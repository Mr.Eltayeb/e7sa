@extends('layouts.app')

@section('title')
     {{ trans('webslider.webslider') }} {{ trans('home.control') }}
@endsection

@section('content')
<div class="container">
    <div><h1>{{ trans('website.webslider') }}</h1></div>
 <table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>{{ trans("webslider.title") }}</th>
				<th>{{ trans("webslider.image") }}</th>
				<th>{{ trans("webslider.url") }}</th>
				<th>{{ trans("webslider.show") }}</th>
				</thead>
		<tbody>
		@if(count($items) > 0)
			@foreach($items as $d)
				<tr>
					<td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
									<td>
				<img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}"  width="80" />
					</td>
<td>{{ str_limit(getDefaultValueKey($d->url) , 20) }}</td>
					<td>@include("website.webslider.buttons.view", ["id" => $d->id ])</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	@include("layouts.paginate" , ["items" => $items])
		
</div>
@endsection
