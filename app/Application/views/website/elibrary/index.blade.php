@extends('layouts.app')

@section('title')
    {{ trans('elibrary.elibrary') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.elibrary') }}</h1></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("elibrary.title") }}</th>
                <th>{{ trans("elibrary.image") }}</th>
                <th>{{ trans("elibrary.file") }}</th>
                <th>{{ trans("elibrary.file_en") }}</th>
                <th>{{ trans("elibrary.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ getDefaultValueKey($d->title) }}</td>
                        <td>
                            <img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}" width="80"/>
                        </td>
                        <td>
                            <a href="{{ url(env("UPLOAD_PATH")."/".$d->file)}}" ></a>
                        </td>
                        <td>
                            <a href="{{ url(env("UPLOAD_PATH")."/".$d->file_en)}}" ></a>
                        </td>
                        <td>@include("website.elibrary.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
