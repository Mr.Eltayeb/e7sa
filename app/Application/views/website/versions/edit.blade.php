@extends('layouts.app')
 @section('title')
    {{ trans('versions.versions') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
<div class="container">
         @include('layouts.messages')
         <a href="{{ url('versions') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('versions/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("website.versions.relation.cateversions.edit")
               <div class="form-group">
   <label for="title">{{ trans("versions.title")}}</label>
    <input type="text" name="title" class="form-control" id="title" value="{{ isset($item->title) ? $item->title : old("title") }}"  placeholder="{{ trans("versions.title")}}">
   </label>
  </div>
  <div class="form-group">
   <label for="image">{{ trans("versions.image")}}</label>
    @if(isset($item) && $item->image != "")
    <br>
    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
    <br>
    @endif
    <input type="file" name="image" >
   </label>
  </div>
  <div class="form-group">
   <label for="file">{{ trans("versions.file")}}</label>
    @if(isset($item) && $item->file != "")
    <br>
    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}" class="thumbnail" alt="" width="200">
    <br>
    @endif
    <input type="file" name="file" >
   </label>
  </div>
  <div class="form-group">
   <label for="file_en">{{ trans("versions.file_en")}}</label>
    @if(isset($item) && $item->file_en != "")
    <br>
    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" class="thumbnail" alt="" width="200">
    <br>
    @endif
    <input type="file" name="file_en" >
   </label>
  </div>
  <div class="form-group">
   <label for="year">{{ trans("versions.year")}}</label>
    {!! extractFiled("year" , isset($item->year) ? $item->year : old("year") , "text" , "versions") !!}
   </label>
  </div>
             <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.versions') }}
                </button>
            </div>
        </form>
</div>
@endsection
