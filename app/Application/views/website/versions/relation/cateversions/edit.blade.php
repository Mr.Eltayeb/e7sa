		<div class="form-group">
			<label for="cateversions">{{ trans( "cateversions.cateversions") }}</label>
			@php $cateversions = App\Application\Model\Cateversions::pluck("title" ,"id")->all()  @endphp
			@php  $cateversions_id = isset($item) ? $item->cateversions_id : null @endphp
			<select name="cateversions_id"  class="form-control" >
			@foreach( $cateversions as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $cateversions_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</label>
			@endforeach
			</select>
		</div>
