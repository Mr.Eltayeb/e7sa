@extends('layouts.app')

@section('title')
     {{ trans('versions.versions') }} {{ trans('home.control') }}
@endsection

@section('content')
<div class="container">
    <div><h1>{{ trans('website.versions') }}</h1></div>
     <div><a href="{{ url('versions/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.versions') }}</a><br></div>
 <table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>{{ trans("versions.title") }}</th>
				<th>{{ trans("versions.image") }}</th>
				<th>{{ trans("versions.file") }}</th>
				<th>{{ trans("versions.file_en") }}</th>
				<th>{{ trans("versions.year") }}</th>
				<th>{{ trans("versions.edit") }}</th>
				<th>{{ trans("versions.show") }}</th>
				<th>{{ trans("versions.delete") }}</th>
				</thead>
		<tbody>
		@if(count($items) > 0)
			@foreach($items as $d)
				<tr>
					<td>{{ str_limit($d->title , 20) }}</td>
									<td>
				<img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}"  width="80" />
					</td>
					<td>
				<img src="{{ url(env("UPLOAD_PATH")."/".$d->file)}}"  width="80" />
					</td>
					<td>
				<img src="{{ url(env("UPLOAD_PATH")."/".$d->file_en)}}"  width="80" />
					</td>
<td>{{ str_limit(getDefaultValueKey($d->year) , 20) }}</td>
				<td>@include("website.versions.buttons.edit", ["id" => $d->id ])</td>
					<td>@include("website.versions.buttons.view", ["id" => $d->id ])</td>
					<td>@include("website.versions.buttons.delete", ["id" => $d->id ])</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	@include("layouts.paginate" , ["items" => $items])
		
</div>
@endsection
