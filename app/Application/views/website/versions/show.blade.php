@extends('layouts.app')
  @section('title')
    {{ trans('versions.versions') }} {{ trans('home.view') }}
@endsection
  @section('content')
<div class="container">
        <a href="{{ url('versions') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
  <table class="table table-bordered table-responsive table-striped">
  @include("website.versions.relation.cateversions.show")
   </tr>
    <tr><th>{{ trans("versions.title") }}</th>
    @php $type =  getFileType("title" , $item->title) @endphp
    @if($type == "File")
     <td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
    @elseif($type == "Image")
     <td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}" /></td>
    @else
     <td>{{ nl2br($item->title) }}</td>
    @endif</tr>
    <tr><th>{{ trans("versions.image") }}</th>
    @php $type =  getFileType("image" , $item->image) @endphp
    @if($type == "File")
     <td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->image) }}">{{ $item->image }}</a></td>
    @elseif($type == "Image")
     <td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" /></td>
    @else
     <td>{{ getDefaultValueKey(nl2br($item->image)) }}</td>
    @endif</tr>
    <tr><th>{{ trans("versions.file") }}</th>
    @php $type =  getFileType("file" , $item->file) @endphp
    @if($type == "File")
     <td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->file) }}">{{ $item->file }}</a></td>
    @elseif($type == "Image")
     <td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}" /></td>
    @else
     <td>{{ getDefaultValueKey(nl2br($item->file)) }}</td>
    @endif</tr>
    <tr><th>{{ trans("versions.file_en") }}</th>
    @php $type =  getFileType("file_en" , $item->file_en) @endphp
    @if($type == "File")
     <td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}">{{ $item->file_en }}</a></td>
    @elseif($type == "Image")
     <td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" /></td>
    @else
     <td>{{ getDefaultValueKey(nl2br($item->file_en)) }}</td>
    @endif</tr>
    <tr><th>{{ trans("versions.year") }}</th>
    @php $type =  getFileType("year" , $item->year) @endphp
    @if($type == "File")
     <td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->year) }}">{{ $item->year }}</a></td>
    @elseif($type == "Image")
     <td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->year) }}" /></td>
    @else
     <td>{{ getDefaultValueKey(nl2br($item->year)) }}</td>
    @endif</tr>
   </tr>
  </table>
          @include('website.versions.buttons.delete' , ['id' => $item->id])
        @include('website.versions.buttons.edit' , ['id' => $item->id])
</div>
@endsection
