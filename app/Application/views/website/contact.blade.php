@extends('layouts.app')
@push('css')
<link rel="stylesheet" href="{{asset('css/animate.css')}}">

<style>
    .questo:hover {
        -webkit-animation: pulse 1s;
        animation: pulse 1s;
    }

    .margin {
        margin-bottom: 20px;
    }

    body {
        background-color: white;
    }
</style>
@endpush
@section('content')
    <div class="container">
        <div class="row">

            <div class="wow bounceInUp">
                <div class="col-md-4 " style="
                {{getDir() == 'rtl'? "margin-right: 65%;": "margin-left:65%;"}}
                        width: 40%;
                        margin-top: 2%;
                        ">
                    <div class="contact-info">
                        <div class="list-group">
                            <ul class="contact-place one align_to_left">
                                <li class="animated  questo list-group-item list-group-item-action"
                                    style="background-color: #193e62;color:  black;font-size: 22px;"><span
                                            style="color: white"
                                            class="fa fa-envelope"></span>
                                    <small style="color: white">info@cbs.gov.sd</small>
                                </li>
                                <li class="animated  questo list-group-item list-group-item-action"
                                    style="background-color: #193e62;color:  black;font-size: 22px;"><span
                                            style="color: white"
                                            class="fa fa-phone"></span>
                                    <small style="color: white"> {{getDir() == 'rtl' ? "777255 183 249+ - 777131 183 249+" : "+249 183 777255 +249 183 777131"}}&nbsp;</small>
                                </li>
                                <li class="animated  questo list-group-item list-group-item-action"
                                    style="background-color: #193e62;color:  black;font-size: 22px;"><span
                                            style="color: white"
                                            class="fa fa-map-marker"></span>
                                    <small style="color: white">{{getDir()=='rtl'? "شارع الستين جنوب تقاطع لفة جوبا &nbsp;مربع&nbsp;85" : "Khartoum-Sudan"}}</small>
                                </li>
                                <li class="animated  questo list-group-item list-group-item-action"
                                    style="background-color: #193e62;color:  black;font-size: 22px;"><span
                                            style="color: white" class="fa fa-fax"></span>
                                    <small style="color: white">{{getDir() =='rtl'? "7771960 183 249+" : "+249 183 7771860"}}&nbsp;</small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8" style="
    margin-top: -18%;
>
                <div class=" panel panel-default">
            <div class="panel-heading"><b style="
    font-size: 20px;
">{{ trans('website.Contact Us') }}</b></div>
            <div class="panel-body">
                <form class="row" action="{{ concatenateLangToUrl('contact') }}" name="contactform"
                      method="post">
                    {{ csrf_field() }}
                    <div class="col-md-6 margin">
                        <input type="text" name="name" id="name" class="form-control"
                               placeholder="{{ trans('website.Name') }}"
                               value="{{ auth()->check() ? auth()->user()->name : '' }}" required>
                    </div>
                    <div class="col-md-6 margin">
                        <input type="text" name="email" id="email" class="form-control"
                               placeholder="{{ trans('website.Email') }}"
                               required {{ auth()->check() ? auth()->user()->email : '' }}>
                    </div>
                    <div class="col-md-6 margin">
                        <input type="text" name="phone" id="phone" class="form-control"
                               placeholder="{{ trans('website.Phone') }}">
                    </div>
                    <div class="col-md-6 margin">
                        <input type="text" name="subject" id="subject" class="form-control"
                               placeholder="{{ trans('website.Subject') }}" required>
                    </div>
                    <div class="col-md-12 margin">
                    <textarea class="form-control" name="message" id="comments" rows="6"
                              placeholder="{{ trans('website.Message Below') }}" required></textarea>
                    </div>
                    <div class="col-md-12 margin">
                        <button type="submit" value="SEND" id="submit"
                                class="btn btn-primary">{{ trans('website.Send Form') }} </button>
                    </div>
                </form>
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>

@endsection