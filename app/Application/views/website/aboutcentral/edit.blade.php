@extends('layouts.app')

@section('title')
    {{ trans('aboutcentral.aboutcentral') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    <div class="container">
        @include('layouts.messages')
        <a href="{{ url('aboutcentral') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('aboutcentral/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">{{ trans("aboutcentral.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "aboutcentral") !!}
            </div>
            <div class="form-group">
                <label for="body">{{ trans("aboutcentral.body")}}</label>
                {!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , "textarea" , "aboutcentral") !!}
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.aboutcentral') }}
                </button>
            </div>
        </form>
    </div>
@endsection
