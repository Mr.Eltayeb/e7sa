@extends('layouts.app')

@section('title')
    {{ trans('aboutcentral.aboutcentral') }} {{ trans('home.view') }}
@endsection

@section('content')
    <div class="container">
        <a href="{{ url('aboutcentral') }}" class="btn btn-danger btn-lg"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th style="
    font-size: 16px;
">{{ trans("aboutcentral.title") }}</th>
                    <td style="
    font-size: 16px;
">{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            <tr>
                <th style="
    font-size: 16px;
">{{ trans("aboutcentral.body") }}</th>
                @php $type =  getFileType("body" , $item->body) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->body) }}">{{ $item->body }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->body) }}"/></td>
                @else
                    <td>{!! getDefaultValueKey(nl2br($item->body)) !!}</td>
                @endif</tr>
        </table>

    </div>
@endsection
