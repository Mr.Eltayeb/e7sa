@extends('layouts.app')
@section('title')
    {{ trans('training.training') }} {{ trans('home.view') }}
@endsection
@section('content')
    <div class="container">
        <a href="{{ url('training') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            @include("website.training.relation.user.show")
            <tr>
                <th style="width: 15%;">{{ trans("training.request_status") }}</th>

                <td>{{ nl2br($item->request_status) }}</td>
            <tr>
            <tr>
                <th>{{ trans("training.name") }}</th>

                <td>{{ nl2br($item->name) }}</td>
            <tr>
                <th>{{ trans("training.university") }}</th>

                <td>{{ nl2br($item->university) }}</td>
            <tr>
                <th>{{ trans("training.college") }}</th>
                <td>{{ nl2br($item->college) }}</td>
            <tr>
                <th>{{ trans("training.specialty") }}</th>
                @php $type =  getFileType("specialty" , $item->specialty) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->specialty) }}">{{ $item->specialty }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->specialty) }}"/></td>
                @else
                    <td>{{ nl2br($item->specialty) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.phone") }}</th>
                @php $type =  getFileType("phone" , $item->phone) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->phone) }}">{{ $item->phone }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->phone) }}"/></td>
                @else
                    <td>{{ nl2br($item->phone) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.email") }}</th>
                @php $type =  getFileType("email" , $item->email) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->email) }}">{{ $item->email }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->email) }}"/></td>
                @else
                    <td>{{ nl2br($item->email) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.type_training") }}</th>
                @php $type =  getFileType("type_training" , $item->type_training) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->type_training) }}">{{ $item->type_training }}</a>
                    </td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->type_training) }}"/></td>
                @else
                    <td>{{ nl2br($item->type_training) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.departments") }}</th>
                @php $type =  getFileType("departments" , $item->departments) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->departments) }}">{{ $item->departments }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->departments) }}"/></td>
                @else
                    <td>{{ nl2br($item->departments) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.competence") }}</th>
                @php $type =  getFileType("competence" , $item->competence) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->competence) }}">{{ $item->competence }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->competence) }}"/></td>
                @else
                    <td>{{ nl2br($item->competence) }}</td>
                @endif</tr>
            </tr>
        </table>
        @include('website.training.buttons.delete' , ['id' => $item->id])
        @include('website.training.buttons.edit' , ['id' => $item->id])
    </div>
@endsection
