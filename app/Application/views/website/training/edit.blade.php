@extends('layouts.app')
@section('title')
    {{ trans('training.training') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    <div class="container">
        @include('layouts.messages')
        <a href="{{ url('training') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('training/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post"
              enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("website.training.relation.user.edit")
            <div class="form-group">
                <label for="name">{{ trans("training.name")}}</label>
                <input type="text" name="name" class="form-control" id="name"
                       value="{{ isset($item->name) ? $item->name : old("name") }}"
                       placeholder="{{ trans("training.name")}}">
            </div>
            <div class="form-group">
                <label for="university">{{ trans("training.university")}}</label>
                <input type="text" name="university" class="form-control" id="university"
                       value="{{ isset($item->university) ? $item->university : old("university") }}"
                       placeholder="{{ trans("training.university")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="college">{{ trans("training.college")}}</label>
                <input type="text" name="college" class="form-control" id="college"
                       value="{{ isset($item->college) ? $item->college : old("college") }}"
                       placeholder="{{ trans("training.college")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="specialty">{{ trans("training.specialty")}}</label>
                <input type="text" name="specialty" class="form-control" id="specialty"
                       value="{{ isset($item->specialty) ? $item->specialty : old("specialty") }}"
                       placeholder="{{ trans("training.specialty")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="phone">{{ trans("training.phone")}}</label>
                <input type="text" name="phone" class="form-control" id="phone" maxlength="10" minlength="10"
                       value="{{ isset($item->phone) ? $item->phone : old("phone") }}"
                       placeholder="{{ trans("training.phone")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="email">{{ trans("training.email")}}</label>
                <input type="text" name="email" class="form-control" id="email"
                       value="{{ isset($item->email) ? $item->email : old("email") }}"
                       placeholder="{{ trans("training.email")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="type_training">{{ trans("training.type_training")}}
                    <select class="form-control" name="type_training">
                        <option value="فردي">{{getDir() == 'rtl' ? "فردي" :"Individual"}}</option>
                        <option value="منظمات">{{getDir() == 'rtl' ? "منظمات" : "Organizations"}}</option>
                    </select>
                </label>
                <label for="type_college">{{ trans("training.type_college")}}
                    <select class="form-control" name="type_college">
                        <option value="حكومي">{{getDir() == 'rtl' ? "حكومي" :"Governmental"}}</option>
                        <option value="خاص">{{getDir() == 'rtl' ? "خاص" : "Special"}}</option>
                    </select>
                </label>
                {{--<label for="type_training">{{ trans("training.type_training")}}</label>--}}
                {{--<input type="text" name="type_training" class="form-control" id="type_training" value="{{ isset($item->type_training) ? $item->type_training : old("type_training") }}"  placeholder="{{ trans("training.type_training")}}">--}}
            </div>
            <div class="form-group">
                <label for="departments">{{ trans("training.departments")}}</label>
                <input type="text" name="departments" class="form-control" id="departments"
                       value="{{ isset($item->departments) ? $item->departments : old("departments") }}"
                       placeholder="{{ trans("training.departments")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="competence">{{ trans("training.competence")}}</label>
                <textarea name="competence" class="form-control" id="competence"
                          placeholder="{{ trans("training.competence")}}">{{ isset($item->competence) ? $item->competence : old("competence") }}</textarea>
                </label>
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.training') }}
                </button>
            </div>
        </form>
    </div>
@endsection
