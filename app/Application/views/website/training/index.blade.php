@extends('layouts.app')

@section('title')
    {{ trans('training.training') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.training') }}</h1></div>
        <div><a href="{{ url('training/item') }}" class="btn btn-default"><i
                        class="fa fa-plus"></i> {{ trans('website.training') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("training.request_status") }}</th>
                <th>{{ trans("training.name") }}</th>
                <th>{{ trans("training.university") }}</th>
                <th>{{ trans("training.college") }}</th>
                <th>{{ trans("training.specialty") }}</th>
                <th>{{ trans("training.phone") }}</th>
                <th>{{ trans("training.email") }}</th>
                <th>{{ trans("training.type_training") }}</th>
                <th>{{ trans("training.departments") }}</th>
                <th>{{ trans("training.competence") }}</th>
                <th>{{ trans("training.edit") }}</th>
                <th>{{ trans("training.show") }}</th>
                <th>{{ trans("training.delete") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit($d->request_status , 20) }}</td>
                        <td>{{ str_limit($d->name , 20) }}</td>
                        <td>{{ str_limit($d->university , 20) }}</td>
                        <td>{{ str_limit($d->college , 20) }}</td>
                        <td>{{ str_limit($d->specialty , 20) }}</td>
                        <td>{{ str_limit($d->phone , 20) }}</td>
                        <td>{{ str_limit($d->email , 20) }}</td>
                        <td>{{ str_limit($d->type_training , 20) }}</td>
                        <td>{{ str_limit($d->departments , 20) }}</td>
                        <td>{{ str_limit($d->competence , 20) }}</td>
                        <td>@include("website.training.buttons.edit", ["id" => $d->id ])</td>
                        <td>@include("website.training.buttons.view", ["id" => $d->id ])</td>
                        <td>@include("website.training.buttons.delete", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
