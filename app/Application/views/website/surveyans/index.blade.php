@extends('layouts.app')

@section('title')
     {{ trans('surveyans.surveyans') }} {{ trans('home.control') }}
@endsection

@section('content')
<div class="container">
    <div><h1>{{ trans('website.surveyans') }}</h1></div>
     <div><a href="{{ url('surveyans/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.surveyans') }}</a><br></div>
 <table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>{{ trans("surveyans.answers") }}</th>
				<th>{{ trans("surveyans.edit") }}</th>
				<th>{{ trans("surveyans.show") }}</th>
				<th>{{ trans("surveyans.delete") }}</th>
				</thead>
		<tbody>
		@if(count($items) > 0)
			@foreach($items as $d)
				<tr>
					<td>{{ str_limit($d->answers , 20) }}</td>
				<td>@include("website.surveyans.buttons.edit", ["id" => $d->id ])</td>
					<td>@include("website.surveyans.buttons.view", ["id" => $d->id ])</td>
					<td>@include("website.surveyans.buttons.delete", ["id" => $d->id ])</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	@include("layouts.paginate" , ["items" => $items])
		
</div>
@endsection
