@extends('layouts.app')

@section('title')
    {{ trans('surveyans.surveyans') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
<div class="container">
         @include('layouts.messages')
         <a href="{{ url('surveyans') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('surveyans/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
             		<div class="form-group">
			<label for="answers">{{ trans("surveyans.answers")}}</label>
				<input type="text" name="answers" class="form-control" id="answers" value="{{ isset($item->answers) ? $item->answers : old("answers") }}"  placeholder="{{ trans("surveyans.answers")}}">
			</label>
		</div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.surveyans') }}
                </button>
            </div>
        </form>
</div>
@endsection
