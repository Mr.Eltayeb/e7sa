@extends('layouts.app')

@section('title')
    {{ trans('surveyans.surveyans') }} {{ trans('home.view') }}
@endsection

@section('content')
<div class="container">
        <a href="{{ url('surveyans') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("surveyans.answers") }}</th>
				@php $type =  getFileType("answers" , $item->answers) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->answers) }}">{{ $item->answers }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->answers) }}" /></td>
				@else
					<td>{{ nl2br($item->answers) }}</td>
				@endif</tr>
			</tr>
		</table>

        @include('website.surveyans.buttons.delete' , ['id' => $item->id])
        @include('website.surveyans.buttons.edit' , ['id' => $item->id])
</div>
@endsection
