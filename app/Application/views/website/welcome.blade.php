@extends('layouts.app')
{{--@extends('layouts.mod')--}}
@push('css')
<style>
    .margin {
        margin-top: 100px;
        margin-bottom: 100px;
    }
</style>
@endpush
@section('content')
    {{--@section('mod')--}}


    <!-- الشريط المتحرك و الكبري -->

    <div data-in="lightSpeedIn" style=" margin-top: -2%;">
        <!--<div class="row ">-->
        <!--</div>-->
        <div class="row  {{getDir() == 'rtl' ? 'containerBKRTL' : 'containerBK'}}"
             style="background-image: url({{asset('image/barg3.jpg')}});">
            <div class='pull-left col-md-8'>

                <div class="carousel slide media-carousel" id="media" dir="ltr">
                    <div class="carousel-inner" dir="ltr">
                        <div hidden {{$isActive = false}}></div>
                        @foreach($slider as $sil)
                            @if (!$isActive)
                                <div class="item  active" style="margin-bottom: 0%;">
                                    <div class="row">
                                        <div class="col-md-5 {{getDir() == 'rtl' ? 'col-md-offset-8' : ''}}">
                                            <a class="thumbnail" style="background-color: transparent"
                                               href="{{getDefaultValueKey($sil->url)}}"><img
                                                        src="{{asset('/files//')}}/{{$sil->image}}"></a>
                                        </div>
                                    </div>
                                </div>
                                <div hidden {{$isActive = true}}></div>
                            @else
                                <div class="item" style="margin-bottom: 0%;">
                                    <div class="row">
                                        <div class="col-md-5 {{getDir() == 'rtl' ? 'col-md-offset-8' : ''}}">
                                            <a class="thumbnail" style="background-color: transparent"
                                               href="{{getDefaultValueKey($sil->url)}}"><img
                                                        src="{{asset('/files//')}}/{{$sil->image}}"></a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </div>
                    <a data-slide="prev" href="#media"
                       class="{{getDir() == 'rtl' ? 'left carousel-control' : 'left carousel-control'}}">‹</a>
                    <a data-slide="next" href="#media" class="carousel-control">›</a>

                </div>
            </div>
        </div>
    </div>
    <!-- نهاية الشريط المتحرك -->



    <!--الرسوم الايضاحية-->
    <div dir="ltr" class="infoG">

        <img class="{{getDir() == 'rtl' ? "pull-right" : "pull-left"}}" width="285" height="42"
             src="{{getDir() == 'rtl' ? asset('image/lab/info.PNG') : asset('image/lab/en/info.PNG')}}"
             style="width: 200px; margin-top: 0%">

        <div class="accordian lightSpeedIn">
            <ul>
                @foreach($infoG as $ig)
                    <li>
                        <div class="image_title">
                            <a href="{{getDefaultValueKey($ig->url)}}">{{getDefaultValueKey($ig->title)}}</a>
                        </div>
                        <a href="{{getDefaultValueKey($ig->url)}}">
                            <img src="{{asset('/files/')}}/{{$ig->image}}">
                        </a>
                    </li>

                @endforeach
            </ul>
        </div>

    </div>


    <!--الاخبار-->

    <div class="ticker-container" style="margin-top: -5%">
        <div class="ticker-caption">
            <p>{{getDir() == 'rtl' ? "الأخبار" : "NEWS"}}</p>
        </div>
        <ul>
            @foreach($d as $ne)
                <div>
                    <li><span>{{getDefaultValueKey($ne->title)}} &ndash; <a
                                    href="{{getDefaultValueKey($ne->url)}}">{{getDir() == 'rtl' ? "عرض المزيد" : "Read More"}}</a></span>
                    </li>
                </div>

            @endforeach
        </ul>
    </div>
    <!--نهاية الاخبار-->


    <!--الاصدارات-->
    <br>
    <img class="{{getDir() == 'rtl' ? "pull-right" : "pull-left"}}" width="285" height="42"
         src="{{getDir() == 'rtl' ? asset('image/lab/3.PNG') : asset('image/lab/en/3.PNG')}}"
         style="width: 200px; margin-top: 0%">


    <div class="container">
        <div class='row'>
            <div class='col-md-11'>
                <div class="carousel slide isdarat-carousel" id="isdarat">
                    <div class="carousel-inner">
                        <div class="item  active">
                            <div class="row">
                                @foreach($isdarat as $item)
                                    <div class="col-md-4">
                                        <a class="thumbnail" href="#"><img alt=""
                                                                           src="{{asset('files/')}}/{{$item->image}}"></a>
                                        <div class="card-body">

                                            <!-- Title -->
                                            <h4 class="card-title">{{getDefaultValueKey($item->title)}}</h4>
                                            <!-- Text -->
                                            <!-- Button -->
                                            <a href="{{asset('files/')}}/{{getDir()== 'rtl' ? "$item->file" : "$item->file_en"}}"
                                               class="btn btn-primary"><span
                                                        class="fa fa-3x fa-download"></span> {{getDir() =='rtl' ? "تنزيل" : "Download"}}
                                            </a>

                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
                                </div>
                                <div class="col-md-4">
                                    <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
                                </div>
                                <div class="col-md-4">
                                    <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
                                </div>
                                <div class="col-md-4">
                                    <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
                                </div>
                                <div class="col-md-4">
                                    <a class="thumbnail" href="#"><img alt="" src="http://placehold.it/150x150"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a data-slide="prev" href="#isdarat" class="left carousel-control">‹</a>
                    <a data-slide="next" href="#isdarat" class="right carousel-control">›</a>
                </div>
            </div>
        </div>
    </div>
    <!--نهاية الاصدارات-->


    <!--الخريطة والاستطلاهع-->
    <div class="" style="background-image: url({{asset('image/bk1.png')}});">
        <div class="row" style="margin-left: 0%;margin-right: 0%;">

            <div class="col-sm-6">
                <img class="{{getDir() == 'rtl' ? "pull-right" : "pull-left"}}" width="285" height="42"
                     src="{{getDir() == 'rtl' ? asset('image/lab/5.PNG') : asset('image/lab/en/5.PNG')}}"
                     style="width: 200px; margin-top: 0%">
                <div class="thumbnail ">
                    {{--<h3 class=" pull-right"> استطلاع</h3>--}}
                    <br>
                    <div class="caption">
                        <br>
                        <br>
                        <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))

                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div> <!-- end .flash-message -->
                        <form method="post" action="SendSurvey">
                            {{ csrf_field() }}
                            @foreach($survey as $item)
                                <label class="label {{getDir()=='rtl'? "pull-right" : "pull-left"}}"
                                       style="color: black; font-size: 14px">{{getDefaultValueKey($item->title)}}</label>
                                <br>
                                <div class="radio">
                                    <label>
                                        <input type="radio" class="pull-right"
                                               name="answers"
                                               value="{{getLangValue($item->answers1,"en")}}">{{getDefaultValueKey($item->answers1)}}
                                    </label>
                                </div>

                                <div class="radio">
                                    <label><input type="radio" name="answers"
                                                  value="{{getLangValue($item->answers2,"en")}}">{{getDefaultValueKey($item->answers2)}}
                                    </label>
                                </div>
                                <div class="radio disabled">
                                    <label><input type="radio" name="answers"
                                                  value="{{getLangValue($item->answers3,"en")}}">{{getDefaultValueKey($item->answers3)}}
                                    </label>
                                </div>
                            @endforeach
                            <input type="submit" class="btn btn-success"></input>
                            <br>
                            <br>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <img class="{{getDir()=='rtl'? "pull-right" : "pull-left"}}" width="285" height="42"
                     src="{{getDir() == 'rtl' ? asset('image/lab/4.PNG') : asset('image/lab/en/map.PNG')}}"
                     style="width: 200px; margin-top: 0%">


                <div class="card" style="
    background-color: #f5f8fa;
">
                    <div class="card-body">
                        {{--<img id="zoom_07" src="{{asset('image/lab/map.jpg')}}"--}}
                        {{--data-zoom-image="{{asset('image/map/map.jpg')}}"--}}
                        {{--style=" width: 49%;--}}
                        {{--"/>--}}
                        <div id="map" style="width:100%;height:200px;"></div>
                        <script>

                            function myMap() {
                                var myLatLng = {lat: 15.5421327, lng: 32.5841866};

                                var map = new google.maps.Map(document.getElementById('map'), {
                                    zoom: 17,
                                    center: myLatLng,
                                    styles: [
                                        {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                                        {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                                        {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                                        {
                                            featureType: 'administrative.locality',
                                            elementType: 'labels.text.fill',
                                            stylers: [{color: '#d59563'}]
                                        },
                                        {
                                            featureType: 'poi',
                                            elementType: 'labels.text.fill',
                                            stylers: [{color: '#d59563'}]
                                        },
                                        {
                                            featureType: 'poi.park',
                                            elementType: 'geometry',
                                            stylers: [{color: '#263c3f'}]
                                        },
                                        {
                                            featureType: 'poi.park',
                                            elementType: 'labels.text.fill',
                                            stylers: [{color: '#6b9a76'}]
                                        },
                                        {
                                            featureType: 'road',
                                            elementType: 'geometry',
                                            stylers: [{color: '#38414e'}]
                                        },
                                        {
                                            featureType: 'road',
                                            elementType: 'geometry.stroke',
                                            stylers: [{color: '#212a37'}]
                                        },
                                        {
                                            featureType: 'road',
                                            elementType: 'labels.text.fill',
                                            stylers: [{color: '#9ca5b3'}]
                                        },
                                        {
                                            featureType: 'road.highway',
                                            elementType: 'geometry',
                                            stylers: [{color: '#746855'}]
                                        },
                                        {
                                            featureType: 'road.highway',
                                            elementType: 'geometry.stroke',
                                            stylers: [{color: '#1f2835'}]
                                        },
                                        {
                                            featureType: 'road.highway',
                                            elementType: 'labels.text.fill',
                                            stylers: [{color: '#f3d19c'}]
                                        },
                                        {
                                            featureType: 'transit',
                                            elementType: 'geometry',
                                            stylers: [{color: '#2f3948'}]
                                        },
                                        {
                                            featureType: 'transit.station',
                                            elementType: 'labels.text.fill',
                                            stylers: [{color: '#d59563'}]
                                        },
                                        {
                                            featureType: 'water',
                                            elementType: 'geometry',
                                            stylers: [{color: '#17263c'}]
                                        },
                                        {
                                            featureType: 'water',
                                            elementType: 'labels.text.fill',
                                            stylers: [{color: '#515c6d'}]
                                        },
                                        {
                                            featureType: 'water',
                                            elementType: 'labels.text.stroke',
                                            stylers: [{color: '#17263c'}]
                                        }
                                    ]
                                });

                                var marker = new google.maps.Marker({
                                    position: myLatLng,
                                    map: map,
                                    title: 'Hello World!'
                                });
                            }
                        </script>


                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqAtldbyZm_IWwc29E2XLkJS2HGWzVxIU&callback=myMap"></script>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--نهايةالخريطة والاستطلاهع-->

@endsection
