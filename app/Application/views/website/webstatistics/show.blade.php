@extends('layouts.app')

@section('title')
    {{ trans('webstatistics.webstatistics') }} {{ trans('home.view') }}
@endsection

@section('content')

    <div class="container">
        <a href="{{ url('webstatistics') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("webstatistics.title") }}</th>

                <td>@if(getDir() == 'rtl')
                        عرض
                    @else
                        Show
                    @endif
                </td>
            </tr>
            <div hidden>
                {!! $arrs= \App\Application\Model\Webstatisticsadd::where('webstatistics_id',$item->id)->select('title','id')->get() !!}
            </div>
            <tr>
                @foreach($arrs as $arr)
                    <td>
                        {{--<button  ></button>--}}
                        <a href="{{asset('/webstatisticsadd/')}}/{{$arr->id}}/view"
                           class="btn btn-info">{{getDefaultValueKey(nl2br($arr->title))}}</a>
                    </td>
                    <td>
                        <a href="{{ url('webstatisticsadd/'.$arr->id. '/view') }}" class="btn btn-warning">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                @endforeach
            </tr>
        </table>


    </div>
@endsection
