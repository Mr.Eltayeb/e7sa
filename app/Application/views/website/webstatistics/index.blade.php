@extends('layouts.app')

@section('title')
    {{ trans('webstatistics.webstatistics') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.webstatistics') }}</h1></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("webstatistics.title") }}</th>
                <th>{{ trans("webstatistics.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title)) }}</td>
                        <td>@include("website.webstatistics.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
