@extends('layouts.app')

@section('title')
    {{ trans('requestdata.requestdata') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    <div class="container" xmlns="http://www.w3.org/1999/html">
        @include('layouts.messages')
        <a href="{{ url('requestdata') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('requestdata/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="name">{{ trans("requestdata.name")}}</label>
                <input type="text" name="name" class="form-control" id="name"
                       value="{{ isset($item->name) ? $item->name : old("name") }}"
                       placeholder="{{ trans("requestdata.name")}}">
            </div>

            <div class="form-group form-group col-md-6">
                <label for="type_identity">{{ trans("requestdata.type_identity")}}</label>
                <select class="form-control" id="type_identity" name="type_identity">
                    <option value="National number">{{getDir() == 'rtl' ? "رقم وطني" :"National number"}}</option>
                    <option value="ID card">{{getDir() == 'rtl' ? "بطاقة شخصية" :"ID card"}}</option>
                    <option value="passport">{{getDir() == 'rtl' ? "جواز سفر" :"Passport"}}</option>
                    <option value="Driving License">{{getDir() == 'rtl' ? "رخصة قيادة" :"Driving License"}}</option>
                    >
                </select>
                {{--<label for="type_identity">{{ trans("requestdata.type_identity")}}</label>--}}
                {{--<input type="text" name="type_identity" class="form-control" id="type_identity"--}}
                {{--value="{{ isset($item->type_identity) ? $item->type_identity : old("type_identity") }}"--}}
                {{--placeholder="{{ trans("requestdata.type_identity")}}">--}}

            </div>
            <div class="form-group form-group col-md-6">
                <label for="Identity">{{ trans("requestdata.Identity")}}</label>
                <input type="text" name="Identity" class="form-control" id="Identity"
                       value="{{ isset($item->Identity) ? $item->Identity : old("Identity") }}"
                       placeholder="{{ trans("requestdata.Identity")}}">
                </label>
            </div>
            <div class="form-group form-group col-md-6">
                <label for="occupation">{{ trans("requestdata.occupation")}}</label>
                <input type="text" name="occupation" class="form-control" id="occupation"
                       value="{{ isset($item->occupation) ? $item->occupation : old("occupation") }}"
                       placeholder="{{ trans("requestdata.occupation")}}">
                </label>
            </div>
            <div class="form-group form-group col-md-6">
                <label for="qualification">{{ trans("requestdata.qualification")}}</label>
                <input type="text" name="qualification" class="form-control" id="qualification"
                       value="{{ isset($item->qualification) ? $item->qualification : old("qualification") }}"
                       placeholder="{{ trans("requestdata.qualification")}}">
                </label>
            </div>
            <div class="form-group form-group col-md-6">
                <label for="phone">{{ trans("requestdata.phone")}}</label>
                <input type="text" name="phone" class="form-control" id="phone"
                       value="{{ isset($item->phone) ? $item->phone : old("phone") }}"
                       placeholder="{{ trans("requestdata.phone")}}">
                </label>
            </div>
            <div class="form-group form-group col-md-6">
                <label for="fax">{{ trans("requestdata.fax")}}</label>
                <input type="text" name="fax" class="form-control" id="fax"
                       value="{{ isset($item->fax) ? $item->fax : old("fax") }}"
                       placeholder="{{ trans("requestdata.fax")}}">
            </div>
            <div class="form-group">
                <label for="email">{{ trans("requestdata.email")}}</label>
                <input type="text" name="email" class="form-control" id="email"
                       value="{{ isset($item->email) ? $item->email : old("email") }}"
                       placeholder="{{ trans("requestdata.email")}}">
            </div>

            <div class="form-group form-group col-md-2">
                <label for="data_usage">{{ trans("requestdata.data_usage")}}</label>
                <select class="form-control" id="data_usage" name="data_usage">
                    <option value="فردي">{{getDir() == 'rtl' ? "فردي" :"Individually"}}</option>
                    <option value="مؤسسة">{{getDir() == 'rtl' ? "مؤسسة" :"company"}}</option>
                </select>
                </label>

            </div>
            {{--<label for="data_usage">{{ trans("requestdata.data_usage")}}</label>--}}
            {{--<input type="text" name="data_usage" class="form-control" id="data_usage"--}}
            {{--value="{{ isset($item->data_usage) ? $item->data_usage : old("data_usage") }}"--}}
            {{--placeholder="{{ trans("requestdata.data_usage")}}">--}}
            {{--</div>--}}
            <div class="form-group col-md-10">
                <label for="company_name">{{ trans("requestdata.company_name")}}</label>
                <input type="text" name="company_name" class="form-control" id="company_name"
                       value="{{ isset($item->company_name) ? $item->company_name : old("company_name") }}"
                       placeholder="{{ trans("requestdata.company_name")}}">
                </label>

            </div>
            {{--Governmental organizations.--}}
            {{--Family.--}}
            {{--Private sector.--}}
            {{--Foreign.--}}
            {{--Studies / Research--}}
            <div class="form-group col-md-6">
                <label for="type_Company">{{ trans("requestdata.type_Company")}}</label>
                <select class="form-control" id="type_Company" name="type_Company">
                    <option value="حكومية">{{getDir() == 'rtl' ? "حكومية" :"Governmental organizations"}}</option>
                    <option value="أهلية">{{getDir() == 'rtl' ? "أهلية" :"Family"}}</option>
                    <option value="قطاع خاص">{{getDir() == 'rtl' ? "قطاع خاص" :"Private sector"}}</option>
                    <option value="أجنبية">{{getDir() == 'rtl' ? "أجنبية" :"Foreign"}}</option>
                    <option value="دراسات/ابحاث">{{getDir() == 'rtl' ? "دراسات/ابحاث" :"Studies / Research"}}</option>
                </select>
                </label>

                {{--<label for="type_Company">{{ trans("requestdata.type_Company")}}</label>--}}
                {{--<input type="text" name="type_Company" class="form-control" id="type_Company"--}}
                {{--value="{{ isset($item->type_Company) ? $item->type_Company : old("type_Company") }}"--}}
                {{--placeholder="{{ trans("requestdata.type_Company")}}">--}}
            </div>
            <br>
            <div class="form-group col-md-6">
                <label for="Data_usage_area">{{ trans("requestdata.Data_usage_area")}}</label>
                <select class="form-control" id="Data_usage_area" name="Data_usage_area">
                    <option value="رسالة دكتوراه">{{getDir() == 'rtl' ? "رسالة دكتوراه" :"Ph.D"}}</option>
                    <option value="رسالة ماجستير">{{getDir() == 'rtl' ? "رسالة ماجستير" :"Master Thesis"}}</option>
                    <option value="رسالة بحث علمي">{{getDir() == 'rtl' ? "رسالة بحث علمي" :"Scientific research message"}}</option>
                </select>
                </label>

            </div>
            <div class="form-group col-md-10">
                <label for="data_details">{{ trans("requestdata.data_details")}}</label>
                <textarea name="data_details" class="form-control" id="data_details"
                          placeholder="{{ trans("requestdata.data_details")}}">{{ isset($item->data_details) ? $item->data_details : old("data_details") }}</textarea>
                </label>

            </div>
            <div class="form-group">
                <label>بالضغط علي موافق فانت تقر بالبنود والشروط</label>
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.requestdata') }}
                </button>
            </div>
        </form>
    </div>
@endsection
