@extends('layouts.app')

@section('title')
    {{ trans('requestdata.requestdata') }} {{ trans('home.view') }}
@endsection

@section('content')
<div class="container">
        <a href="{{ url('requestdata') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("requestdata.name") }}</th>
				@php $type =  getFileType("name" , $item->name) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->name) }}">{{ $item->name }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->name) }}" /></td>
				@else
					<td>{{ nl2br($item->name) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.type_identity") }}</th>
				@php $type =  getFileType("type_identity" , $item->type_identity) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->type_identity) }}">{{ $item->type_identity }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->type_identity) }}" /></td>
				@else
					<td>{{ nl2br($item->type_identity) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.Identity") }}</th>
				@php $type =  getFileType("Identity" , $item->Identity) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->Identity) }}">{{ $item->Identity }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->Identity) }}" /></td>
				@else
					<td>{{ nl2br($item->Identity) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.occupation") }}</th>
				@php $type =  getFileType("occupation" , $item->occupation) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->occupation) }}">{{ $item->occupation }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->occupation) }}" /></td>
				@else
					<td>{{ nl2br($item->occupation) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.qualification") }}</th>
				@php $type =  getFileType("qualification" , $item->qualification) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->qualification) }}">{{ $item->qualification }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->qualification) }}" /></td>
				@else
					<td>{{ nl2br($item->qualification) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.phone") }}</th>
				@php $type =  getFileType("phone" , $item->phone) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->phone) }}">{{ $item->phone }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->phone) }}" /></td>
				@else
					<td>{{ nl2br($item->phone) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.fax") }}</th>
				@php $type =  getFileType("fax" , $item->fax) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->fax) }}">{{ $item->fax }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->fax) }}" /></td>
				@else
					<td>{{ nl2br($item->fax) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.email") }}</th>
				@php $type =  getFileType("email" , $item->email) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->email) }}">{{ $item->email }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->email) }}" /></td>
				@else
					<td>{{ nl2br($item->email) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.data_usage") }}</th>
				@php $type =  getFileType("data_usage" , $item->data_usage) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->data_usage) }}">{{ $item->data_usage }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->data_usage) }}" /></td>
				@else
					<td>{{ nl2br($item->data_usage) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.company_name") }}</th>
				@php $type =  getFileType("company_name" , $item->company_name) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->company_name) }}">{{ $item->company_name }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->company_name) }}" /></td>
				@else
					<td>{{ nl2br($item->company_name) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.type_Company") }}</th>
				@php $type =  getFileType("type_Company" , $item->type_Company) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->type_Company) }}">{{ $item->type_Company }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->type_Company) }}" /></td>
				@else
					<td>{{ nl2br($item->type_Company) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.Data_usage_area") }}</th>
				@php $type =  getFileType("Data_usage_area" , $item->Data_usage_area) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->Data_usage_area) }}">{{ $item->Data_usage_area }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->Data_usage_area) }}" /></td>
				@else
					<td>{{ nl2br($item->Data_usage_area) }}</td>
				@endif</tr>
				<tr><th>{{ trans("requestdata.data_details") }}</th>
				@php $type =  getFileType("data_details" , $item->data_details) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->data_details) }}">{{ $item->data_details }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->data_details) }}" /></td>
				@else
					<td>{{ nl2br($item->data_details) }}</td>
				@endif</tr>
			</tr>
		</table>

        @include('website.requestdata.buttons.delete' , ['id' => $item->id])
        @include('website.requestdata.buttons.edit' , ['id' => $item->id])
</div>
@endsection
