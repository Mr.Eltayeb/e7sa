@extends('layouts.app')

@section('title')
    {{ trans('webstate.webstate') }} {{ trans('home.view') }}
@endsection

@section('content')
    <div class="container">
        <a href="{{ url('webstate') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("webstate.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webstate.image") }}</th>
                <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
            </tr>
            <tr>
                <th>{{ trans("webstate.body") }}</th>

                <td>{!! getDefaultValueKey(nl2br($item->body)) !!}</td>
            </tr>
            <tr>
                <th width="200">{{ trans("webstate.file") }}</th>
                <td>
                    @isset($item)
                        @if(json_decode($item->file))
                            <input type="hidden" name="oldFiles_file" value="{{ $item->file }}">
                            @php $files = returnFilesImages($item , "file"); @endphp
                            <div class="row text-center">
                                @foreach($files["image"] as $jsonimage )
                                    <div class="col-lg-2 text-center"><img src="{{ small($jsonimage) }}" width="100"/>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row text-center">
                                @foreach($files["file"] as $jsonimage )
                                    <div class="col-lg-2 text-center"><a
                                                href="{{ url(env("UPLOAD_PATH")."/".$jsonimage) }}"> {{trans("webstate.title")}}
                                            <i
                                                    class="fa fa-file"></i></a>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    @endisset
                </td>
            </tr>
        </table>

        {{--        @include('website.webstate.buttons.delete' , ['id' => $item->id])--}}
        {{--        @include('website.webstate.buttons.edit' , ['id' => $item->id])--}}
    </div>
@endsection
