@extends('layouts.app')

@section('title')
    {{ trans('webstate.webstate') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.webstate') }}</h1></div>
        {{--<div><a href="{{ url('webstate/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.webstate') }}</a><br></div>--}}
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th width="100">{{ trans("webstate.title") }}</th>
                <th width="100">{{ trans("webstate.image") }}</th>
                <th width="50">{{ trans("webstate.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>
                            <img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}" width="80"/>
                        </td>
                        <td>{!!  str_limit(getDefaultValueKey($d->body) , 40) !!}</td>
                        <td>@include("website.webstate.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
