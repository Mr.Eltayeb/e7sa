@extends('layouts.app')

@section('title')
    {{ trans('newsletter.newsletter') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.newsletter') }}</h1></div>
        <div><a href="{{ url('newsletter/item') }}" class="btn btn-default"><i
                        class="fa fa-plus"></i> {{ trans('website.newsletter') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("newsletter.title") }}</th>
                <th>{{ trans("newsletter.image") }}</th>
                <th>{{ trans("newsletter.file") }}</th>
                <th>{{ trans("newsletter.file_en") }}</th>
                <th>{{ trans("newsletter.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>
                            <img src="{{ url(env("UPLOAD_PATH")."/".$d->image)}}" width="80"/>
                        </td>
                        <td>
                            <a src="{{ url(env("UPLOAD_PATH")."/".$d->file)}}"><span style="color: brown"
                                                                                     class="fa fa-3x fa-download"></span></a>
                        </td>
                        <td>
                            <a src="{{ url(env("UPLOAD_PATH")."/".$d->file_en)}}"><span style="color: #f0c54c"
                                                                                        class="fa fa-3x fa-download"></span></a>
                        </td>
                        <td>@include("website.newsletter.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
