@extends('layouts.app')

@section('title')
    {{ trans('newsletter.newsletter') }} {{ trans('home.view') }}
@endsection

@section('content')
    <div class="container">
        <a href="{{ url('newsletter') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("newsletter.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("newsletter.image") }}</th>

                <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
            </tr>
            <tr>
                <th>{{ trans("newsletter.file") }}</th>

                <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file) }}"><span style="color: brown"
                                                                                  class="fa fa-3x fa-download"></span></a>
                </td>
            </tr>
            <tr>
                <th>{{ trans("newsletter.file_en") }}</th>
                <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"><span style="color: #f1a417" class="fa fa-3x fa-download"></span></a></td>
            </tr>
        </table>


    </div>
@endsection
