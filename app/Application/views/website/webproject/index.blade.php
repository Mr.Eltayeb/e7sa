@extends('layouts.app')

@section('title')
    {{ trans('webproject.webproject') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.webproject') }}</h1></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("webproject.title") }}</th>
                <th>{{ trans("webproject.body") }}</th>
                <th>{{ trans("webproject.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>{!!  str_limit(getDefaultValueKey($d->body) , 40) !!}</td>
                        <td>@include("website.webproject.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
