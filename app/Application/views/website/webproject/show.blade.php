@extends('layouts.app')

@section('title')
    {{ trans('webproject.webproject') }} {{ trans('home.view') }}
@endsection

@section('content')
    <div class="container">
        <a href="{{ url('webproject') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("webproject.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webproject.body") }}</th>

                <td>{!!  getDefaultValueKey(nl2br($item->body)) !!}</td>
            </tr>
        </table>

    </div>
@endsection
