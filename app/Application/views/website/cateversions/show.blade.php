@extends('layouts.app')

@section('title')
    {{ trans('cateversions.cateversions') }} {{ trans('home.view') }}
@endsection

@section('content')
    <div class="container">
        <a href="{{ url('cateversions') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            </tr>
            <tr>
                <th>{{ trans("cateversions.title") }}</th>
                @php $type =  getFileType("title" , $item->title) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                @endif</tr>
            </tr>
            <div hidden>
                {!! $arrs= \App\Application\Model\Versions::where('cateversions_id',$item->id)->get() !!}
            </div>
            <tr>
                @foreach($arrs as $arr)
                    <td>
                        {{--<button  ></button>--}}
                        <a href="{{asset('/webstatisticsadd/')}}/{{$arr->id}}/view"
                           class="btn btn-info">{{getDefaultValueKey(nl2br($arr->title))}}</a>
                    </td>
                    <td>
                        <a href="{{ url('webstatisticsadd/'.$arr->id. '/view') }}" class="btn btn-warning">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                @endforeach
            </tr>
        </table>

    </div>
@endsection
