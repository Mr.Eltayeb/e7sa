		<tr>
			<th>
			{{ trans( "webstatistics.webstatistics") }}
			</th>
			<td>
				@php $webstatistics = App\Application\Model\Webstatistics::find($item->webstatistics_id);  @endphp
				{{ is_json($webstatistics->title) ? getDefaultValueKey($webstatistics->title) :  $webstatistics->title}}
			</td>
		</tr>
