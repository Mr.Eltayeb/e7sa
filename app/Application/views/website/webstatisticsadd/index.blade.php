@extends('layouts.app')

@section('title')
    {{ trans('webstatisticsadd.webstatisticsadd') }} {{ trans('home.control') }}
@endsection

@section('content')
    <div class="container">
        <div><h1>{{ trans('website.webstatisticsadd') }}</h1></div>
        <div><a href="{{ url('webstatisticsadd/item') }}" class="btn btn-default"><i
                        class="fa fa-plus"></i> {{ trans('website.webstatisticsadd') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("webstatisticsadd.title") }}</th>
                <th>{{ trans("webstatisticsadd.file_ar") }}</th>
                <th>{{ trans("webstatisticsadd.file_en") }}</th>
                <th>{{ trans("webstatisticsadd.show") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
                        <td>
                            <a href="{{ getDefaultValueKey($d->file_ar)  }}"><span style="color:#f1a417;" class="fa fa-download"></span></a>
                        </td>
                        <td>
                            <a href="{{ getDefaultValueKey($d->file_ar)  }}"><span style="color: #f0ad4e" class="fa fa-download"></span></a>
                        </td>
                        <td>@include("website.webstatisticsadd.buttons.view", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        @include("layouts.paginate" , ["items" => $items])

    </div>
@endsection
