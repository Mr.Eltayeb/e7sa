@extends('layouts.app')
@section('title')
    {{ trans('webstatisticsadd.webstatisticsadd') }} {{ trans('home.view') }}
@endsection
@section('content')
    <div class="container">
        <a href="{{ url('webstatisticsadd') }}" class="btn btn-danger"><i
                    class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <table class="table table-bordered table-responsive table-striped">
            @include("website.webstatisticsadd.relation.webstatistics.show")

            <tr>
                <th style="color: black; font-size: 13px">{{ trans("webstatisticsadd.title") }}</th>
                <td style="color: black; font-size: 13px">{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                =
            </tr>
            <tr>
                <th style="color: black; font-size: 13px">{{ trans("webstatisticsadd.body") }}</th>
                <td>{!!  getDefaultValueKey(nl2br($item->body)) !!}</td>
                =
            </tr>
            <tr style="color: black; font-size: 13px">
                <th style="color: black; font-size: 13px">{{ trans("webstatisticsadd.file_ar") }}</th>
                @php $type =  getFileType("file_ar" , $item->file_ar) @endphp
                <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_ar) }}"><i class="fa fa-download"
                                                                                  style="font-size: x-large; color: #00b0e4"></i>
                    </a></td>
            </tr>
            <tr style="color: black; font-size: 13px">
                <th style="color: black; font-size: 13px">{{ trans("webstatisticsadd.file_en") }}</th>

                <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"><i class="fa fa-download"
                                                                                  style="font-size: x-large;color: #ff3b11"></i></a>
                </td>
            </tr>
            <
        </table>
    </div>
@endsection
