@extends('layouts.app')

@section('title')
    {{ trans('survey.survey') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
<div class="container">
         @include('layouts.messages')
         <a href="{{ url('survey') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
        <form action="{{ concatenateLangToUrl('survey/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
             		<div class="form-group">
			<label for="title">{{ trans("survey.title")}}</label>
				{!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "survey") !!}
			</label>
		</div>
		<div class="form-group">
			<label for="answers1">{{ trans("survey.answers1")}}</label>
				{!! extractFiled("answers1" , isset($item->answers1) ? $item->answers1 : old("answers1") , "text" , "survey") !!}
			</label>
		</div>
		<div class="form-group">
			<label for="answers2">{{ trans("survey.answers2")}}</label>
				{!! extractFiled("answers2" , isset($item->answers2) ? $item->answers2 : old("answers2") , "text" , "survey") !!}
			</label>
		</div>
		<div class="form-group">
			<label for="answers3">{{ trans("survey.answers3")}}</label>
				{!! extractFiled("answers3" , isset($item->answers3) ? $item->answers3 : old("answers3") , "text" , "survey") !!}
			</label>
		</div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="fa fa-save"></i>
                    {{ trans('website.Update') }}  {{ trans('website.survey') }}
                </button>
            </div>
        </form>
</div>
@endsection
