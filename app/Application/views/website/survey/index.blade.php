@extends('layouts.app')

@section('title')
     {{ trans('survey.survey') }} {{ trans('home.control') }}
@endsection

@section('content')
<div class="container">
    <div><h1>{{ trans('website.survey') }}</h1></div>
     <div><a href="{{ url('survey/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.survey') }}</a><br></div>
 <table class="table table-responsive table-striped table-bordered">
		<thead>
			<tr>
				<th>{{ trans("survey.title") }}</th>
				<th>{{ trans("survey.answers1") }}</th>
				<th>{{ trans("survey.answers2") }}</th>
				<th>{{ trans("survey.answers3") }}</th>
				<th>{{ trans("survey.edit") }}</th>
				<th>{{ trans("survey.show") }}</th>
				<th>{{ trans("survey.delete") }}</th>
				</thead>
		<tbody>
		@if(count($items) > 0)
			@foreach($items as $d)
				<tr>
					<td>{{ str_limit(getDefaultValueKey($d->title) , 20) }}</td>
				<td>{{ str_limit(getDefaultValueKey($d->answers1) , 20) }}</td>
				<td>{{ str_limit(getDefaultValueKey($d->answers2) , 20) }}</td>
				<td>{{ str_limit(getDefaultValueKey($d->answers3) , 20) }}</td>
				<td>@include("website.survey.buttons.edit", ["id" => $d->id ])</td>
					<td>@include("website.survey.buttons.view", ["id" => $d->id ])</td>
					<td>@include("website.survey.buttons.delete", ["id" => $d->id ])</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	@include("layouts.paginate" , ["items" => $items])
		
</div>
@endsection
