@extends('layouts.app')

@section('title')
    {{ trans('survey.survey') }} {{ trans('home.view') }}
@endsection

@section('content')
<div class="container">
        <a href="{{ url('survey') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> {{ trans('website.Back') }}  </a>
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("survey.title") }}</th>
				@php $type =  getFileType("title" , $item->title) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
				@endif</tr>
				<tr><th>{{ trans("survey.answers1") }}</th>
				@php $type =  getFileType("answers1" , $item->answers1) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->answers1) }}">{{ $item->answers1 }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->answers1) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->answers1)) }}</td>
				@endif</tr>
				<tr><th>{{ trans("survey.answers2") }}</th>
				@php $type =  getFileType("answers2" , $item->answers2) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->answers2) }}">{{ $item->answers2 }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->answers2) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->answers2)) }}</td>
				@endif</tr>
				<tr><th>{{ trans("survey.answers3") }}</th>
				@php $type =  getFileType("answers3" , $item->answers3) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->answers3) }}">{{ $item->answers3 }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->answers3) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->answers3)) }}</td>
				@endif</tr>
			</tr>
		</table>

        @include('website.survey.buttons.delete' , ['id' => $item->id])
        @include('website.survey.buttons.edit' , ['id' => $item->id])
</div>
@endsection
