<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="imagetoolbar" content="no"/>
    <title>SUDAN ADMINISTRATIVE MAP</title>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script type="text/javascript"
            src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.rawgit.com/jquery/jquery-mousewheel/3.1.12/jquery.mousewheel.js"></script>

    <script type="text/javascript"
            src="https://cdn.rawgit.com/igorlino/fancybox-plus/1.3.6/src/jquery.fancybox-plus.js"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.rawgit.com/igorlino/fancybox-plus/1.3.6/css/jquery.fancybox-plus.css" media="screen"/>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{asset('css/ticker.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootsnav.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <script type="text/javascript"
            src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.17/src/jquery.ez-plus.js"></script>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.17/css/jquery.ez-plus.css" media="screen"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.17/demo/css/style.css" media="screen"/>
    <script type="text/javascript"
            src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.17/demo/js/web.js?m=20100203"></script>

<style>
    body{
        background-color: #1b6d85;
    }
</style>

    <!--[if IE 6]>
    <script src="https://cdn.rawgit.com/igorlino/DD_belatedPNG/0.0.8a/DD_belatedPNG_0.0.8a.js"></script>

    <script>
        DD_belatedPNG.fix('.png_bg');
    </script>
    <![endif]-->
    <script src="https://cdn.rawgit.com/sorccu/cufon/1.09i/js/cufon.js" type="text/javascript"></script>
    <script src="https://cdn.rawgit.com/igorlino/fancybox-plus/1.3.6/demo/js/Museo_300_300.font.js"
            type="text/javascript"></script>
    <script type="text/javascript">
        Cufon.replace('h1', {color: '#ff6347'});
    </script>

    <script type="text/javascript"
            src="https://cdn.rawgit.com/igorlino/snippet-helper/1.0.1/src/snippet-helper.js"></script>
    <link type="text/css" rel="stylesheet"
          href="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.17/demo/css/prism.css"/>
    <script type="text/javascript"
            src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.17/demo/js/prism.js"></script>
</head>
<body>
<a class="btn btn-info" style="font-size: large" href="/" > GO Back</a>
<div id="page" class="home">


    <div class="zoom-left">
        <img style="width:611px" id="img_01" width="700px" height="500px"
             src="{{asset('image/map/map.jpg')}}"
             data-zoom-image="{{asset('image/map/map.jpg')}}"
        />

        <div id="gal1" style="width:500px;float:left;">

            <a href="#" class="elevatezoom-gallery" data-update=""
               data-image="{{asset('image/map/map.jpg')}}"
               data-zoom-image="{{asset('image/map/map.jpg')}}">
                <img id="img_01"
                     src="{{asset('image/map/map.jpg')}}"
                     width="100"/></a>

        </div>
    </div>
    <div style="clear:both;"></div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#zoom_06").ezPlus({
                zoomType: "inner",
                debug: true,
                cursor: "crosshair"
            });
        });
    </script>
</div>
<script type="text/javascript">
    var snippets = [
        {code: "code-ezp-01", ext: "html,js"},
        {code: "code-ezp-02", ext: "html"},
        {code: "code-ezp-03", ext: "html,js"}
    ];
    $(document).ready(function () {
        snippetHelper.loadSnippets(snippets);
    });
</script>
</body>
</html>
