<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--    <title>{{ config('app.name', 'SADAD CP') }} | @yield('title')</title>--}}
    <title>الجهاز المركزي للإحصاء</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{asset('css/ticker.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootsnav.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/isdarat.css')}}">

    <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/droid-arabic-kufi" type="text/css"/>
    <link rel="stylesheet" href="http://cbs.gov.sd/resources/files/ar/css/responsive.css"/>

    <link rel="stylesheet" media="screen"/>
    <script src=""></script>


    <style>
        * {
            font-family: 'Droid Arabic Kufi', serif;
            font-size: 12px;
            /*direction: rtl;*/
            /*padding: 5px;*/
            /*text-align: justify;*/
            /*word-spacing: 1px;*/
        }

        .footer {
            font-family: 'Al-Jazeera-Arabic-Bold';
            /*src: url('http://nic.gov.sd/fonts/Al-Jazeera-Arabic-Bold.ttf');*/
        }

        .header-top {
            padding: 10px 0 0;
        }

        .header-top-two {
            background: #434343 none repeat scroll 0 0;
            padding-bottom: 5px;
            padding-top: 15px;
        }

        .header-top-two span {
            color: #ffffff;
            font-weight: 400;
        }

        .welcm-ht {
            position: relative;
        }

        .welcm-ht a {
            color: #434343;
        }

        .welcm-ht-two {
            color: #D3D3D3;
        }

        .social-linked {
            position: relative;
        }

        .social-linked li {
            padding: 0;
        }

        .social-linked a {
            border: 1px solid #DCDCDC;
            color: #434343;
            font-size: 14px;
            padding: 5px 9px;
            -webkit-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        .social-linked a:hover {
            color: #ffffff;
        }

        .social-linked-two {
            position: relative;
        }

        .social-linked-two li {
            padding: 0;
        }

        .social-linked-two a {
            border: 1px solid #ffffff;
            color: #ffffff;
            font-size: 14px;
            padding: 5px 9px;
            -webkit-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        .dropbtn {
            background-color: transparent;
            border: none;
            color: black;
            cursor: pointer;
        }

        .dropdown {
            position: relative;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            padding: 6px 0;
            width: 85px;
            z-index: 99;
        }

        .dropdown-content a {
            color: black;
            padding: 5px 0px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #A9A9A9;
        }

        .social-linked a {
            color: #fff;
        }

        .social-linked a:hover {
            background-color: #023368;
        }

        .list-inline a:hover {
            text-decoration: none;
        }

        body {
            background-color: white;
        }

    </style>

    @if(getDir() == 'rtl')
        <link rel="stylesheet" href="{{asset('css/bootstrap-rtl.css')}}">
    @endif
    {{ Html::style('css/sweetalert.css') }}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/css/mdb.min.css" rel="stylesheet">--}}

    <script>
        window.Laravel = '{!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};'
    </script>
    @stack('css')
</head>
<body>
@if (getDir() == "rtl")
    <div class="wrapper">
        <div class="header-top" style="margin-bottom:10px;background:#193e62;">
            <div class="container">
                <div class="row" style="">
                    <div class="col-md-4 pull-left">
                        <div class="social-linked pull-left">
                            <ul class="list-inline">
                                <li>
                                    <a href="https://www.youtube.com/channel/UCIzgURV1vGe1FtAktLt2DeA?disable_polymer=true"><i
                                                class="fa fa-youtube" style="color:#ff1c3a "></i></a></li>
                                <li><a href="https://twitter.com/CbsSudan"><i class="fa fa-twitter"
                                                                              style="color: #4eccff; "></i></a></li>
                                <li><a href="https://www.facebook.com/CBS-SUDAN-1421285614800488/"><i
                                                class="fa fa-facebook" style=" color:blue;"></i></a></li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="welcm-ht text-right">
                            <ul class="list-inline ltr_direction">
                                <li>
                                    <div class="dropdown lang-button text-center">
                                        <button style="color:#fff;" class="dropbtn"><a
                                                    href="{{asset('/en')}}"> <span><img
                                                            src="{{asset('/image/lan/english.jpg')}}"
                                                            alt=""></span> </a></button>
                                    </div>
                                </li>
                                @if (Auth::guest())
                                    <li>
                                        <a style="color:#fff;"
                                           href="{{asset('/register')}}">التسجيل</a> <span
                                                style="color:#fff;"> | </span> <a style="color:#fff;"
                                                                                  href="{{asset('/login')}}">تسجيل
                                            الدخول </a>
                                    </li>

                                @else
                                    <a style="color:#fff;">{{ Auth::user()->name }}</a>
                                    <li>

                                        <a style="color:#fff;" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                                            تسجل الخروج
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                @endif
                                <li>
                                    <a style="color:#fff;" href="{{asset('contact')}}">اتصل بنا
                                        &nbsp;&nbsp;</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="wrapper">
        <div class="header-top" style="margin-bottom:10px;background:#193e62;">
            <div class="container">
                <div class="row" style="">
                    <div class="col-md-4">
                        <div class="welcm-ht">
                            <ul class="list-inline ltr_direction">
                                <li>
                                    <div class="dropdown lang-button text-center">
                                        <button style="color:#fff;" class="dropbtn"><a
                                                    href="{{asset('/ar')}}"> <span><img
                                                            src="{{asset('/image/lan')}}/sudan.png"
                                                            alt=""></span> </a></button>

                                    </div>
                                </li>
                                @if (Auth::guest())
                                    <li>
                                        <a style="color:#fff;" href="{{asset('/register')}}">registration</a>
                                        <span style="color:#fff;"> | </span> <a style="color:#fff;"
                                                                                href="{{asset('/login')}}">Login </a>
                                    </li>

                                @else
                                    <li>
                                        <a style="color:#fff;" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                @endif
                                <li>
                                    <a style="color:#fff;" href="{{asset('contact')}}">contact us
                                        &nbsp;&nbsp;</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-offset-4 col-md-4">
                        <div class="social-linked pull-right">
                            <ul class="list-inline">
                                <li>
                                    <a href="https://www.youtube.com/channel/UCIzgURV1vGe1FtAktLt2DeA?disable_polymer=true"><i
                                                class="fa fa-youtube" style="color:#ff1c3a "></i></a></li>
                                <li><a href="https://twitter.com/CbsSudan"><i class="fa fa-twitter"
                                                                              style="color: #4eccff; "></i></a></li>
                                <li><a href="https://www.facebook.com/CBS-SUDAN-1421285614800488/"><i
                                                class="fa fa-facebook" style=" color:blue;"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<!-- الكارد الرئيسي و الشعار -->
<div class="container " style="margin-top: -0.7%;margin-bottom: 0%;width:86%">


    <div class="row "
         style="margin-left: -9%;margin-right: -9%;background-image: url({{ asset('image/bk1.png')}});height: 244px; ">
        <img class="pull-right navaLogo" src="{{ asset('image/hedar.png')}}">
{{--        <img class="pull-right navaText" src="{{ asset('image/title.png')}}">--}}

        <style>

            .div_population {
                font-family: sans-serif;
                font-size: 14px;
                background: -webkit-gradient(linear, 0% 0%, 100% 0%, from(#4088ce), to(#082748));
                letter-spacing: 3px;
                padding: 0px 3px;
                border: 1px solid #7c7e80;
                border-radius: 2px;
                display: inline-block;
                color: #fff;
                margin-left: 3px;
            }
        </style>
        <div class="pull-left" style="margin-top: 2%;margin-left: 2%;">
            <div>
                <div class="ulockd-icon text-thm2">

                    <!-- <span class="flaticon-home"></span> -->
                    {{--<img src="http://cbs.gov.sd/resources/files/ar/images/population.png" height="55"--}}
                    {{--style=" margin-top:17px;">--}}

                </div>
                <div class="ulockd-info">

                    <div style="margin-top: 34%;"><p class="population_margin"
                                                     style=" display:inline-block;font-size:20px;color:black;margin:0px 3px;margin-top: -1%; ">{{getDir()=='rtl' ? "عدد السكان الآن":"Population now"}}</p>
                        <div id="sudan_poulation_main" style="display:inline-block">
                            <div class="div_population pull-left">4</div>
                            <div class="div_population pull-left">0</div>
                            <div class="div_population pull-left">5</div>
                            <div class="div_population pull-left">0</div>
                            <div class="div_population pull-left">5</div>
                            <div class="div_population pull-left">6</div>
                            <div class="div_population pull-left">0</div>
                            <div class="div_population pull-left">7</div>
                        </div>
                    </div>


                </div>
            </div>
            {{--<p style="--}}
            {{--font-size: 25px;--}}
            {{--"><span style="background-color: #a5a4a5;border-top-right-radius: 34%;border-bottom-right-radius: 34%;color: white;">عدد السكان</span><span--}}
            {{--style="border-top-left-radius:34%;border-bottom-left-radius:34%;color: white;background-color: #282828;">:40,504,810</span>--}}
            {{--</p>--}}
        </div>


        {{--</div>--}}

    </div>

</div>

<!-- نهاية     الكارد الرئيسي و الشعار -->
<div hidden="hidden">
    {{--    {!! $ddd= \App\Application\Model\Aboutcentral::!!}--}}
    {{--    {!! $ddd= \App\Application\Model\Aboutcentral::!!}--}}
    {!! $ddd = \App\Application\Model\Aboutcentral::select('title','id')->get()!!}

    {!!$ecoCat = \App\Application\Model\Webstatistics::select('title','id')->get()!!}
    {!!$proj = \App\Application\Model\Webproject::select('title','id')->get()!!}
    {!!$vers = \App\Application\Model\Cateversions::select('title','id')->get()!!}
</div>

<div id="app">

    <nav class="navbar navbar-default navbar-sticky navbar-scrollspy bootsnav "
         style="margin-top: -2%; background-color: #193e62"
         data-minus-value-desktop="70" data-minus-value-mobile="55" data-speed="1000">
        {{--<nav class="navbar navbar-default bootsnav navaBK" style="margin-top: -2%;">--}}
        <div style="{{getDir() == 'rtl' ? "margin-right: 3%;" : "margin-left: 3%;"}}">
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                {{--<a class="navbar-brand" href="#brand"><img src="LOGO_ADDRESS" class="logo" alt=""></a>--}}
            </div>
            <!-- End Header Navigation -->

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                @if(getDir() == "rtl")
                    <div class="top-search">
                        <div class="container">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-search"></i></span>
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </div>
                    <ul class="nav navbar-nav navbar-right" data-in="bounceInDown" data-out="bounceUpDown">
                        <div class="attr-nav">
                            <ul>
                                <li class="search"><a style="font-size: 17px; color:#ffffff" href="#"><i
                                                class="fa fa-search"></i></a>
                                </li>
                            </ul>
                        </div>
                        @else
                            <ul class="nav navbar-nav" data-in="bounceInDown" data-out="bounceUpDown" style="
    font-size: 11px;width: 120%;
">

                                <li class="search"><a style="font-size: 17px; color:#ffffff" href="#"><i
                                                class="fa fa-search"></i></a>
                                </li>
                                @endif
                                <li><a href="{{asset('/')}}">
                                        @if(getDir() == "rtl")
                                            الرئيسية
                                        @else
                                            HOME
                                        @endif
                                    </a></li>


                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="slideInDown">
                                        @if(getDir() == "rtl")
                                            عن الجهاز
                                        @else
                                            ABOUT
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu ">
                                        {{--<li>--}}

                                        {{--<a href="{{asset("/")}}aboutcentral/{{$about->id}}/view">{{getDefaultValueKey($about->title)}}</a>--}}
                                        {{--</li>--}}
                                        @foreach($ddd as $about)
                                            <li>
                                                <a href="{{asset("/")}}aboutcentral/{{$about->id}}/view">{{getDefaultValueKey($about->title)}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                </li>

                                <li>
                                    <a href="{{asset('/webstate')}}">@if(getDir() == "rtl")
                                            الولايات
                                        @else
                                            STATES
                                        @endif
                                    </a></li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle lightSpeedIn" data-toggle="slideInDown">
                                        @if(getDir() == "rtl")
                                            الاحصائيات
                                        @else
                                            STATISTICS
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu ">
                                        {{--                                        @foreach($ecoCat as $item)--}}

                                        <li class="dropdown">
                                            <a class="dropdown-toggle"
                                               href="#">{{getDir() == 'rtl' ? 'الإقتصادية' : "ECONOMIC"}}</a>
                                            <ul class="dropdown-menu ">
                                                <li class="dropdown">
                                                    <a class="dropdown-toggle"
                                                       href="#">{{getDir() == 'rtl' ? 'الارقام القياسية و الاسعار' : "ECONOMIC"}}

                                                    </a>
                                                    <ul class="dropdown-menu animated"
                                                        style="display: none; opacity: 1;">


                                                        <li>
                                                            <a href="{{asset('/')}}webstatistics/1/view">
                                                                {{getDir() == 'rtl' ? "الرقم القياسي لاسعار المستهلك" : "Consumer Price Index"}}
                                                            </a>


                                                        </li>


                                                        <li>
                                                            <a href="{{asset('/')}}webstatistics/2/view">
                                                                {{getDir() == 'rtl' ? "حركة اسعار السلع الاستهلاكية والخدمية" : "Prices movement of the consumer commodities and services"}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a class="dropdown-toggle"
                                                       href="{{asset("/")}}webstatistics/3/view">{{getDir() == 'rtl' ? 'التجارة الخارجية' : "Foreign Trade"}}</a>
                                                </li>
                                                <li class="dropdown on">
                                                    <a href="{{asset('/')}}webstatistics/5/view"
                                                       class="dropdown-toggle" data-toggle="dropdown">
                                                        {{getDir() == 'rtl' ? "الحسابات القومية" : "National Accounts"}} </a>
                                                    <ul class="dropdown-menu bounceIn"
                                                        style="display: none; opacity: 1;">
                                                        <li>
                                                            <a href="{{asset('/')}}webstatistics/6/view">
                                                                {{getDir() == 'rtl' ? 'الناتج المحلى الاجمالي' : "Gross Domestic Product"}}
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="{{asset('/')}}webstatistics/7/view">
                                                                {{getDir() == 'rtl' ? 'المؤشرات الاقتصادية' : "Economics Indicators"}}
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a class="dropdown-toggle"
                                                       href="{{asset('/')}}webstatistics/8/view">{{getDir() == 'rtl' ? 'الاحصاءات القطاعية' : "ECONOMIC"}}</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown">
                                            <a class="dropdown-toggle"
                                               href="#">{{getDir() == 'rtl' ? 'السكانية والإجتماعية' : "POPULATION AND SOCIAL"}}</a>
                                            <ul class="dropdown-menu" style="display: none; opacity: 1;">
                                                <li class="dropdown">
                                                    <a href="{{asset('/')}}webstatistics/9/view">
                                                        {{getDir() == 'rtl' ? "إحصاءات السكان" : "Population statistics"}}
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="{{asset('/')}}webstatistics/10/view">
                                                        {{getDir() == 'rtl' ? "إجتماعية" : "Social"}}
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="{{asset('/')}}webstatistics/11/view">
                                                        {{getDir() == 'rtl' ? 'الصحية والحيوية' : "Health and Vital"}} </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a href="{{asset('/')}}webstatistics/12/view">
                                                        {{getDir() == 'rtl' ? "القوى العاملة والهجرة" : "Labour Force and Immigration"}}
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="{{asset('/')}}webstatistics/13/view">{{getDir() == 'rtl' ? 'التنمية المستدامة' : "SUSTAINABLE DEVELOPMENT "}}</a>
                                        </li>
                                    </ul>
                                </li>


                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle " data-toggle="slideInDown">
                                        @if(getDir() == "rtl")
                                            المشاريع الجارية
                                        @else
                                            PROJECTS
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu ">
                                        @foreach($proj as $item)

                                            <li>
                                                <a href="{{asset("/")}}webproject/{{$item->id}}/view">{{getDefaultValueKey($item->title)}}</a>
                                            </li>
                                        @endforeach

                                    </ul>
                                </li>

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle " data-toggle="slideInDown">
                                        @if(getDir() == "rtl")
                                            الاصدارات
                                        @else
                                            PUBLICATIONS
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu ">
                                        @foreach($vers as $item)

                                            <li>
                                                <a class="dropdown-toggle" data-toggle="dropdown"
                                                   href="{{asset("/")}}cateversions/{{$item->id}}/view">{{getDefaultValueKey($item->title)}}</a>

                                        @endforeach

                                    </ul>
                                </li>


                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="slideInDown">
                                        @if(getDir() == "rtl")
                                            الأدلة و التصانيف
                                        @else
                                            DIRECTORIES & CATEGORIES
                                        @endif
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{asset("/")}}directory">
                                                @if(getDir() == "rtl")
                                                    الأدلة
                                                @else
                                                    DIRECTORIES
                                                @endif
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{asset("/")}}centralcategories">
                                                @if(getDir() == "rtl")
                                                    التصانيف
                                                @else
                                                    CATEGORIES
                                                @endif
                                            </a>
                                        </li>

                                    </ul>
                                </li>


                                <li>
                                    <a href="{{asset("sudanmaps")}}">
                                        @if(getDir() == "rtl")
                                            خريطة السودان الأدارية
                                        @else
                                            ADMINISTRATIVE MAP
                                        @endif
                                    </a>
                                </li>


                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle "
                                       data-toggle="slideInDown">{{getDir() == 'rtl' ? "المركز الإعلامي" : "MEDIA CENTER"}}</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{asset('/fullnews/')}}">{{getDir() == 'rtl' ? "الأخبار" : "News"}}</a>
                                        </li>

                                        <li>
                                            <a href="{{asset('/newsletter/')}}">{{getDir() == 'rtl' ? "النشرة الإستراتيجية" : "Strategy Newsletter"}}</a>
                                        </li>
                                        <li><a href="#">{{getDir() == 'rtl' ? "معرض الصور" : "Gallery"}}</a></li>
                                        <li><a href="#">{{getDir() == 'rtl' ? "معرض الفيديو" : "Video Gallery"}}</a>
                                        </li>
                                        <li><a href="{{asset('/elibrary/')}}">{{getDir() == 'rtl' ? "المكتبة الإلكترونية" : "E-Library"}}</a>
                                        </li>
                                    </ul>
                                </li>


                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle"
                                       data-toggle="dropdown">{{getDir() == 'rtl' ? "الخدمات" : "SERVICES "}}</a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{{asset('/servicesdetails/1/view')}}">{{getDir() == 'rtl' ? "البيانات الوصفية" : "Metadata"}}</a>
                                        </li>
                                        <li>
                                            <a href="{{asset('/servicesdetails/2/view')}}">{{getDir() == 'rtl' ? "طلب بيانات إحصائية" : "Request for statistical data"}}</a>
                                        </li>
                                        <li><a href="#">{{getDir() == 'rtl' ? "طلب إجراء مسح" : "Request a survey"}}</a>
                                        </li>
                                        <li><a href="#">{{getDir() == 'rtl' ? "طلب تدريب" : "training request"}}</a>
                                        </li>
                                    </ul>
                                </li>


                            </ul>


                    </ul>
            </div><!-- /.navbar-collapse -->

        </div>

    </nav>

    @yield('content')

</div>



<script src="{{ asset('js/app.js') }}"></script>

{{--<script src="{{ asset('js/model.js') }}"></script>--}}
{{--<script src="{{ asset('js/jquery.min.js') }}"></script>--}}

{{--<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>--}}
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--}}
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function () {
//
        $('#isdarat').carousel({
            pause: true,
            interval: false,
        });

        $('#media').carousel({
            interval: 3000,
            cycle: true
        });
    });
</script>

{{--{!! Links::track(true) !!}--}}
{{ Html::script('js/sweetalert.min.js') }}
<script type="application/javascript">
    function deleteThisItem(e) {
        var link = $(e).data('link');
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Item Again!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                window.location = link;
            });
    }
</script>
@include('sweet::alert')
@stack('js')

<script src={{asset('js/ticker.js')}}></script>
<script src={{asset('js/bootsnav.js')}}></script>
<script src={{asset('js/wow.min.js')}}></script>

{{--@section('script')--}}

    {{--<script type="text/javascript">--}}
        {{--$("#zoom_07").elevateZoom({--}}
            {{--zoomType: "lens",--}}
            {{--lensShape: "round",--}}
            {{--lensSize: 200--}}
        {{--});--}}
    {{--</script>--}}

    {{--<script>--}}
        {{--$(document).ready(function (ev) {--}}
            {{--$('#custom_carousel').on('slide.bs.carousel', function (evt) {--}}
                {{--$('#custom_carousel').find('.controls li.active').removeClass('active');--}}
                {{--$('#custom_carousel').find('.controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');--}}
            {{--})--}}
        {{--});--}}
    {{--</script>--}}





    {{----}}


{{--@endsection--}}


<div class="container">
{{--<section style="height:80px;"></section>--}}
<!----------- Footer ------------>
    <footer class="footer-bs"
            @if(getDir() == 'rtl')
            style="
    width: 120%;
    margin-right: -10%;
    margin-top: 2%;"
            @else
            style="
    margin-left: -10%;
    width: 120%;
    margin-top: 2%;
"
            @endif
    >
        @if(getDir() =='rtl')
            <div class="row">
                <div class="col-md-4 footer-brand animated fadeInLeft">
                    <h2 style="color: white;font-family: Arial, sans-serif">عن الجهاز</h2>
                    <p>فى عام 1903م كانت مصلحة الإحصاء ( الجهاز المركزي للإحصاء حاليا ) قسما من أقسام مصلحة الجمارك
                        وظيفتها
                        الأساسية إعداد إحصاءات التجارة الخارجية وبحلول عام 1932م تحول قسم الإحصاء هذا الي ما كان
                        يعرف
                        باللجنة الاقتصادية التى تحولت فى عام 1934م الى مصلحة التجارة والاقتصاد .</p>
                    <p>جميع الحقوق محفوظة </p>
                </div>
                <div class="col-md-6 footer-nav animated fadeInUp">
                    <h2 style="color: white;font-family: Arial, sans-serif"><span style="color: #00b0e4"> —</span>مواقع
                        ذات
                        صلة<span style="color: #00b0e4"> —</span></h2>
                    <div class="col-md-7">
                        <ul class="pages">
                            <li><a href="http://sudan.gov.sd/index.php/ar" style="font-size: 10px">وزراة مجلس
                                    الوزراء</a>
                            </li>
                            <li><a href="#" style="font-size: 10px">المركزي القومي للمعلومات</a></li>
                            <li><a href="#" style="font-size: 10px">وزارة المالية والتخطيط الاقتصادي</a></li>
                            <li><a href="#" style="font-size: 10px">بنك السودان المركزي</a></li>
                            <li><a href="#" style="font-size: 10px">وزارة الصحة الاتحادية</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list">
                            {{--<li><a href="#">About Us</a></li>--}}
                            <li><a href="#" style="font-size: 10px;color: #ffffff;">الاتصال بنا</a></li>
                            {{--<li><a href="#">Terms & Condition</a></li>--}}
                            <li><a href="#" style="font-size: 10px;color: #ffffff;">سياسة الخصوصية</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 footer-social animated fadeInDown">
                    <h4>Follow Us</h4>
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Youtube</a></li>
                        {{--<li><a href="#">RSS</a></li>--}}
                    </ul>
                </div>
                <div class="col-md-3 footer-nse animated fadeInRight">
                    <h4>Newsletter</h4>
                    {{--<div id="googleMap" style="width:100%;height:200px;"></div>--}}

                    {{--<script>--}}

                    {{--function myMap() {--}}
                    {{--var mapProp = {--}}
                    {{--center: new google.maps.LatLng(15.6122208, 32.5390243),--}}
                    {{--zoom: 7--}}
                    {{--};--}}
                    {{--var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);--}}
                    {{--}--}}
                    {{--</script>--}}

                    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCqAtldbyZm_IWwc29E2XLkJS2HGWzVxIU&callback=myMap"></script>--}}

                    {{--<p>A rover wearing a fuzzy suit doesn’t alarm the real penguins</p>--}}
                    {{--<p>--}}
                    {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control" placeholder="Search for...">--}}
                    {{--<span class="input-group-btn">--}}
                    {{--<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-envelope"></span></button>--}}
                    {{--</span>--}}
                    {{--</div><!-- /input-group -->--}}
                    {{--</p>--}}
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-4  footer-brand animated fadeInLeft">
                    <h2 style="color: white;font-family: Arial, sans-serif">A breif of CBS</h2>
                    <p>A brief history of the Central Bureau of Statistics (CBS)
                        Establishment
                        In 1903, the Statistics Authority (Central Bureau of Statistics now) was a section affiliated to
                        the Customs Authority. Its main function was the development of foreign trade statistics. In
                        1932 the section became what was known as the economic committee which in 1934 became the Tade
                        and economic Authority.</p>
                    <p>All Rights Reserved © Central Bureau of Statistic 2018</p>
                </div>
                <div class="col-md-5 footer-nav animated fadeInUp">
                    <h2 style="color: white;font-family: Arial, sans-serif"><span style="color: #00b0e4"> —</span>Related
                        Sites<span style="color: #00b0e4"> —</span></h2>
                    <div class="col-md-6">
                        <ul class="list">
                            <li><a href="http://sudan.gov.sd/index.php/ar" style="font-size: 10px">Ministry of
                                    Cabinet</a>
                            </li>
                            <li><a href="#" style="font-size: 12px">National Information Center</a></li>
                            <li><a href="#" style="font-size: 11px">Ministry of Finance and Economic Planning</a></li>
                            <li><a href="#" style="font-size: 12px">Central Bank of Sudan</a></li>
                            <li><a href="#" style="font-size: 12px">Federal Ministry of Health</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list">
                            <li><a href="#">About Us</a></li>
                            <li><a href="{{asset('/contact')}}" style="font-size: 10px">Contact Us</a></li>
                            <li><a href="#">Terms & Condition</a></li>
                            <li><a href="#" style="font-size: 10px">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2  footer-social animated fadeInDown">
                    <h4>Follow Us</h4>
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Youtube</a></li>
                        {{--<li><a href="#">RSS</a></li>--}}
                    </ul>
                </div>
                <div class="col-md-2 footer-nse animated fadeInRight">
                    <h4>Newsletter</h4>

                </div>
            </div>
        @endif
    </footer>
</div>

</body>
</html>