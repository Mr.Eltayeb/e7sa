@extends(layoutExtend())
@section('title')
    {{ trans('training.training') }} {{ trans('home.view') }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('training.training') , 'model' => 'training' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            @include("admin.training.relation.user.show")
            <tr>
                <th>{{ trans("training.request_status") }}</th>

                <td>{{ nl2br($item->request_status) }}</td>
            </tr>

            <tr>
                <th>{{ trans("training.name") }}</th>
                @php $type =  getFileType("name" , $item->name) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->name) }}">{{ $item->name }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->name) }}"/></td>
                @else
                    <td>{{ nl2br($item->name) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.university") }}</th>
                @php $type =  getFileType("university" , $item->university) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->university) }}">{{ $item->university }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->university) }}"/></td>
                @else
                    <td>{{ nl2br($item->university) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.college") }}</th>
                @php $type =  getFileType("college" , $item->college) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->college) }}">{{ $item->college }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->college) }}"/></td>
                @else
                    <td>{{ nl2br($item->college) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.specialty") }}</th>
                @php $type =  getFileType("specialty" , $item->specialty) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->specialty) }}">{{ $item->specialty }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->specialty) }}"/></td>
                @else
                    <td>{{ nl2br($item->specialty) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.phone") }}</th>
                @php $type =  getFileType("phone" , $item->phone) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->phone) }}">{{ $item->phone }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->phone) }}"/></td>
                @else
                    <td>{{ nl2br($item->phone) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.email") }}</th>
                @php $type =  getFileType("email" , $item->email) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->email) }}">{{ $item->email }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->email) }}"/></td>
                @else
                    <td>{{ nl2br($item->email) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.type_training") }}</th>
                @php $type =  getFileType("type_training" , $item->type_training) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->type_training) }}">{{ $item->type_training }}</a>
                    </td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->type_training) }}"/></td>
                @else
                    <td>{{ nl2br($item->type_training) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.departments") }}</th>
                @php $type =  getFileType("departments" , $item->departments) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->departments) }}">{{ $item->departments }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->departments) }}"/></td>
                @else
                    <td>{{ nl2br($item->departments) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("training.competence") }}</th>
                @php $type =  getFileType("competence" , $item->competence) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->competence) }}">{{ $item->competence }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->competence) }}"/></td>
                @else
                    <td>{{ nl2br($item->competence) }}</td>
                @endif</tr>
            </tr>
        </table>
        @include('admin.training.buttons.delete' , ['id' => $item->id])
        @include('admin.training.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
