@extends(layoutExtend())
@section('title')
    {{ trans('training.training') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('training.training') , 'model' => 'training' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/training/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.training.relation.user.edit")
            {{--request_status--}}
            {{--<div class="form-group col-md-4">--}}
            {{----}}
            {{--</div>--}}
            {{--end request_status--}}
            <div class="form-group">
                <label for="request_status">{{ trans("training.request_status")}}
                    <select class="form-control" name="request_status">
                        {{--<option value="غير مقروء" {{ old('request_status') == "غير مقروء" ? 'غير مقروء' : '' }}>{{getDir() == 'rtl' ? "غير مقروء" :"unread"}}</option>--}}
                        <option value="غير مقروء">{{getDir() == 'rtl' ? "غير مقروء" :"unread"}}</option>
                        <option value="في الانتظار">{{getDir() == 'rtl' ? "في الانتظار" : "waiting"}}</option>
                        <option value="تم الموافقة">{{getDir() == 'rtl' ? "تم الموافقة" : "Been approved"}}</option>
                    </select>
                </label>
                <br>

                <label for="name">{{ trans("training.name")}}</label>
                <input type="text" name="name" class="form-control" id="name"
                       value="{{ isset($item->name) ? $item->name : old("name")  }}"
                       placeholder="{{ trans("training.name")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="university">{{ trans("training.university")}}</label>
                <input type="text" name="university" class="form-control" id="university"
                       value="{{ isset($item->university) ? $item->university : old("university")  }}"
                       placeholder="{{ trans("training.university")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="college">{{ trans("training.college")}}</label>
                <input type="text" name="college" class="form-control" id="college"
                       value="{{ isset($item->college) ? $item->college : old("college")  }}"
                       placeholder="{{ trans("training.college")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="specialty">{{ trans("training.specialty")}}</label>
                <input type="text" name="specialty" class="form-control" id="specialty"
                       value="{{ isset($item->specialty) ? $item->specialty : old("specialty")  }}"
                       placeholder="{{ trans("training.specialty")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="phone">{{ trans("training.phone")}}</label>
                <input type="text" name="phone" class="form-control" id="phone"
                       value="{{ isset($item->phone) ? $item->phone : old("phone")  }}"
                       placeholder="{{ trans("training.phone")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="email">{{ trans("training.email")}}</label>
                <input type="text" name="email" class="form-control" id="email"
                       value="{{ isset($item->email) ? $item->email : old("email")  }}"
                       placeholder="{{ trans("training.email")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="type_training">{{ trans("training.type_training")}}</label>
                <input type="text" name="type_training" class="form-control" id="type_training"
                       value="{{ isset($item->type_training) ? $item->type_training : old("type_training")  }}"
                       placeholder="{{ trans("training.type_training")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="departments">{{ trans("training.departments")}}</label>
                <input type="text" name="departments" class="form-control" id="departments"
                       value="{{ isset($item->departments) ? $item->departments : old("departments")  }}"
                       placeholder="{{ trans("training.departments")}}">
                </label>
            </div>
            <div class="form-group">
                <label for="competence">{{ trans("training.competence")}}</label>
                <textarea name="competence" class="form-control" id="competence"
                          placeholder="{{ trans("training.competence")}}">{{ isset($item->competence) ? $item->competence : old("competence")  }}</textarea>
                </label>
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('training.training') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
