@extends(layoutExtend())

@section('title')
    {{ trans('newsletter.newsletter') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('newsletter.newsletter') , 'model' => 'newsletter' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("newsletter.title") }}</th>
                @php $type =  getFileType("title" , $item->title) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("newsletter.image") }}</th>
                @php $type =  getFileType("image" , $item->image) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->image) }}">{{ $item->image }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->image)) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("newsletter.file") }}</th>
                @php $type =  getFileType("file" , $item->file) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file) }}"><span style="color: brown"
                                                                                      class="fa fa-3x fa-download"></span></a>
                    </td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->file)) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("newsletter.file_en") }}</th>
                @php $type =  getFileType("file_en" , $item->file_en) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"><span style="color: #f0ad4e"
                                                                                         class="fa fa-3x fa-download"></span></a>
                    </td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->file_en)) }}</td>
                @endif</tr>
        </table>

        @include('admin.newsletter.buttons.delete' , ['id' => $item->id])
        @include('admin.newsletter.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
