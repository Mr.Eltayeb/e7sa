@extends(layoutExtend())

@section('title')
    {{ trans('newsletter.newsletter') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('newsletter.newsletter') , 'model' => 'newsletter' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/newsletter/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">{{ trans("newsletter.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "newsletter") !!}
            </div>
            <div class="form-group">
                <label for="image">{{ trans("newsletter.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
            </div>
            <div class="form-group">
                <label for="file">{{ trans("newsletter.file")}}</label>
                @if(isset($item) && $item->file != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file">
            </div>
            <div class="form-group">
                <label for="file_en">{{ trans("newsletter.file_en")}}</label>
                @if(isset($item) && $item->file_en != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file_en">
            </div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('newsletter.newsletter') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
