@extends(layoutExtend())

@section('title')
     {{ trans('newsletter.newsletter') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('newsletter.newsletter') , 'model' => 'newsletter' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection