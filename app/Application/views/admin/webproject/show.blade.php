@extends(layoutExtend())

@section('title')
    {{ trans('webproject.webproject') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('webproject.webproject') , 'model' => 'webproject' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("webproject.title") }}</th>
                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webproject.body") }}</th>

                <td>{!! getDefaultValueKey(nl2br($item->body)) !!}</td>
            </tr>
        </table>

        @include('admin.webproject.buttons.delete' , ['id' => $item->id])
        @include('admin.webproject.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
