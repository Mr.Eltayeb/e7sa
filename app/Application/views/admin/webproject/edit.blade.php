@extends(layoutExtend())

@section('title')
    {{ trans('webproject.webproject') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('webproject.webproject') , 'model' => 'webproject' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/webproject/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">{{ trans("webproject.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "webproject") !!}
            </div>
            <div class="form-group">
                <label for="body">{{ trans("webproject.body")}}</label>
                {!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , "textarea" , "webproject", 'tinymce') !!}
            </div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('webproject.webproject') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection


@section('script')
    @include(layoutPath('layout.helpers.tynic'))
    {{ Html::script('/admin/plugins/momentjs/moment.js') }}
    {{ Html::script('/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}
    <script>
        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            format: "L",
            setDate: "{{ date('d/m/Y')  }}",
            nowButton: true
        });
    </script>
@endsection
