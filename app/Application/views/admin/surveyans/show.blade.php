@extends(layoutExtend())

@section('title')
    {{ trans('surveyans.surveyans') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('surveyans.surveyans') , 'model' => 'surveyans' , 'action' => trans('home.view')  ])
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("surveyans.answers") }}</th>
				@php $type =  getFileType("answers" , $item->answers) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->answers) }}">{{ $item->answers }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->answers) }}" /></td>
				@else
					<td>{{ nl2br($item->answers) }}</td>
				@endif</tr>
			</tr>
		</table>

        @include('admin.surveyans.buttons.delete' , ['id' => $item->id])
        @include('admin.surveyans.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
