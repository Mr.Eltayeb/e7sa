@extends(layoutExtend())

@section('title')
    {{ trans('surveyans.surveyans') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('surveyans.surveyans') , 'model' => 'surveyans' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
         @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/surveyans/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

 		<div class="form-group">
			<label for="answers">{{ trans("surveyans.answers")}}</label>
				<input type="text" name="answers" class="form-control" id="answers" value="{{ isset($item->answers) ? $item->answers : old("answers")  }}"  placeholder="{{ trans("surveyans.answers")}}">
			</label>
		</div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('surveyans.surveyans') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
