@extends(layoutExtend())
@section('title')
    {{ trans('webslider.webslider') }} {{ trans('home.view') }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('webslider.webslider') , 'model' => 'webslider' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            @include("admin.webslider.relation.webcategorie.show")
            <tr>
                <th>{{ trans("webslider.title") }}</th>
               <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                </tr>
            <tr>
                <th>{{ trans("webslider.image") }}</th>
               <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
               </tr>
            <tr>
                <th>{{ trans("webslider.url") }}</th>

                    <td><a href="{{ getDefaultValueKey(nl2br($item->url)) }}"><span class="fa fa-eye"></span></a> </td>
                </tr>
        </table>
        @include('admin.webslider.buttons.delete' , ['id' => $item->id])
        @include('admin.webslider.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
