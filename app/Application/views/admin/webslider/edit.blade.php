@extends(layoutExtend())
@section('title')
    {{ trans('webslider.webslider') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('webslider.webslider') , 'model' => 'webslider' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/webslider/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.webslider.relation.webcategorie.edit")
            <div class="form-group">
                <label for="title">{{ trans("webslider.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "webslider") !!}
            </div>
            <div class="form-group">
                <label for="image">{{ trans("webslider.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
            </div>
            <div class="form-group">
                <label for="url">{{ trans("webslider.url")}}</label>
                {!! extractFiled("url" , isset($item->url) ? $item->url : old("url") , "text" , "webslider") !!}
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('webslider.webslider') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
