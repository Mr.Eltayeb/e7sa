		<tr>
			<th>
			{{ trans( "webcategorie.webcategorie") }}
			</th>
			<td>
				@php $webcategorie = App\Application\Model\Webcategorie::find($item->webcategorie_id);  @endphp
				{{ is_json($webcategorie->title) ? getDefaultValueKey($webcategorie->title) :  $webcategorie->title}}
			</td>
		</tr>
