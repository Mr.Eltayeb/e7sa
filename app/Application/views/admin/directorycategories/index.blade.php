@extends(layoutExtend())

@section('title')
     {{ trans('directorycategories.directorycategories') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('directorycategories.directorycategories') , 'model' => 'directorycategories' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection