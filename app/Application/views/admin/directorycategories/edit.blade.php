@extends(layoutExtend())
 @section('title')
    {{ trans('directorycategories.directorycategories') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
 @section('content')
    @component(layoutForm() , ['title' => trans('directorycategories.directorycategories') , 'model' => 'directorycategories' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
         @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/directorycategories/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
    <div class="form-group">
   <label for="title">{{ trans("directorycategories.title")}}</label>
    {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "directorycategories") !!}
  </div>
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('directorycategories.directorycategories') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
