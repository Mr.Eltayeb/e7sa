@extends(layoutExtend())
  @section('title')
    {{ trans('directorycategories.directorycategories') }} {{ trans('home.view') }}
@endsection
  @section('content')
    @component(layoutForm() , ['title' => trans('directorycategories.directorycategories') , 'model' => 'directorycategories' , 'action' => trans('home.view')  ])
  <table class="table table-bordered table-responsive table-striped">
   </tr>
    <tr><th>{{ trans("directorycategories.title") }}</th>
    @php $type =  getFileType("title" , $item->title) @endphp
    @if($type == "File")
     <td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
    @elseif($type == "Image")
     <td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}" /></td>
    @else
     <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
    @endif</tr>
   </tr>
  </table>
          @include('admin.directorycategories.buttons.delete' , ['id' => $item->id])
        @include('admin.directorycategories.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
