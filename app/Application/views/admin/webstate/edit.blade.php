@extends(layoutExtend())

@section('title')
    {{ trans('webstate.webstate') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('webstate.webstate') , 'model' => 'webstate' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/webstate/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">{{ trans("webstate.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "webstate") !!}
            </div>
            <div class="form-group">
                <label for="image">{{ trans("webstate.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
            </div>
            <div class="form-group">
                <label for="body">{{ trans("webstate.body")}}</label>
                {!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , "textarea" , "webstate", 'tinymce') !!}
            </div>

            <div class="form-group {{ $errors->has("file[]") ? "has-error" : "" }}" >
                <label for="file">{{ trans("webstate.file")}}</label>
                <div id="laraflat-file">
                    @isset($item)
                        @if(json_decode($item->file))
                            <input type="hidden" name="oldFiles_file" value="{{ $item->file }}">
                            @php $files = returnFilesImages($item , "file"); @endphp
                            <div class="row text-center">
                                @foreach($files["image"] as $jsonimage )
                                    <div class="col-lg-2 text-center"><img src="{{ small($jsonimage) }}" class="img-responsive" /><br>
                                        <a class="btn btn-danger" onclick="deleteThisItem(this)" data-link="{{ url("deleteFile/webstate/".$item->id."?name=".$jsonimage."&filed_name=file") }}"><i class="fa fa-trash"></i></a></div>
                                @endforeach
                            </div>
                            <div class="row text-center">
                                @foreach($files["file"] as $jsonimage )
                                    <div class="col-lg-2 text-center"><a href="{{ url(env("UPLOAD_PATH")."/".$jsonimage) }}" ><i class="fa fa-file"></i></a>
                                        <span  onclick="deleteThisItem(this)" data-link="{{ url("deleteFile/webstate/".$item->id."?name=".$jsonimage."&filed_name=file") }}"><i class="fa fa-trash"></i> {{ $jsonimage }} </span></div>
                                @endforeach
                            </div>
                        @endif
                    @endisset
                    <input name="file[]"  type="file" multiple >
                </div>
            </div>
            @if ($errors->has("file[]"))
                <div class="alert alert-danger">
					<span class='help-block'>
						<strong>{{ $errors->first("file[]") }}</strong>
					</span>
                </div>
            @endif


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('webstate.webstate') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection


@section('script')
    @include(layoutPath('layout.helpers.tynic'))
    {{ Html::script('/admin/plugins/momentjs/moment.js') }}
    {{ Html::script('/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}
    <script>
        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            format: "L",
            setDate: "{{ date('d/m/Y')  }}",
            nowButton: true
        });
    </script>
@endsection
