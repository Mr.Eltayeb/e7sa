@extends(layoutExtend())

@section('title')
    {{ trans('webstate.webstate') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('webstate.webstate') , 'model' => 'webstate' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("webstate.title") }}</th>
                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webstate.image") }}</th>
                <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
            </tr>
            <tr>
                <th>{{ trans("webstate.body") }}</th>

                <td>{!! getDefaultValueKey(nl2br($item->body)) !!}</td>
            </tr>
        </table>

        @include('admin.webstate.buttons.delete' , ['id' => $item->id])
        @include('admin.webstate.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
