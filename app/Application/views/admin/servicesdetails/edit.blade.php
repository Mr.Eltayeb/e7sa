@extends(layoutExtend())

@section('title')
    {{ trans('servicesdetails.servicesdetails') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('servicesdetails.servicesdetails') , 'model' => 'servicesdetails' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/servicesdetails/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">{{ trans("servicesdetails.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "servicesdetails") !!}
                </label>
            </div>
            <div class="form-group">
                <label for="image">{{ trans("servicesdetails.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
                </label>
            </div>
            <div class="form-group">
                <label for="body">{{ trans("servicesdetails.body")}}</label>
                {!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , "textarea" , "servicesdetails") !!}
                </label>
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="url">{{ trans("servicesdetails.url")}}</label>--}}
                {{--{!! extractFiled("url" , isset($item->url) ? $item->url : old("url") , "text" , "servicesdetails") !!}--}}

            {{--</div>--}}

            <div class="form-group">
                <label for="url">{{ trans("servicesdetails.url")}}</label>
                <input type="text" name="url" class="form-control" id="url"
                       value="{{ isset($item->url) ? $item->url : old("url") }}"
                       placeholder="{{ trans("servicesdetails.url")}}">
                </label>
            </div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('servicesdetails.servicesdetails') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
