@extends(layoutExtend())

@section('title')
     {{ trans('servicesdetails.servicesdetails') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('servicesdetails.servicesdetails') , 'model' => 'servicesdetails' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection