@extends(layoutExtend())

@section('title')
    {{ trans('webcategorie.webcategorie') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('webcategorie.webcategorie') , 'model' => 'webcategorie' , 'action' => trans('home.view')  ])
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("webcategorie.title") }}</th>
				@php $type =  getFileType("title" , $item->title) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
				@endif</tr>
			</tr>
		</table>

        @include('admin.webcategorie.buttons.delete' , ['id' => $item->id])
        @include('admin.webcategorie.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
