@extends(layoutExtend())

@section('title')
     {{ trans('webcategorie.webcategorie') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('webcategorie.webcategorie') , 'model' => 'webcategorie' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection