@extends(layoutExtend())

@section('title')
    {{ trans('aboutcentral.aboutcentral') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('aboutcentral.aboutcentral') , 'model' => 'aboutcentral' , 'action' => trans('home.view')  ])
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("aboutcentral.title") }}</th>
				@php $type =  getFileType("title" , $item->title) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
				@endif</tr>
				<tr><th>{{ trans("aboutcentral.body") }}</th>
				@php $type =  getFileType("body" , $item->body) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->body) }}">{{ $item->body }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->body) }}" /></td>
				@else
					<td>{!! getDefaultValueKey(nl2br($item->body)) !!}</td>
				@endif</tr>
			</tr>
		</table>

        @include('admin.aboutcentral.buttons.delete' , ['id' => $item->id])
        @include('admin.aboutcentral.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
