@extends(layoutExtend())

@section('title')
     {{ trans('aboutcentral.aboutcentral') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('aboutcentral.aboutcentral') , 'model' => 'aboutcentral' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection