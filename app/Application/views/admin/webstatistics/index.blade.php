@extends(layoutExtend())

@section('title')
     {{ trans('webstatistics.webstatistics') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('webstatistics.webstatistics') , 'model' => 'webstatistics' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection