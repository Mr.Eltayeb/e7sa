@extends(layoutExtend())
@section('title')
    {{ trans('webinfographic.webinfographic') }} {{ trans('home.view') }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('webinfographic.webinfographic') , 'model' => 'webinfographic' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            @include("admin.webinfographic.relation.webcategorie.show")
            <tr>
                <th>{{ trans("webinfographic.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webinfographic.url") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->url)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webinfographic.image") }}</th>
                <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
            </tr>
        </table>
        @include('admin.webinfographic.buttons.delete' , ['id' => $item->id])
        @include('admin.webinfographic.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
