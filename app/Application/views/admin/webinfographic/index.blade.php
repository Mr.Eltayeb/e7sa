@extends(layoutExtend())

@section('title')
     {{ trans('webinfographic.webinfographic') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('webinfographic.webinfographic') , 'model' => 'webinfographic' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection