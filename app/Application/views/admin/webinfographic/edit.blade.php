@extends(layoutExtend())
@section('title')
    {{ trans('webinfographic.webinfographic') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('webinfographic.webinfographic') , 'model' => 'webinfographic' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/webinfographic/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.webinfographic.relation.webcategorie.edit")
            <div class="form-group">
                <label for="title">{{ trans("webinfographic.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "webinfographic") !!}
            </div>
            <div class="form-group">
                <label for="title">{{ trans("webinfographic.url")}}</label>
                {!! extractFiled("url" , isset($item->url) ? $item->title : old("url") , "text" , "webinfographic") !!}
            </div>
            <div class="form-group">
                <label for="image">{{ trans("webinfographic.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('webinfographic.webinfographic') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
