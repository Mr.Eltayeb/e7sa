@extends(layoutExtend())

@section('title')
    {{ trans('fullnews.fullnews') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('fullnews.fullnews') , 'model' => 'fullnews' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/fullnews/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">{{ trans("fullnews.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "fullnews") !!}
            </div>
            <div class="form-group">
                <label for="image">{{ trans("fullnews.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
            </div>
            <div class="form-group">
                <label for="body">{{ trans("fullnews.body")}}</label>
                {!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , "textarea" , "fullnews", 'tinymce') !!}
            </div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('fullnews.fullnews') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection

@section('script')
    @include(layoutPath('layout.helpers.tynic'))
    {{ Html::script('/admin/plugins/momentjs/moment.js') }}
    {{ Html::script('/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}
    <script>
        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            format: "L",
            setDate: "{{ date('d/m/Y')  }}",
            nowButton: true
        });
    </script>
@endsection

