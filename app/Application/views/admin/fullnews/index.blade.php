@extends(layoutExtend())

@section('title')
    {{ trans('fullnews.fullnews') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('fullnews.fullnews') , 'model' => 'fullnews' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection