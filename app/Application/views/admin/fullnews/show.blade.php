@extends(layoutExtend())

@section('title')
    {{ trans('fullnews.fullnews') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('fullnews.fullnews') , 'model' => 'fullnews' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("fullnews.title") }}</th>

                    <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
                </tr>
            <tr>
                <th>{{ trans("fullnews.image") }}</th>

                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}"/></td>
               </tr>
            <tr>
                <th>{{ trans("fullnews.body") }}</th>

                    <td>{!! getDefaultValueKey(nl2br($item->body)) !!}</td>
                </tr>
        </table>

        @include('admin.fullnews.buttons.delete' , ['id' => $item->id])
        @include('admin.fullnews.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
