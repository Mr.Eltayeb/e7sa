		<div class="form-group">
			<label for="directorycategories">{{ trans( "directorycategories.directorycategories") }}</label>
			@php $directorycategories = App\Application\Model\Directorycategories::pluck("title" ,"id")->all()  @endphp
			@php  $directorycategories_id = isset($item) ? $item->directorycategories_id : null @endphp
			<select name="directorycategories_id"  class="form-control" >
			@foreach( $directorycategories as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $directorycategories_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</label>
			@endforeach
			</select>
		</div>
