		<tr>
			<th>
			{{ trans( "directorycategories.directorycategories") }}
			</th>
			<td>
				@php $directorycategories = App\Application\Model\Directorycategories::find($item->directorycategories_id);  @endphp
				{{ is_json($directorycategories->title) ? getDefaultValueKey($directorycategories->title) :  $directorycategories->title}}
			</td>
		</tr>
