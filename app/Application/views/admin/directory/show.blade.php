@extends(layoutExtend())
@section('title')
    {{ trans('directory.directory') }} {{ trans('home.view') }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('directory.directory') , 'model' => 'directory' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            @include("admin.directory.relation.directorycategories.show")
            <tr>
                <th>{{ trans("directory.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("directory.body") }}</th>

                <td>{!!  getDefaultValueKey(nl2br($item->body))  !!}</td>
            </tr>
            <tr>
                <th>{{ trans("directory.file") }}</th>
                @php $type =  getFileType("file" , $item->file) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file) }}"> <span style="color: blue" class="fa fa-3x fa-download"></span> {{ $item->file }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->file)) }}</td>
                @endif</tr>
            <tr>
                <th>{{ trans("directory.file_en") }}</th>
                @php $type =  getFileType("file_en" , $item->file_en) @endphp
                @if($type == "File")
                    <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"> <span style="color: brown" class="fa fa-3x fa-download"></span>  {{ $item->file_en }}</a></td>
                @elseif($type == "Image")
                    <td><img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}"/></td>
                @else
                    <td>{{ getDefaultValueKey(nl2br($item->file_en)) }}</td>
                @endif</tr>
            </tr>
        </table>
        @include('admin.directory.buttons.delete' , ['id' => $item->id])
        @include('admin.directory.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
