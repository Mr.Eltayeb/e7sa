@extends(layoutExtend())
@section('title')
    {{ trans('directory.directory') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('directory.directory') , 'model' => 'directory' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/directory/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.directory.relation.directorycategories.edit")
            <div class="form-group">
                <label for="title">{{ trans("directory.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "directory") !!}
            </div>
            <div class="form-group">
                <label for="body">{{ trans("directory.body")}}</label>
                {!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , 'textarea' , 'directory', 'tinymce') !!}
            </div>
            <div class="form-group">
                <label for="file">{{ trans("directory.file")}}</label>
                @if(isset($item) && $item->file != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file">
            </div>
            <div class="form-group">
                <label for="file_en">{{ trans("directory.file_en")}}</label>
                @if(isset($item) && $item->file_en != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file_en">
                </label>
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('directory.directory') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection

@section('script')
    @include(layoutPath('layout.helpers.tynic'))
    {{ Html::script('/admin/plugins/momentjs/moment.js') }}
    {{ Html::script('/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}
    <script>
        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            format: "L",
            setDate: "{{ date('d/m/Y')  }}",
            nowButton: true
        });
    </script>
@endsection
