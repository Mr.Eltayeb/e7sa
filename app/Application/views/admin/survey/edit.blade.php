@extends(layoutExtend())

@section('title')
    {{ trans('survey.survey') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('survey.survey') , 'model' => 'survey' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/survey/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">{{ trans("survey.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "survey") !!}
            </div>
            <div class="form-group">
                <label for="answers1">{{ trans("survey.answers1")}}</label>
                {!! extractFiled("answers1" , isset($item->answers1) ? $item->answers1 : old("answers1") , "text" , "survey") !!}
            </div>
            <div class="form-group">
                <label for="answers2">{{ trans("survey.answers2")}}</label>
                {!! extractFiled("answers2" , isset($item->answers2) ? $item->answers2 : old("answers2") , "text" , "survey") !!}
            </div>
            <div class="form-group">
                <label for="answers3">{{ trans("survey.answers3")}}</label>
                {!! extractFiled("answers3" , isset($item->answers3) ? $item->answers3 : old("answers3") , "text" , "survey") !!}
            </div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('survey.survey') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
