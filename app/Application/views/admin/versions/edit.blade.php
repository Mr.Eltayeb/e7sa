@extends(layoutExtend())
@section('title')
    {{ trans('versions.versions') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('versions.versions') , 'model' => 'versions' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/versions/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.versions.relation.cateversions.edit")

            {{--<div class="form-group">--}}
            {{--<label for="title">{{ trans("versions.title")}}</label>--}}
            {{--<input type="text" name="title" class="form-control" id="title"--}}
            {{--value="{{ isset($item->title) ? $item->title : old("title")  }}"--}}
            {{--placeholder="{{ trans("versions.title")}}">--}}
            {{--</label>--}}
            {{--</div>--}}
            <div class="form-group">
                <label for="title">{{ trans("versions.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "versions") !!}
            </div>

            <div class="form-group">
                <label for="body">{{ trans("versions.body")}}</label>
                {!! extractFiled("body" , isset($item->body) ? $item->body : old("body") , 'textarea' , 'versions', 'tinymce') !!}
            </div>

            <div class="form-group">
                <label for="image">{{ trans("versions.image")}}</label>
                @if(isset($item) && $item->image != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="image">
            </div>
            <div class="form-group">
                <label for="file">{{ trans("versions.file")}}</label>
                @if(isset($item) && $item->file != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file">
            </div>
            <div class="form-group">
                <label for="file_en">{{ trans("versions.file_en")}}</label>
                @if(isset($item) && $item->file_en != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file_en">
            </div>
            <div class="form-group">
                <label for="year">{{ trans("versions.year")}}</label>
                {!! extractFiled("year" , isset($item->year) ? $item->year : old("year") , "text" , "versions") !!}
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('versions.versions') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection

@section('script')
    @include(layoutPath('layout.helpers.tynic'))
    {{ Html::script('/admin/plugins/momentjs/moment.js') }}
    {{ Html::script('/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}
    <script>
        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            format: "L",
            setDate: "{{ date('d/m/Y')  }}",
            nowButton: true
        });
    </script>
@endsection
