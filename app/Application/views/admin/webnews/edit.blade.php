@extends(layoutExtend())

@section('title')
    {{ trans('webnews.webnews') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('webnews.webnews') , 'model' => 'webnews' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/webnews/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group">
                <label for="title">{{ trans("webnews.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "webnews") !!}
            </div>
            <div class="form-group">
                <label for="url">{{ trans("webnews.url")}}</label>
                {!! extractFiled("url" , isset($item->url) ? $item->url : old("url") , "text" , "webnews") !!}
            </div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('webnews.webnews') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
