@extends(layoutExtend())

@section('title')
    {{ trans('webnews.webnews') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('webnews.webnews') , 'model' => 'webnews' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            <tr>
                <th>{{ trans("webnews.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webnews.url") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->url)) }}</td>
            </tr>
        </table>


    @endcomponent
@endsection
