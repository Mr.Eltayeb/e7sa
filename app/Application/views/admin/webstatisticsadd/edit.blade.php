@extends(layoutExtend())
@section('title')
    {{ trans('webstatisticsadd.webstatisticsadd') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('webstatisticsadd.webstatisticsadd') , 'model' => 'webstatisticsadd' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
        @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/webstatisticsadd/item') }}{{ isset($item) ? '/'.$item->id : '' }}"
              method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("admin.webstatisticsadd.relation.webstatistics.edit")
            <div class="form-group">
                <label for="title">{{ trans("webstatisticsadd.title")}}</label>
                {!! extractFiled("title" , isset($item->title) ? $item->title : old("title") , "text" , "webstatisticsadd") !!}
            </div>
            <div class="form-group">
                <label for="file">{{ trans("webpdf.file_ar")}}</label>
                @if(isset($item) && $item->file_ar != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_ar) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file_ar">
            </div>
            <div class="form-group">
                <label for="file">{{ trans("webpdf.file_en")}}</label>
                @if(isset($item) && $item->file_en != "")
                    <br>
                    <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" class="thumbnail" alt="" width="200">
                    <br>
                @endif
                <input type="file" name="file_en">
            </div>
            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default">
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('webstatisticsadd.webstatisticsadd') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
