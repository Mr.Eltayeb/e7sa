@extends(layoutExtend())

@section('title')
     {{ trans('webstatisticsadd.webstatisticsadd') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('webstatisticsadd.webstatisticsadd') , 'model' => 'webstatisticsadd' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection