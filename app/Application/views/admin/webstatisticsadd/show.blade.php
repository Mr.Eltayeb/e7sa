@extends(layoutExtend())
@section('title')
    {{ trans('webstatisticsadd.webstatisticsadd') }} {{ trans('home.view') }}
@endsection
@section('content')
    @component(layoutForm() , ['title' => trans('webstatisticsadd.webstatisticsadd') , 'model' => 'webstatisticsadd' , 'action' => trans('home.view')  ])
        <table class="table table-bordered table-responsive table-striped">
            @include("admin.webstatisticsadd.relation.webstatistics.show")
            <tr>
                <th>{{ trans("webstatisticsadd.title") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webstatisticsadd.body") }}</th>

                <td>{{ getDefaultValueKey(nl2br($item->body)) }}</td>
            </tr>
            <tr>
                <th>{{ trans("webstatisticsadd.file_ar") }}</th>

                <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_ar) }}">{{ $item->file_ar }}</a></td>
            </tr>
            <tr>
                <th>{{ trans("webstatisticsadd.file_en") }}</th>
                <td><a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}">{{ $item->file_en }}</a></td>
            </tr>
        </table>
        @include('admin.webstatisticsadd.buttons.delete' , ['id' => $item->id])
        @include('admin.webstatisticsadd.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
