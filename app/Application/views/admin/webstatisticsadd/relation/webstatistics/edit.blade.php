		<div class="form-group">
			<label for="webstatistics">{{ trans( "webstatistics.webstatistics") }}</label>
			@php $webstatistics = App\Application\Model\Webstatistics::pluck("title" ,"id")->all()  @endphp
			@php  $webstatistics_id = isset($item) ? $item->webstatistics_id : null @endphp
			<select name="webstatistics_id"  class="form-control" >
			@foreach( $webstatistics as $key => $relatedItem)
			<option value="{{ $key }}"  {{ $key == $webstatistics_id  ? "selected" : "" }}> {{ is_json($relatedItem) ? getDefaultValueKey($relatedItem) :  $relatedItem}}</label>
			@endforeach
			</select>
		</div>
