@extends(layoutExtend())

@section('title')
     {{ trans('requestdata.requestdata') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
{{--    @include(layoutTable() , ['title' => trans('requestdata.requestdata') , 'model' => 'requestdata' , 'table' => $dataTable->table([] , true) ])--}}
    <div class="container">
        <div><h1>{{ trans('website.requestdata') }}</h1></div>
        <div><a href="{{ url('requestdata/item') }}" class="btn btn-default"><i class="fa fa-plus"></i> {{ trans('website.requestdata') }}</a><br></div>
        <table class="table table-responsive table-striped table-bordered">
            <thead>
            <tr>
                <th>{{ trans("requestdata.name") }}</th>
                <th>{{ trans("requestdata.type_identity") }}</th>
{{--                <th>{{ trans("requestdata.Identity") }}</th>--}}
                {{--<th>{{ trans("requestdata.occupation") }}</th>--}}
                {{--<th>{{ trans("requestdata.qualification") }}</th>--}}
                <th>{{ trans("requestdata.phone") }}</th>
                {{--<th>{{ trans("requestdata.fax") }}</th>--}}
                <th>{{ trans("requestdata.email") }}</th>
{{--                <th>{{ trans("requestdata.data_usage") }}</th>--}}
{{--                <th>{{ trans("requestdata.company_name") }}</th>--}}
{{--                <th>{{ trans("requestdata.type_Company") }}</th>--}}
                <th>{{ trans("requestdata.Data_usage_area") }}</th>
{{--                <th>{{ trans("requestdata.data_details") }}</th>--}}
                <th>{{ trans("requestdata.edit") }}</th>
                <th>{{ trans("requestdata.show") }}</th>
                <th>{{ trans("requestdata.delete") }}</th>
            </thead>
            <tbody>
            @if(count($items) > 0)
                @foreach($items as $d)
                    <tr>
                        <td>{{ str_limit($d->name , 20) }}</td>
                        <td>{{ str_limit($d->type_identity , 20) }}</td>
{{--                        <td>{{ str_limit($d->Identity , 20) }}</td>--}}
{{--                        <td>{{ str_limit($d->occupation , 20) }}</td>--}}
{{--                        <td>{{ str_limit($d->qualification , 20) }}</td>--}}
                        <td>{{ str_limit($d->phone , 20) }}</td>
{{--                        <td>{{ str_limit($d->fax , 20) }}</td>--}}
                        <td>{{ str_limit($d->email , 20) }}</td>
                        <td>{{ str_limit($d->data_usage , 20) }}</td>
{{--                        <td>{{ str_limit($d->company_name , 20) }}</td>--}}
{{--                        <td>{{ str_limit($d->type_Company , 20) }}</td>--}}
{{--                        <td>{{ str_limit($d->Data_usage_area , 20) }}</td>--}}
{{--                        <td>{{ str_limit($d->data_details , 20) }}</td>--}}
                        <td>@include("admin.requestdata.buttons.edit", ["id" => $d->id ])</td>
                        <td>@include("admin.requestdata.buttons.view", ["id" => $d->id ])</td>
                        <td>@include("admin.requestdata.buttons.delete", ["id" => $d->id ])</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection

@section('script')
{{--    @include('admin.shared.scripts')--}}
@endsection