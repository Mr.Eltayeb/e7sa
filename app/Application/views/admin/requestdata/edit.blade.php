@extends(layoutExtend())

@section('title')
    {{ trans('requestdata.requestdata') }} {{  isset($item) ? trans('home.edit')  : trans('home.add')  }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('requestdata.requestdata') , 'model' => 'requestdata' , 'action' => isset($item) ? trans('home.edit')  : trans('home.add')  ])
         @include(layoutMessage())
        <form action="{{ concatenateLangToUrl('admin/requestdata/item') }}{{ isset($item) ? '/'.$item->id : '' }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

 		<div class="form-group">
			<label for="name">{{ trans("requestdata.name")}}</label>
				<input type="text" name="name" class="form-control" id="name" value="{{ isset($item->name) ? $item->name : old("name")  }}"  placeholder="{{ trans("requestdata.name")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="type_identity">{{ trans("requestdata.type_identity")}}</label>
				<input type="text" name="type_identity" class="form-control" id="type_identity" value="{{ isset($item->type_identity) ? $item->type_identity : old("type_identity")  }}"  placeholder="{{ trans("requestdata.type_identity")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="Identity">{{ trans("requestdata.Identity")}}</label>
				<input type="text" name="Identity" class="form-control" id="Identity" value="{{ isset($item->Identity) ? $item->Identity : old("Identity")  }}"  placeholder="{{ trans("requestdata.Identity")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="occupation">{{ trans("requestdata.occupation")}}</label>
				<input type="text" name="occupation" class="form-control" id="occupation" value="{{ isset($item->occupation) ? $item->occupation : old("occupation")  }}"  placeholder="{{ trans("requestdata.occupation")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="qualification">{{ trans("requestdata.qualification")}}</label>
				<input type="text" name="qualification" class="form-control" id="qualification" value="{{ isset($item->qualification) ? $item->qualification : old("qualification")  }}"  placeholder="{{ trans("requestdata.qualification")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="phone">{{ trans("requestdata.phone")}}</label>
				<input type="text" name="phone" class="form-control" id="phone" value="{{ isset($item->phone) ? $item->phone : old("phone")  }}"  placeholder="{{ trans("requestdata.phone")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="fax">{{ trans("requestdata.fax")}}</label>
				<input type="text" name="fax" class="form-control" id="fax" value="{{ isset($item->fax) ? $item->fax : old("fax")  }}"  placeholder="{{ trans("requestdata.fax")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="email">{{ trans("requestdata.email")}}</label>
				<input type="text" name="email" class="form-control" id="email" value="{{ isset($item->email) ? $item->email : old("email")  }}"  placeholder="{{ trans("requestdata.email")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="data_usage">{{ trans("requestdata.data_usage")}}</label>
				<input type="text" name="data_usage" class="form-control" id="data_usage" value="{{ isset($item->data_usage) ? $item->data_usage : old("data_usage")  }}"  placeholder="{{ trans("requestdata.data_usage")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="company_name">{{ trans("requestdata.company_name")}}</label>
				<input type="text" name="company_name" class="form-control" id="company_name" value="{{ isset($item->company_name) ? $item->company_name : old("company_name")  }}"  placeholder="{{ trans("requestdata.company_name")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="type_Company">{{ trans("requestdata.type_Company")}}</label>
				<input type="text" name="type_Company" class="form-control" id="type_Company" value="{{ isset($item->type_Company) ? $item->type_Company : old("type_Company")  }}"  placeholder="{{ trans("requestdata.type_Company")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="Data_usage_area">{{ trans("requestdata.Data_usage_area")}}</label>
				<input type="text" name="Data_usage_area" class="form-control" id="Data_usage_area" value="{{ isset($item->Data_usage_area) ? $item->Data_usage_area : old("Data_usage_area")  }}"  placeholder="{{ trans("requestdata.Data_usage_area")}}">
			</label>
		</div>
		<div class="form-group">
			<label for="data_details">{{ trans("requestdata.data_details")}}</label>
				<textarea name="data_details" class="form-control" id="data_details"   placeholder="{{ trans("requestdata.data_details")}}">{{ isset($item->data_details) ? $item->data_details : old("data_details")  }}</textarea>
			</label>
		</div>


            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-default" >
                    <i class="material-icons">check_circle</i>
                    {{ trans('home.save') }}  {{ trans('requestdata.requestdata') }}
                </button>
            </div>
        </form>
    @endcomponent
@endsection
