@extends(layoutExtend())

@section('title')
     {{ trans('elibrary.elibrary') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('elibrary.elibrary') , 'model' => 'elibrary' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection