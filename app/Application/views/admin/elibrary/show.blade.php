@extends(layoutExtend())

@section('title')
    {{ trans('elibrary.elibrary') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('elibrary.elibrary') , 'model' => 'elibrary' , 'action' => trans('home.view')  ])
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("elibrary.title") }}</th>
				@php $type =  getFileType("title" , $item->title) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
				@endif</tr>
				<tr><th>{{ trans("elibrary.image") }}</th>
				@php $type =  getFileType("image" , $item->image) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->image) }}">{{ $item->image }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->image) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->image)) }}</td>
				@endif</tr>
				<tr><th>{{ trans("elibrary.file") }}</th>
				@php $type =  getFileType("file" , $item->file) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->file) }}">{{ $item->file }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->file) }}" /></td>
				@else
					<td>{{ nl2br($item->file) }}</td>
				@endif</tr>
				<tr><th>{{ trans("elibrary.file_en") }}</th>
				@php $type =  getFileType("file_en" , $item->file_en) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}">{{ $item->file_en }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->file_en) }}" /></td>
				@else
					<td>{{ nl2br($item->file_en) }}</td>
				@endif</tr>
			</tr>
		</table>

        @include('admin.elibrary.buttons.delete' , ['id' => $item->id])
        @include('admin.elibrary.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
