@extends(layoutExtend())

@section('title')
     {{ trans('cateversions.cateversions') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('cateversions.cateversions') , 'model' => 'cateversions' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection