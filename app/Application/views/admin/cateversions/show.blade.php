@extends(layoutExtend())

@section('title')
    {{ trans('cateversions.cateversions') }} {{ trans('home.view') }}
@endsection

@section('content')
    @component(layoutForm() , ['title' => trans('cateversions.cateversions') , 'model' => 'cateversions' , 'action' => trans('home.view')  ])
		<table class="table table-bordered table-responsive table-striped">
			</tr>
				<tr><th>{{ trans("cateversions.title") }}</th>
				@php $type =  getFileType("title" , $item->title) @endphp
				@if($type == "File")
					<td> <a href="{{ url(env("UPLOAD_PATH")."/".$item->title) }}">{{ $item->title }}</a></td>
				@elseif($type == "Image")
					<td> <img src="{{ url(env("UPLOAD_PATH")."/".$item->title) }}" /></td>
				@else
					<td>{{ getDefaultValueKey(nl2br($item->title)) }}</td>
				@endif</tr>
			</tr>
		</table>

        @include('admin.cateversions.buttons.delete' , ['id' => $item->id])
        @include('admin.cateversions.buttons.edit' , ['id' => $item->id])
    @endcomponent
@endsection
