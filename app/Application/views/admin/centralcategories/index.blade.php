@extends(layoutExtend())

@section('title')
     {{ trans('centralcategories.centralcategories') }} {{ trans('home.control') }}
@endsection

@section('style')
    @include('admin.shared.style')
@endsection

@section('content')
    @include(layoutTable() , ['title' => trans('centralcategories.centralcategories') , 'model' => 'centralcategories' , 'table' => $dataTable->table([] , true) ])
@endsection

@section('script')
    @include('admin.shared.scripts')
@endsection