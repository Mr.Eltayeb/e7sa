<?php

namespace App\Application\Controllers\Website;


use App\Application\Controllers\AbstractController;
use App\Application\Controllers\Controller;
use App\Application\Model\Page;
use App\Application\Model\Survey;
use App\Application\Model\User;
use App\Application\Model\Versions;
use App\Application\Model\Webnews;
use App\Application\Model\Webslider;
use App\Application\Model\Webinfographic;
use App\Application\Model\Aboutcentral;
use App\Application\Model\Webstatistics;
use App\Application\Model\Webstatisticsadd;


class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('website.home');
    }

    public function getPageBySlug($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if ($page) {
            return view('website.page', compact('page'));
        }
        return redirect('404');
    }

    public function welcome()
    {
        $slider = Webslider::all();
        $infoG = Webinfographic::all();
        $d = Webnews::all();
        $menuAbout = Aboutcentral::all();
        $isdarat = Versions::all();
        $survey = Survey::all();

//        $ecoCat = Webstatisticsadd::where('webstatistics_id','>=',$item)->get();

        return view('website.welcome', compact(['slider', 'infoG', 'd', 'menuAbout', 'isdarat','survey']));
//        echo $slider;
    }
}
