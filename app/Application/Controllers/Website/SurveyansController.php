<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Surveyans;
use App\Application\Requests\Website\Surveyans\AddRequestSurveyans;
use App\Application\Requests\Website\Surveyans\UpdateRequestSurveyans;

class SurveyansController extends AbstractController
{

     public function __construct(Surveyans $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.surveyans.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.surveyans.edit' , $id);
        }


     public function store(AddRequestSurveyans $request){
          $item =  $this->storeOrUpdate($request , null , true);
         $request->session()->flash('alert-success', 'successful!');
         return redirect('/');
     }

     public function update($id , UpdateRequestSurveyans $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.surveyans.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'surveyans')->with('sucess' , 'Done Delete Surveyans From system');
        }


}
