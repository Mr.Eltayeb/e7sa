<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webslider;
use App\Application\Requests\Website\Webslider\AddRequestWebslider;
use App\Application\Requests\Website\Webslider\UpdateRequestWebslider;

class WebsliderController extends AbstractController
{

     public function __construct(Webslider $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webslider.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.webslider.edit' , $id);
        }


     public function store(AddRequestWebslider $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webslider');
     }

     public function update($id , UpdateRequestWebslider $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webslider.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webslider')->with('sucess' , 'Done Delete Webslider From system');
        }


}
