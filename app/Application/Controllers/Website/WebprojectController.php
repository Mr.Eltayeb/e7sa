<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webproject;
use App\Application\Requests\Website\Webproject\AddRequestWebproject;
use App\Application\Requests\Website\Webproject\UpdateRequestWebproject;

class WebprojectController extends AbstractController
{

     public function __construct(Webproject $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webproject.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.webproject.edit' , $id);
        }


     public function store(AddRequestWebproject $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webproject');
     }

     public function update($id , UpdateRequestWebproject $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webproject.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webproject')->with('sucess' , 'Done Delete Webproject From system');
        }


}
