<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webstate;
use App\Application\Requests\Website\Webstate\AddRequestWebstate;
use App\Application\Requests\Website\Webstate\UpdateRequestWebstate;

class WebstateController extends AbstractController
{

     public function __construct(Webstate $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webstate.index' , compact('items'));
        }

        public function show($id = null){

            return $this->createOrEdit('website.webstate.edit' , $id);
        }


     public function store(AddRequestWebstate $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webstate');
     }

     public function update($id , UpdateRequestWebstate $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webstate.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webstate')->with('sucess' , 'Done Delete Webstate From system');
        }


}
