<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webinfographic;
use App\Application\Requests\Website\Webinfographic\AddRequestWebinfographic;
use App\Application\Requests\Website\Webinfographic\UpdateRequestWebinfographic;

class WebinfographicController extends AbstractController
{

     public function __construct(Webinfographic $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webinfographic.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.webinfographic.edit' , $id);
        }


     public function store(AddRequestWebinfographic $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webinfographic');
     }

     public function update($id , UpdateRequestWebinfographic $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webinfographic.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webinfographic')->with('sucess' , 'Done Delete Webinfographic From system');
        }


}
