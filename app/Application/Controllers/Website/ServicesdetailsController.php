<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Servicesdetails;
use App\Application\Requests\Website\Servicesdetails\AddRequestServicesdetails;
use App\Application\Requests\Website\Servicesdetails\UpdateRequestServicesdetails;

class ServicesdetailsController extends AbstractController
{

     public function __construct(Servicesdetails $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.servicesdetails.index' , compact('items'));
        }

        public function show($id = null){

            return $this->createOrEdit('website.servicesdetails.edit' , $id);
        }


     public function store(AddRequestServicesdetails $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('servicesdetails');
     }

     public function update($id , UpdateRequestServicesdetails $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.servicesdetails.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'servicesdetails')->with('sucess' , 'Done Delete Servicesdetails From system');
        }


}
