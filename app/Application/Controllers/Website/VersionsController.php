<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Versions;
use App\Application\Requests\Website\Versions\AddRequestVersions;
use App\Application\Requests\Website\Versions\UpdateRequestVersions;

class VersionsController extends AbstractController
{

     public function __construct(Versions $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.versions.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.versions.edit' , $id);
        }


     public function store(AddRequestVersions $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('versions');
     }

     public function update($id , UpdateRequestVersions $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.versions.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'versions')->with('sucess' , 'Done Delete Versions From system');
        }


}
