<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Aboutcentral;
use GuzzleHttp\Psr7\Request;

//use App\Application\Requests\Website\Aboutcentral\AddRequestAboutcentral;
//use App\Application\Requests\Website\Aboutcentral\UpdateRequestAboutcentral;

class AboutcentralController extends AbstractController
{

     public function __construct(Aboutcentral $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.aboutcentral.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.aboutcentral.edit' , $id);
        }


     public function store(AddRequestAboutcentral $request){
//         $request->request->add(['user_id' => auth()->user()->id]);
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('aboutcentral');
     }

     public function update($id , UpdateRequestAboutcentral $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.aboutcentral.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'aboutcentral')->with('sucess' , 'Done Delete Aboutcentral From system');
        }


}
