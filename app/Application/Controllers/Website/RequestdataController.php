<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Requestdata;
use App\Application\Requests\Website\Requestdata\AddRequestRequestdata;
use App\Application\Requests\Website\Requestdata\UpdateRequestRequestdata;

class RequestdataController extends AbstractController
{

     public function __construct(Requestdata $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.requestdata.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.requestdata.edit' , $id);
        }


     public function store(AddRequestRequestdata $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('requestdata');
     }

     public function update($id , UpdateRequestRequestdata $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.requestdata.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'requestdata')->with('sucess' , 'Done Delete Requestdata From system');
        }


}
