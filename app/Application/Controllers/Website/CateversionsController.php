<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Cateversions;
use App\Application\Requests\Website\Cateversions\AddRequestCateversions;
use App\Application\Requests\Website\Cateversions\UpdateRequestCateversions;

class CateversionsController extends AbstractController
{

     public function __construct(Cateversions $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.cateversions.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.cateversions.edit' , $id);
        }


     public function store(AddRequestCateversions $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('cateversions');
     }

     public function update($id , UpdateRequestCateversions $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.cateversions.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'cateversions')->with('sucess' , 'Done Delete Cateversions From system');
        }


}
