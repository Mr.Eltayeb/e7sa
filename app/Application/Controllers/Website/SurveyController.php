<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Survey;
use App\Application\Requests\Website\Survey\AddRequestSurvey;
use App\Application\Requests\Website\Survey\UpdateRequestSurvey;

class SurveyController extends AbstractController
{

     public function __construct(Survey $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.survey.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.survey.edit' , $id);
        }


     public function store(AddRequestSurvey $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('survey');
     }

     public function update($id , UpdateRequestSurvey $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.survey.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'survey')->with('sucess' , 'Done Delete Survey From system');
        }


}
