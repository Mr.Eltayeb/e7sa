<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Directorycategories;
use App\Application\Requests\Website\Directorycategories\AddRequestDirectorycategories;
use App\Application\Requests\Website\Directorycategories\UpdateRequestDirectorycategories;

class DirectorycategoriesController extends AbstractController
{

     public function __construct(Directorycategories $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.directorycategories.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.directorycategories.edit' , $id);
        }


     public function store(AddRequestDirectorycategories $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('directorycategories');
     }

     public function update($id , UpdateRequestDirectorycategories $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.directorycategories.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'directorycategories')->with('sucess' , 'Done Delete Directorycategories From system');
        }


}
