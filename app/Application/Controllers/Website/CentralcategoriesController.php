<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Centralcategories;
use App\Application\Requests\Website\Centralcategories\AddRequestCentralcategories;
use App\Application\Requests\Website\Centralcategories\UpdateRequestCentralcategories;

class CentralcategoriesController extends AbstractController
{

     public function __construct(Centralcategories $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.centralcategories.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.centralcategories.edit' , $id);
        }


     public function store(AddRequestCentralcategories $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('centralcategories');
     }

     public function update($id , UpdateRequestCentralcategories $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.centralcategories.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'centralcategories')->with('sucess' , 'Done Delete Centralcategories From system');
        }


}
