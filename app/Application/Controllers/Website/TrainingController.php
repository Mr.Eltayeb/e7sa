<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Training;
use App\Application\Requests\Website\Training\AddRequestTraining;
use App\Application\Requests\Website\Training\UpdateRequestTraining;
use Auth;

class TrainingController extends AbstractController
{

    public function __construct(Training $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        $items = $this->model->paginate(env('PAGINATE'));
        return view('website.training.index', compact('items'));
    }

    public function show($id = null)
    {
        return $this->createOrEdit('website.training.edit', $id);
    }


    public function store(AddRequestTraining $request)
    {
        $request->request->add(['user_id' => auth()->user()->id]);

        $item = $this->storeOrUpdate($request, null, true);
        return redirect('training');
    }

    public function update($id, UpdateRequestTraining $request)
    {
        $request->request->add(['user_id' => auth()->user->id]);

        $item = $this->storeOrUpdate($request, $id, true);
        return redirect()->back();
    }


    public function getById($id)
    {
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('website.training.show', $id, ['fields' => $fields]);
    }

    public function destroy($id)
    {
        return $this->deleteItem($id, 'training')->with('sucess', 'Done Delete Training From system');
    }


}
