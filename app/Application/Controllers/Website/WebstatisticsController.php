<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webstatistics;
use App\Application\Requests\Website\Webstatistics\AddRequestWebstatistics;
use App\Application\Requests\Website\Webstatistics\UpdateRequestWebstatistics;

class WebstatisticsController extends AbstractController
{

     public function __construct(Webstatistics $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webstatistics.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.webstatistics.edit' , $id);
        }


     public function store(AddRequestWebstatistics $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webstatistics');
     }

     public function update($id , UpdateRequestWebstatistics $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webstatistics.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webstatistics')->with('sucess' , 'Done Delete Webstatistics From system');
        }


}
