<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webstatisticsadd;
use App\Application\Requests\Website\Webstatisticsadd\AddRequestWebstatisticsadd;
use App\Application\Requests\Website\Webstatisticsadd\UpdateRequestWebstatisticsadd;

class WebstatisticsaddController extends AbstractController
{

     public function __construct(Webstatisticsadd $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webstatisticsadd.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.webstatisticsadd.edit' , $id);
        }


     public function store(AddRequestWebstatisticsadd $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webstatisticsadd');
     }

     public function update($id , UpdateRequestWebstatisticsadd $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webstatisticsadd.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webstatisticsadd')->with('sucess' , 'Done Delete Webstatisticsadd From system');
        }


}
