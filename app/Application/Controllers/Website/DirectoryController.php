<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Directory;
use App\Application\Requests\Website\Directory\AddRequestDirectory;
use App\Application\Requests\Website\Directory\UpdateRequestDirectory;

class DirectoryController extends AbstractController
{

     public function __construct(Directory $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.directory.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.directory.edit' , $id);
        }


     public function store(AddRequestDirectory $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('directory');
     }

     public function update($id , UpdateRequestDirectory $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.directory.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'directory')->with('sucess' , 'Done Delete Directory From system');
        }


}
