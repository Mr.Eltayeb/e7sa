<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Fullnews;
use App\Application\Requests\Website\Fullnews\AddRequestFullnews;
use App\Application\Requests\Website\Fullnews\UpdateRequestFullnews;

class FullnewsController extends AbstractController
{

     public function __construct(Fullnews $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.fullnews.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.fullnews.edit' , $id);
        }


     public function store(AddRequestFullnews $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('fullnews');
     }

     public function update($id , UpdateRequestFullnews $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.fullnews.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'fullnews')->with('sucess' , 'Done Delete Fullnews From system');
        }


}
