<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webnews;
use App\Application\Requests\Website\Webnews\AddRequestWebnews;
use App\Application\Requests\Website\Webnews\UpdateRequestWebnews;

class WebnewsController extends AbstractController
{

     public function __construct(Webnews $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webnews.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.webnews.edit' , $id);
        }


     public function store(AddRequestWebnews $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webnews');
     }

     public function update($id , UpdateRequestWebnews $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webnews.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webnews')->with('sucess' , 'Done Delete Webnews From system');
        }


}
