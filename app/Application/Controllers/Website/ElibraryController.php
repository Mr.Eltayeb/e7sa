<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Elibrary;
use App\Application\Requests\Website\Elibrary\AddRequestElibrary;
use App\Application\Requests\Website\Elibrary\UpdateRequestElibrary;

class ElibraryController extends AbstractController
{

     public function __construct(Elibrary $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.elibrary.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.elibrary.edit' , $id);
        }


     public function store(AddRequestElibrary $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('elibrary');
     }

     public function update($id , UpdateRequestElibrary $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.elibrary.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'elibrary')->with('sucess' , 'Done Delete Elibrary From system');
        }


}
