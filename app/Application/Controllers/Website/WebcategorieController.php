<?php

namespace App\Application\Controllers\Website;

use App\Application\Controllers\AbstractController;
use Alert;
use App\Application\Model\Webcategorie;
use App\Application\Requests\Website\Webcategorie\AddRequestWebcategorie;
use App\Application\Requests\Website\Webcategorie\UpdateRequestWebcategorie;

class WebcategorieController extends AbstractController
{

     public function __construct(Webcategorie $model)
        {
            parent::__construct($model);
        }

        public function index(){
            $items = $this->model->paginate(env('PAGINATE'));
            return view('website.webcategorie.index' , compact('items'));
        }

        public function show($id = null){
            return $this->createOrEdit('website.webcategorie.edit' , $id);
        }


     public function store(AddRequestWebcategorie $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('webcategorie');
     }

     public function update($id , UpdateRequestWebcategorie $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


        public function getById($id){
            $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
            return $this->createOrEdit('website.webcategorie.show' , $id , ['fields' =>  $fields]);
        }

        public function destroy($id){
            return $this->deleteItem($id , 'webcategorie')->with('sucess' , 'Done Delete Webcategorie From system');
        }


}
