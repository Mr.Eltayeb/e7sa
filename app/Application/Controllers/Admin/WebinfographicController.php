<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webinfographic\AddRequestWebinfographic;
use App\Application\Requests\Admin\Webinfographic\UpdateRequestWebinfographic;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebinfographicsDataTable;
use App\Application\Model\Webinfographic;
use Yajra\Datatables\Request;
use Alert;

class WebinfographicController extends AbstractController
{
    public function __construct(Webinfographic $model)
    {
        parent::__construct($model);
    }

    public function index(WebinfographicsDataTable $dataTable){
        return $dataTable->render('admin.webinfographic.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webinfographic.edit' , $id);
    }

     public function store(AddRequestWebinfographic $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webinfographic');
     }

     public function update($id , UpdateRequestWebinfographic $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webinfographic.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webinfographic')->with('sucess' , 'Done Delete webinfographic From system');
    }
}
