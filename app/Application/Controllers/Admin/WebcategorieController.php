<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webcategorie\AddRequestWebcategorie;
use App\Application\Requests\Admin\Webcategorie\UpdateRequestWebcategorie;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebcategoriesDataTable;
use App\Application\Model\Webcategorie;
use Yajra\Datatables\Request;
use Alert;

class WebcategorieController extends AbstractController
{
    public function __construct(Webcategorie $model)
    {
        parent::__construct($model);
    }

    public function index(WebcategoriesDataTable $dataTable){
        return $dataTable->render('admin.webcategorie.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webcategorie.edit' , $id);
    }

     public function store(AddRequestWebcategorie $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webcategorie');
     }

     public function update($id , UpdateRequestWebcategorie $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webcategorie.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webcategorie')->with('sucess' , 'Done Delete webcategorie From system');
    }
}
