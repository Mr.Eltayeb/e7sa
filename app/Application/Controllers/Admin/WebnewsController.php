<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webnews\AddRequestWebnews;
use App\Application\Requests\Admin\Webnews\UpdateRequestWebnews;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebnewssDataTable;
use App\Application\Model\Webnews;
use Yajra\Datatables\Request;
use Alert;

class WebnewsController extends AbstractController
{
    public function __construct(Webnews $model)
    {
        parent::__construct($model);
    }

    public function index(WebnewssDataTable $dataTable){
        return $dataTable->render('admin.webnews.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webnews.edit' , $id);
    }

     public function store(AddRequestWebnews $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webnews');
     }

     public function update($id , UpdateRequestWebnews $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webnews.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webnews')->with('sucess' , 'Done Delete webnews From system');
    }
}
