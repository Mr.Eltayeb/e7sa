<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Servicesdetails\AddRequestServicesdetails;
use App\Application\Requests\Admin\Servicesdetails\UpdateRequestServicesdetails;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\ServicesdetailssDataTable;
use App\Application\Model\Servicesdetails;
use Yajra\Datatables\Request;
use Alert;

class ServicesdetailsController extends AbstractController
{
    public function __construct(Servicesdetails $model)
    {
        parent::__construct($model);
    }

    public function index(ServicesdetailssDataTable $dataTable){
        return $dataTable->render('admin.servicesdetails.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.servicesdetails.edit' , $id);
    }

     public function store(AddRequestServicesdetails $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/servicesdetails');
     }

     public function update($id , UpdateRequestServicesdetails $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.servicesdetails.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/servicesdetails')->with('sucess' , 'Done Delete servicesdetails From system');
    }
}
