<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Survey\AddRequestSurvey;
use App\Application\Requests\Admin\Survey\UpdateRequestSurvey;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\SurveysDataTable;
use App\Application\Model\Survey;
use Yajra\Datatables\Request;
use Alert;

class SurveyController extends AbstractController
{
    public function __construct(Survey $model)
    {
        parent::__construct($model);
    }

    public function index(SurveysDataTable $dataTable){
        return $dataTable->render('admin.survey.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.survey.edit' , $id);
    }

     public function store(AddRequestSurvey $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/survey');
     }

     public function update($id , UpdateRequestSurvey $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.survey.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/survey')->with('sucess' , 'Done Delete survey From system');
    }
}
