<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Directory\AddRequestDirectory;
use App\Application\Requests\Admin\Directory\UpdateRequestDirectory;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\DirectorysDataTable;
use App\Application\Model\Directory;
use Yajra\Datatables\Request;
use Alert;

class DirectoryController extends AbstractController
{
    public function __construct(Directory $model)
    {
        parent::__construct($model);
    }

    public function index(DirectorysDataTable $dataTable){
        return $dataTable->render('admin.directory.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.directory.edit' , $id);
    }

     public function store(AddRequestDirectory $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/directory');
     }

     public function update($id , UpdateRequestDirectory $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.directory.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/directory')->with('sucess' , 'Done Delete directory From system');
    }
}
