<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Centralcategories\AddRequestCentralcategories;
use App\Application\Requests\Admin\Centralcategories\UpdateRequestCentralcategories;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\CentralcategoriessDataTable;
use App\Application\Model\Centralcategories;
use Yajra\Datatables\Request;
use Alert;

class CentralcategoriesController extends AbstractController
{
    public function __construct(Centralcategories $model)
    {
        parent::__construct($model);
    }

    public function index(CentralcategoriessDataTable $dataTable){
        return $dataTable->render('admin.centralcategories.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.centralcategories.edit' , $id);
    }

     public function store(AddRequestCentralcategories $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/centralcategories');
     }

     public function update($id , UpdateRequestCentralcategories $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.centralcategories.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/centralcategories')->with('sucess' , 'Done Delete centralcategories From system');
    }
}
