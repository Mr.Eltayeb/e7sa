<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Surveyans\AddRequestSurveyans;
use App\Application\Requests\Admin\Surveyans\UpdateRequestSurveyans;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\SurveyanssDataTable;
use App\Application\Model\Surveyans;
use Yajra\Datatables\Request;
use Alert;

class SurveyansController extends AbstractController
{
    public function __construct(Surveyans $model)
    {
        parent::__construct($model);
    }

    public function index(SurveyanssDataTable $dataTable){
        return $dataTable->render('admin.surveyans.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.surveyans.edit' , $id);
    }

     public function store(AddRequestSurveyans $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/surveyans');
     }

     public function update($id , UpdateRequestSurveyans $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.surveyans.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/surveyans')->with('sucess' , 'Done Delete surveyans From system');
    }
}
