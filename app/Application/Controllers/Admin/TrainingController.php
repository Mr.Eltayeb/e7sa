<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Training\AddRequestTraining;
use App\Application\Requests\Admin\Training\UpdateRequestTraining;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\TrainingsDataTable;
use App\Application\Model\Training;
use Yajra\Datatables\Request;
use Alert;

class TrainingController extends AbstractController
{
    public function __construct(Training $model)
    {
        parent::__construct($model);
    }

    public function index(TrainingsDataTable $dataTable){
        $items = Training::all();
        return view('admin.training.index', compact(['items','dataTable']));

//        return $dataTable->render('admin.training.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.training.edit' , $id);
    }

     public function store(AddRequestTraining $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/training');
     }

     public function update($id , UpdateRequestTraining $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.training.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/training')->with('sucess' , 'Done Delete training From system');
    }
}
