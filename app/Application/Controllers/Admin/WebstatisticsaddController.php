<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webstatisticsadd\AddRequestWebstatisticsadd;
use App\Application\Requests\Admin\Webstatisticsadd\UpdateRequestWebstatisticsadd;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebstatisticsaddsDataTable;
use App\Application\Model\Webstatisticsadd;
use Yajra\Datatables\Request;
use Alert;

class WebstatisticsaddController extends AbstractController
{
    public function __construct(Webstatisticsadd $model)
    {
        parent::__construct($model);
    }

    public function index(WebstatisticsaddsDataTable $dataTable){
        return $dataTable->render('admin.webstatisticsadd.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webstatisticsadd.edit' , $id);
    }

     public function store(AddRequestWebstatisticsadd $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webstatisticsadd');
     }

     public function update($id , UpdateRequestWebstatisticsadd $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webstatisticsadd.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webstatisticsadd')->with('sucess' , 'Done Delete webstatisticsadd From system');
    }
}
