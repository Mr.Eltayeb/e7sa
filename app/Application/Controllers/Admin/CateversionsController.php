<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Cateversions\AddRequestCateversions;
use App\Application\Requests\Admin\Cateversions\UpdateRequestCateversions;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\CateversionssDataTable;
use App\Application\Model\Cateversions;
use Yajra\Datatables\Request;
use Alert;

class CateversionsController extends AbstractController
{
    public function __construct(Cateversions $model)
    {
        parent::__construct($model);
    }

    public function index(CateversionssDataTable $dataTable){
        return $dataTable->render('admin.cateversions.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.cateversions.edit' , $id);
    }

     public function store(AddRequestCateversions $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/cateversions');
     }

     public function update($id , UpdateRequestCateversions $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.cateversions.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/cateversions')->with('sucess' , 'Done Delete cateversions From system');
    }
}
