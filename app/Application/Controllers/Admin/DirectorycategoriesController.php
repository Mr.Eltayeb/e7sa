<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Directorycategories\AddRequestDirectorycategories;
use App\Application\Requests\Admin\Directorycategories\UpdateRequestDirectorycategories;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\DirectorycategoriessDataTable;
use App\Application\Model\Directorycategories;
use Yajra\Datatables\Request;
use Alert;

class DirectorycategoriesController extends AbstractController
{
    public function __construct(Directorycategories $model)
    {
        parent::__construct($model);
    }

    public function index(DirectorycategoriessDataTable $dataTable){
        return $dataTable->render('admin.directorycategories.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.directorycategories.edit' , $id);
    }

     public function store(AddRequestDirectorycategories $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/directorycategories');
     }

     public function update($id , UpdateRequestDirectorycategories $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.directorycategories.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/directorycategories')->with('sucess' , 'Done Delete directorycategories From system');
    }
}
