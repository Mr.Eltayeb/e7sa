<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Fullnews\AddRequestFullnews;
use App\Application\Requests\Admin\Fullnews\UpdateRequestFullnews;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\FullnewssDataTable;
use App\Application\Model\Fullnews;
use Yajra\Datatables\Request;
use Alert;

class FullnewsController extends AbstractController
{
    public function __construct(Fullnews $model)
    {
        parent::__construct($model);
    }

    public function index(FullnewssDataTable $dataTable){
        return $dataTable->render('admin.fullnews.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.fullnews.edit' , $id);
    }

     public function store(AddRequestFullnews $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/fullnews');
     }

     public function update($id , UpdateRequestFullnews $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.fullnews.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/fullnews')->with('sucess' , 'Done Delete fullnews From system');
    }
}
