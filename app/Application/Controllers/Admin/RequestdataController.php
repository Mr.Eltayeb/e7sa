<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Requestdata\AddRequestRequestdata;
use App\Application\Requests\Admin\Requestdata\UpdateRequestRequestdata;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\RequestdatasDataTable;
use App\Application\Model\Requestdata;
use Yajra\Datatables\Request;
use Alert;

class RequestdataController extends AbstractController
{
    public function __construct(Requestdata $model)
    {
        parent::__construct($model);
    }

    public function index(RequestdatasDataTable $dataTable){
//        $items = $this->model->paginate(env('PAGINATE'));
        $items = Requestdata::all();
        return view('admin.requestdata.index' , compact(['items','dataTable']));
//        return $dataTable->render('admin.requestdata.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.requestdata.edit' , $id);
    }

     public function store(AddRequestRequestdata $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/requestdata');
     }

     public function update($id , UpdateRequestRequestdata $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.requestdata.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/requestdata')->with('sucess' , 'Done Delete requestdata From system');
    }
}
