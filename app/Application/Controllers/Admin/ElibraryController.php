<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Elibrary\AddRequestElibrary;
use App\Application\Requests\Admin\Elibrary\UpdateRequestElibrary;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\ElibrarysDataTable;
use App\Application\Model\Elibrary;
use Yajra\Datatables\Request;
use Alert;

class ElibraryController extends AbstractController
{
    public function __construct(Elibrary $model)
    {
        parent::__construct($model);
    }

    public function index(ElibrarysDataTable $dataTable){
        return $dataTable->render('admin.elibrary.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.elibrary.edit' , $id);
    }

     public function store(AddRequestElibrary $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/elibrary');
     }

     public function update($id , UpdateRequestElibrary $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.elibrary.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/elibrary')->with('sucess' , 'Done Delete elibrary From system');
    }
}
