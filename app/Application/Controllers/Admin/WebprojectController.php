<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webproject\AddRequestWebproject;
use App\Application\Requests\Admin\Webproject\UpdateRequestWebproject;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebprojectsDataTable;
use App\Application\Model\Webproject;
use Yajra\Datatables\Request;
use Alert;

class WebprojectController extends AbstractController
{
    public function __construct(Webproject $model)
    {
        parent::__construct($model);
    }

    public function index(WebprojectsDataTable $dataTable){
        return $dataTable->render('admin.webproject.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webproject.edit' , $id);
    }

     public function store(AddRequestWebproject $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webproject');
     }

     public function update($id , UpdateRequestWebproject $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webproject.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webproject')->with('sucess' , 'Done Delete webproject From system');
    }
}
