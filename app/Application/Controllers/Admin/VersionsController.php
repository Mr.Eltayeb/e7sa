<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Versions\AddRequestVersions;
use App\Application\Requests\Admin\Versions\UpdateRequestVersions;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\VersionssDataTable;
use App\Application\Model\Versions;
use Yajra\Datatables\Request;
use Alert;

class VersionsController extends AbstractController
{
    public function __construct(Versions $model)
    {
        parent::__construct($model);
    }

    public function index(VersionssDataTable $dataTable){
        return $dataTable->render('admin.versions.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.versions.edit' , $id);
    }

     public function store(AddRequestVersions $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/versions');
     }

     public function update($id , UpdateRequestVersions $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.versions.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/versions')->with('sucess' , 'Done Delete versions From system');
    }
}
