<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Aboutcentral\AddRequestAboutcentral;
use App\Application\Requests\Admin\Aboutcentral\UpdateRequestAboutcentral;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\AboutcentralsDataTable;
use App\Application\Model\Aboutcentral;
use Yajra\Datatables\Request;
use Alert;

class AboutcentralController extends AbstractController
{
    public function __construct(Aboutcentral $model)
    {
        parent::__construct($model);
    }

    public function index(AboutcentralsDataTable $dataTable){
        return $dataTable->render('admin.aboutcentral.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.aboutcentral.edit' , $id);
    }

     public function store(AddRequestAboutcentral $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/aboutcentral');
     }

     public function update($id , UpdateRequestAboutcentral $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.aboutcentral.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/aboutcentral')->with('sucess' , 'Done Delete aboutcentral From system');
    }
}
