<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webstate\AddRequestWebstate;
use App\Application\Requests\Admin\Webstate\UpdateRequestWebstate;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebstatesDataTable;
use App\Application\Model\Webstate;
use Yajra\Datatables\Request;
use Alert;

class WebstateController extends AbstractController
{
    public function __construct(Webstate $model)
    {
        parent::__construct($model);
    }

    public function index(WebstatesDataTable $dataTable){
        return $dataTable->render('admin.webstate.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webstate.edit' , $id);
    }

     public function store(AddRequestWebstate $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webstate');
     }

     public function update($id , UpdateRequestWebstate $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webstate.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webstate')->with('sucess' , 'Done Delete webstate From system');
    }
}
