<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webslider\AddRequestWebslider;
use App\Application\Requests\Admin\Webslider\UpdateRequestWebslider;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebslidersDataTable;
use App\Application\Model\Webslider;
use Yajra\Datatables\Request;
use Alert;

class WebsliderController extends AbstractController
{
    public function __construct(Webslider $model)
    {
        parent::__construct($model);
    }

    public function index(WebslidersDataTable $dataTable){
        return $dataTable->render('admin.webslider.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webslider.edit' , $id);
    }

     public function store(AddRequestWebslider $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webslider');
     }

     public function update($id , UpdateRequestWebslider $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webslider.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webslider')->with('sucess' , 'Done Delete webslider From system');
    }
}
