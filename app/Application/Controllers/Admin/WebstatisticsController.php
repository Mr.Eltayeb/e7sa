<?php

namespace App\Application\Controllers\Admin;

use App\Application\Requests\Admin\Webstatistics\AddRequestWebstatistics;
use App\Application\Requests\Admin\Webstatistics\UpdateRequestWebstatistics;
use App\Application\Controllers\AbstractController;
use App\Application\DataTables\WebstatisticssDataTable;
use App\Application\Model\Webstatistics;
use Yajra\Datatables\Request;
use Alert;

class WebstatisticsController extends AbstractController
{
    public function __construct(Webstatistics $model)
    {
        parent::__construct($model);
    }

    public function index(WebstatisticssDataTable $dataTable){
        return $dataTable->render('admin.webstatistics.index');
    }

    public function show($id = null){
        return $this->createOrEdit('admin.webstatistics.edit' , $id);
    }

     public function store(AddRequestWebstatistics $request){
          $item =  $this->storeOrUpdate($request , null , true);
          return redirect('admin/webstatistics');
     }

     public function update($id , UpdateRequestWebstatistics $request){
          $item =  $this->storeOrUpdate($request , $id , true);
          return redirect()->back();
     }


    public function getById($id){
        $fields = $this->model->getConnection()->getSchemaBuilder()->getColumnListing($this->model->getTable());
        return $this->createOrEdit('admin.webstatistics.show' , $id , ['fields' =>  $fields]);
    }

    public function destroy($id){
        return $this->deleteItem($id , 'admin/webstatistics')->with('sucess' , 'Done Delete webstatistics From system');
    }
}
