<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Aboutcentral extends Model
{

  public $table = "aboutcentral";

   protected $fillable = [
        'title','body'
   ];

}
