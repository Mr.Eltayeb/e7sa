<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Webnews extends Model
{

  public $table = "webnews";

   protected $fillable = [
        'title','url'
   ];

}
