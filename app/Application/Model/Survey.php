<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{

  public $table = "survey";

   protected $fillable = [
        'title','answers1','answers2','answers3'
   ];

}
