<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Directory extends Model
{
   public $table = "directory";
   public function directorycategories(){
		return $this->belongsTo(Directorycategories::class, "directorycategories_id");
		}
    protected $fillable = [
   'directorycategories_id',
        'title', 'body', 'file','file_en'
   ];
 }
