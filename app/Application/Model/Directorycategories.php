<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Directorycategories extends Model
{
   public $table = "directorycategories";
   public function directory(){
		return $this->hasMany(Directory::class, "directorycategories_id");
		}
    protected $fillable = [
        'title'
   ];
 }
