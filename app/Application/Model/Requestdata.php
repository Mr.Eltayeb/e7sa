<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Requestdata extends Model
{

  public $table = "requestdata";

   protected $fillable = [
        'name','type_identity','Identity','occupation','qualification','phone','fax','email','data_usage','company_name','type_Company','Data_usage_area','data_details'
   ];

}
