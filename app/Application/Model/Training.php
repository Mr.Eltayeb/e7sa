<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    public $table = "training";

    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }

    protected $fillable = [
        'user_id',
        'name', 'university', 'college', 'specialty', 'phone', 'email', 'type_training', 'type_college', 'departments', 'competence', 'request_status'
    ];
}
