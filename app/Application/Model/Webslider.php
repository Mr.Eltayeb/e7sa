<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Webslider extends Model
{
   public $table = "webslider";
   public function webcategorie(){
		return $this->belongsTo(Webcategorie::class, "webcategorie_id");
		}
    protected $fillable = [
   'webcategorie_id',
        'title','image','url'
   ];
 }
