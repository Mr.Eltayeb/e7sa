<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Elibrary extends Model
{

  public $table = "elibrary";

   protected $fillable = [
        'title','image','file','file_en'
   ];

}
