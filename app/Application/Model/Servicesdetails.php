<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Servicesdetails extends Model
{

  public $table = "servicesdetails";

   protected $fillable = [
        'title','image','body', 'url'
   ];

}
