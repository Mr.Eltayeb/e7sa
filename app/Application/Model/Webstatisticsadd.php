<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Webstatisticsadd extends Model
{
    public $table = "webstatisticsadd";

    public function webstatistics()
    {
        return $this->belongsTo(Webstatistics::class, "webstatistics_id");
    }

    protected $fillable = [
        'webstatistics_id',
        'title', 'body', 'file_ar', 'file_en'
    ];
}
