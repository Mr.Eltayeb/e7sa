<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Webinfographic extends Model
{
    public $table = "webinfographic";

    public function webcategorie()
    {
        return $this->belongsTo(Webcategorie::class, "webcategorie_id");
    }

    protected $fillable = [
        'webcategorie_id',
        'title', 'image', 'url'
    ];
}
