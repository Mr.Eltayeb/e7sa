<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Centralcategories extends Model
{
   public $table = "centralcategories";
    protected $fillable = [
        'title', 'body', 'file','file_en'
   ];
 }
