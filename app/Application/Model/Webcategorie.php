<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Webcategorie extends Model
{
   public $table = "webcategorie";
   public function webslider(){
		return $this->hasOne(Webslider::class, "webcategorie_id");
		}
   public function webinfographic(){
  return $this->hasOne(Webinfographic::class, "webcategorie_id");
  }
    protected $fillable = [
        'title'
   ];
 }
