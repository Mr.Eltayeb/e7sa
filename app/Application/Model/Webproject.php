<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Webproject extends Model
{

  public $table = "webproject";

   protected $fillable = [
        'title','body'
   ];

}
