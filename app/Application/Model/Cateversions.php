<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Cateversions extends Model
{
   public $table = "cateversions";
  public function versions(){
		return $this->hasMany(Versions::class, "cateversions_id");
		}
    protected $fillable = [
        'title'
   ];
 }
