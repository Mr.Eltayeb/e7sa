<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Webstate extends Model
{

  public $table = "webstate";

   protected $fillable = [
        'title','image','body', 'file'
   ];

}
