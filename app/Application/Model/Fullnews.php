<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Fullnews extends Model
{

  public $table = "fullnews";

   protected $fillable = [
        'title','image','body'
   ];

}
