<?php

namespace App\Application\Model;

use Illuminate\Database\Eloquent\Model;

class Surveyans extends Model
{

  public $table = "surveyans";

   protected $fillable = [
        'answers'
   ];

}
