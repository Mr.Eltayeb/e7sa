<?php
 namespace App\Application\Model;
 use Illuminate\Database\Eloquent\Model;
 class Versions extends Model
{
   public $table = "versions";
   public function cateversions(){
		return $this->belongsTo(Cateversions::class, "cateversions_id");
		}
    protected $fillable = [
   'cateversions_id',
        'title','image', 'body', 'file','file_en','year'
   ];
 }
