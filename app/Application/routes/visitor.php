<?php

Route::get('/', 'HomeController@welcome');
Route::get('/home', 'HomeController@welcome');

Route::get('/page/{slug}', 'HomeController@getPageBySlug');
Route::get('contact', 'ContactController@index');
Route::post('contact', 'ContactController@storeContact');


#### webcategorie control
Route::get('webcategorie', 'WebcategorieController@index');
Route::get('webcategorie/item/{id?}', 'WebcategorieController@show');
Route::get('webcategorie/{id}/view', 'WebcategorieController@getById');

#### webnews control
Route::get('webnews', 'WebnewsController@index');
Route::get('webnews/item/{id?}', 'WebnewsController@show');
Route::get('webnews/{id}/view', 'WebnewsController@getById');


#### webslider control
Route::get('webslider', 'WebsliderController@index');
Route::get('webslider/item/{id?}', 'WebsliderController@show');
Route::get('webslider/{id}/view', 'WebsliderController@getById');

#### webinfographic control
Route::get('webinfographic', 'WebinfographicController@index');
Route::get('webinfographic/item/{id?}', 'WebinfographicController@show');
Route::get('webinfographic/{id}/view', 'WebinfographicController@getById');


#### webstate control
Route::get('webstate', 'WebstateController@index');
Route::get('webstate/item/{id?}', 'WebstateController@show');
Route::get('webstate/{id}/view', 'WebstateController@getById');

#### aboutcentral control
Route::get('aboutcentral', 'AboutcentralController@index');
Route::get('aboutcentral/item/{id?}', 'AboutcentralController@show');
Route::get('aboutcentral/{id}/view', 'AboutcentralController@getById');

#### webstatistics control
Route::get('webstatistics', 'WebstatisticsController@index');
Route::get('webstatistics/item/{id?}', 'WebstatisticsController@show');
Route::get('webstatistics/{id}/view', 'WebstatisticsController@getById');

#### webstatisticsadd control
Route::get('webstatisticsadd', 'WebstatisticsaddController@index');
Route::get('webstatisticsadd/item/{id?}', 'WebstatisticsaddController@show');
Route::get('webstatisticsadd/{id}/view', 'WebstatisticsaddController@getById');

#### versions control
Route::get('versions', 'VersionsController@index');
Route::get('versions/item/{id?}', 'VersionsController@show');
Route::get('versions/{id}/view', 'VersionsController@getById');

#### cateversions control
Route::get('cateversions', 'CateversionsController@index');
Route::get('cateversions/item/{id?}', 'CateversionsController@show');
Route::get('cateversions/{id}/view', 'CateversionsController@getById');


#### directory control
Route::get('directory', 'DirectoryController@index');
Route::get('directory/item/{id?}', 'DirectoryController@show');
Route::get('directory/{id}/view', 'DirectoryController@getById');

#### directorycategories control
Route::get('directorycategories', 'DirectorycategoriesController@index');
Route::get('directorycategories/item/{id?}', 'DirectorycategoriesController@show');
Route::get('directorycategories/{id}/view', 'DirectorycategoriesController@getById');


#### webproject control
Route::get('webproject', 'WebprojectController@index');
Route::get('webproject/item/{id?}', 'WebprojectController@show');
Route::get('webproject/{id}/view', 'WebprojectController@getById');

#### centralcategories control
Route::get('centralcategories', 'CentralcategoriesController@index');
Route::get('centralcategories/item/{id?}', 'CentralcategoriesController@show');
Route::get('centralcategories/{id}/view', 'CentralcategoriesController@getById');


#### newsletter control
Route::get('newsletter', 'NewsletterController@index');
Route::get('newsletter/item/{id?}', 'NewsletterController@show');
Route::get('newsletter/{id}/view', 'NewsletterController@getById');

#### elibrary control
Route::get('elibrary', 'ElibraryController@index');
Route::get('elibrary/item/{id?}', 'ElibraryController@show');
Route::get('elibrary/{id}/view', 'ElibraryController@getById');

#### fullnews control
Route::get('fullnews', 'FullnewsController@index');
Route::get('fullnews/item/{id?}', 'FullnewsController@show');
Route::post('fullnews/item', 'FullnewsController@store');
Route::get('fullnews/{id}/view', 'FullnewsController@getById');


#### training control
Route::get('training', 'TrainingController@index');
Route::get('training/item/{id?}', 'TrainingController@show');
Route::post('training/item', 'TrainingController@store');
Route::post('training/item/{id}', 'TrainingController@update');
Route::get('training/{id}/delete', 'TrainingController@destroy');
Route::get('training/{id}/view', 'TrainingController@getById');


#### requestdata control
Route::get('requestdata', 'RequestdataController@index');
Route::get('requestdata/item/{id?}', 'RequestdataController@show');
Route::post('requestdata/item', 'RequestdataController@store');
Route::post('requestdata/item/{id}', 'RequestdataController@update');
Route::get('requestdata/{id}/delete', 'RequestdataController@destroy');
Route::get('requestdata/{id}/view', 'RequestdataController@getById');

###Servicesdetails control
Route::get('servicesdetails', 'ServicesdetailsController@index');
Route::get('servicesdetails/item/{id?}', 'ServicesdetailsController@show');
Route::get('servicesdetails/{id}/view', 'ServicesdetailsController@getById');


#### survey control
Route::get('survey' , 'SurveyController@index');
Route::get('survey/item/{id?}' , 'SurveyController@show');
Route::post('survey/item' , 'SurveyController@store');
Route::post('survey/item/{id}' , 'SurveyController@update');
Route::get('survey/{id}/delete' , 'SurveyController@destroy');
Route::get('survey/{id}/view' , 'SurveyController@getById');





#### surveyans control
Route::post('surveyans/item' , 'SurveyansController@store');




Auth::routes();


