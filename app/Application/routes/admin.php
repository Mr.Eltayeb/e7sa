<?php
Route::get('icons', 'HomeController@icons');
Route::get('docs', 'HomeController@apiDocs');

### commands
Route::get('commands', 'CommandsController@index');
Route::post('command/exe', 'CommandsController@exe');
Route::get('laravel/commands', 'CommandsController@command');
Route::post('command/otherExe', 'CommandsController@otherExe');
Route::post('laravel/haveCommand', 'CommandsController@haveCommand');


### relations
Route::get('relation', 'RelationController@index');
Route::post('relation/exe', 'RelationController@exe');
Route::get('getCols/{model}', 'RelationController@getCols');
Route::post('relation/rollback', 'RelationController@rollback');

#### user control
Route::get('user', 'UserController@index');
Route::get('user/item/{id?}', 'UserController@show');
Route::post('user/item', 'UserController@store');
Route::post('user/item/{id}', 'UserController@update');
Route::get('user/{id}/delete', 'UserController@destroy');
Route::get('user/{id}/view', 'UserController@getById');

#### translation
Route::get('translation' , 'TranslationController@index');
Route::get('translation/readFile/{file}' , 'TranslationController@readFile');
Route::post('translation/save' , 'TranslationController@save');
Route::get('translation/getAllContent/{file}' , 'TranslationController@getAllContent');
Route::post('translation/both/save' , 'TranslationController@bothSave');

#### permissions
Route::get('custom-permissions' , 'Development\CustomPermissionsController@index');
Route::get('custom-permissions/readFile/{file}' , 'Development\CustomPermissionsController@readFile');
Route::post('custom-permissions/save' , 'Development\CustomPermissionsController@save');
Route::get('getControllerByType/{type}' , 'Development\PermissionController@getControllerByType');
Route::get('getMethodByController/{controller}/{type}' , 'Development\PermissionController@getMethodByController');


#### group control
Route::get('group', 'GroupController@index');
Route::get('group/item/{id?}', 'GroupController@show');
Route::post('group/item', 'GroupController@store');
Route::post('group/item/{id}', 'GroupController@update');
Route::get('group/{id}/delete', 'GroupController@destroy');
Route::get('group/{id}/view', 'GroupController@getById');
#### role control
Route::get('role', 'RoleController@index');
Route::get('role/item/{id?}', 'RoleController@show');
Route::post('role/item', 'RoleController@store');
Route::post('role/item/{id}', 'RoleController@update');
Route::get('role/{id}/delete', 'RoleController@destroy');
Route::get('role/{id}/view', 'RoleController@getById');
#### permission control
Route::get('permission', 'Development\PermissionController@index');
Route::get('permission/item/{id?}', 'Development\PermissionController@show');
Route::post('permission/item', 'Development\PermissionController@store');
Route::post('permission/item/{id}', 'Development\PermissionController@update');
Route::get('permission/{id}/delete', 'Development\PermissionController@destroy');
Route::get('permission/{id}/view', 'Development\PermissionController@getById');
#### home control
Route::get('home/{pages?}/{limit?}', 'HomeController@index');
#### setting control
Route::get('setting', 'SettingController@index');
Route::get('setting/item/{id?}', 'SettingController@show');
Route::post('setting/item', 'SettingController@store');
Route::post('setting/item/{id}', 'SettingController@update');
Route::get('setting/{id}/delete', 'SettingController@destroy');
Route::get('setting/{id}/view', 'SettingController@getById');
#### menu control
Route::get('menu', 'MenuController@index');
Route::get('menu/item/{id?}', 'MenuController@show');
Route::post('menu/item', 'MenuController@store');
Route::post('menu/item/{id}', 'MenuController@update');
Route::get('menu/{id}/delete', 'MenuController@destroy');
Route::get('menu/{id}/view', 'MenuController@getById');
Route::post('update/menuItem', 'MenuController@menuItem');
Route::post('addNewItemToMenu', 'MenuController@addNewItemToMenu');
Route::get('deleteMenuItem/{id}', 'MenuController@deleteMenuItem');
Route::get('getItemInfo/{id}', 'MenuController@getItemInfo');
Route::post('updateOneMenuItem', 'MenuController@updateOneMenuItem');
#### page control
Route::get('page', 'PageController@index');
Route::get('page/item/{id?}', 'PageController@show');
Route::post('page/item', 'PageController@store');
Route::post('page/item/{id}', 'PageController@update');
Route::get('page/{id}/delete', 'PageController@destroy');
Route::get('page/{id}/view', 'PageController@getById');
#### log control
Route::get('log', 'LogController@index');
Route::get('log/item/{id?}', 'LogController@show');
Route::post('log/item', 'LogController@store');
Route::post('log/item/{id}', 'LogController@update');
Route::get('log/{id}/delete', 'LogController@destroy');
Route::get('log/{id}/view', 'LogController@getById');
#### categorie control
Route::get('categorie', 'CategorieController@index');
Route::get('categorie/item/{id?}', 'CategorieController@show');
Route::post('categorie/item', 'CategorieController@store');
Route::post('categorie/item/{id}', 'CategorieController@update');
Route::get('categorie/{id}/delete', 'CategorieController@destroy');
Route::get('categorie/{id}/view', 'CategorieController@getById');
#### contact control
Route::get('contact', 'ContactController@index');
Route::get('contact/item/{id?}', 'ContactController@show');
Route::post('contact/item', 'ContactController@store');
Route::post('contact/item/{id}', 'ContactController@update');
Route::get('contact/{id}/delete', 'ContactController@destroy');
Route::get('contact/{id}/view', 'ContactController@getById');
Route::post('contact/replay/{id}', 'ContactController@replayEmail');

#### post comment

#### webcategorie control
Route::get('webcategorie' , 'WebcategorieController@index');
Route::get('webcategorie/item/{id?}' , 'WebcategorieController@show');
Route::post('webcategorie/item' , 'WebcategorieController@store');
Route::post('webcategorie/item/{id}' , 'WebcategorieController@update');
Route::get('webcategorie/{id}/delete' , 'WebcategorieController@destroy');
Route::get('webcategorie/{id}/view' , 'WebcategorieController@getById');
#### webnews control
Route::get('webnews' , 'WebnewsController@index');
Route::get('webnews/item/{id?}' , 'WebnewsController@show');
Route::post('webnews/item' , 'WebnewsController@store');
Route::post('webnews/item/{id}' , 'WebnewsController@update');
Route::get('webnews/{id}/delete' , 'WebnewsController@destroy');
Route::get('webnews/{id}/view' , 'WebnewsController@getById');
#### webslider control
Route::get('webslider' , 'WebsliderController@index');
Route::get('webslider/item/{id?}' , 'WebsliderController@show');
Route::post('webslider/item' , 'WebsliderController@store');
Route::post('webslider/item/{id}' , 'WebsliderController@update');
Route::get('webslider/{id}/delete' , 'WebsliderController@destroy');
Route::get('webslider/{id}/view' , 'WebsliderController@getById');
#### webinfographic control
Route::get('webinfographic' , 'WebinfographicController@index');
Route::get('webinfographic/item/{id?}' , 'WebinfographicController@show');
Route::post('webinfographic/item' , 'WebinfographicController@store');
Route::post('webinfographic/item/{id}' , 'WebinfographicController@update');
Route::get('webinfographic/{id}/delete' , 'WebinfographicController@destroy');
Route::get('webinfographic/{id}/view' , 'WebinfographicController@getById');

#### webstate control
Route::get('webstate' , 'WebstateController@index');
Route::get('webstate/item/{id?}' , 'WebstateController@show');
Route::post('webstate/item' , 'WebstateController@store');
Route::post('webstate/item/{id}' , 'WebstateController@update');
Route::get('webstate/{id}/delete' , 'WebstateController@destroy');
Route::get('webstate/{id}/view' , 'WebstateController@getById');
#### aboutcentral control
Route::get('aboutcentral' , 'AboutcentralController@index');
Route::get('aboutcentral/item/{id?}' , 'AboutcentralController@show');
Route::post('aboutcentral/item' , 'AboutcentralController@store');
Route::post('aboutcentral/item/{id}' , 'AboutcentralController@update');
Route::get('aboutcentral/{id}/delete' , 'AboutcentralController@destroy');
Route::get('aboutcentral/{id}/view' , 'AboutcentralController@getById');
#### webstatistics control
Route::get('webstatistics' , 'WebstatisticsController@index');
Route::get('webstatistics/item/{id?}' , 'WebstatisticsController@show');
Route::post('webstatistics/item' , 'WebstatisticsController@store');
Route::post('webstatistics/item/{id}' , 'WebstatisticsController@update');
Route::get('webstatistics/{id}/delete' , 'WebstatisticsController@destroy');
Route::get('webstatistics/{id}/view' , 'WebstatisticsController@getById');
#### webstatisticsadd control
Route::get('webstatisticsadd' , 'WebstatisticsaddController@index');
Route::get('webstatisticsadd/item/{id?}' , 'WebstatisticsaddController@show');
Route::post('webstatisticsadd/item' , 'WebstatisticsaddController@store');
Route::post('webstatisticsadd/item/{id}' , 'WebstatisticsaddController@update');
Route::get('webstatisticsadd/{id}/delete' , 'WebstatisticsaddController@destroy');
Route::get('webstatisticsadd/{id}/view' , 'WebstatisticsaddController@getById');
#### versions control
Route::get('versions' , 'VersionsController@index');
Route::get('versions/item/{id?}' , 'VersionsController@show');
Route::post('versions/item' , 'VersionsController@store');
Route::post('versions/item/{id}' , 'VersionsController@update');
Route::get('versions/{id}/delete' , 'VersionsController@destroy');
Route::get('versions/{id}/view' , 'VersionsController@getById');
#### cateversions control
Route::get('cateversions' , 'CateversionsController@index');
Route::get('cateversions/item/{id?}' , 'CateversionsController@show');
Route::post('cateversions/item' , 'CateversionsController@store');
Route::post('cateversions/item/{id}' , 'CateversionsController@update');
Route::get('cateversions/{id}/delete' , 'CateversionsController@destroy');
Route::get('cateversions/{id}/view' , 'CateversionsController@getById');


#### directory control
Route::get('directory' , 'DirectoryController@index');
Route::get('directory/item/{id?}' , 'DirectoryController@show');
Route::post('directory/item' , 'DirectoryController@store');
Route::post('directory/item/{id}' , 'DirectoryController@update');
Route::get('directory/{id}/delete' , 'DirectoryController@destroy');
Route::get('directory/{id}/view' , 'DirectoryController@getById');
#### directorycategories control
Route::get('directorycategories' , 'DirectorycategoriesController@index');
Route::get('directorycategories/item/{id?}' , 'DirectorycategoriesController@show');
Route::post('directorycategories/item' , 'DirectorycategoriesController@store');
Route::post('directorycategories/item/{id}' , 'DirectorycategoriesController@update');
Route::get('directorycategories/{id}/delete' , 'DirectorycategoriesController@destroy');
Route::get('directorycategories/{id}/view' , 'DirectorycategoriesController@getById');




#### webproject control
Route::get('webproject' , 'WebprojectController@index');
Route::get('webproject/item/{id?}' , 'WebprojectController@show');
Route::post('webproject/item' , 'WebprojectController@store');
Route::post('webproject/item/{id}' , 'WebprojectController@update');
Route::get('webproject/{id}/delete' , 'WebprojectController@destroy');
Route::get('webproject/{id}/view' , 'WebprojectController@getById');
#### centralcategories control
Route::get('centralcategories' , 'CentralcategoriesController@index');
Route::get('centralcategories/item/{id?}' , 'CentralcategoriesController@show');
Route::post('centralcategories/item' , 'CentralcategoriesController@store');
Route::post('centralcategories/item/{id}' , 'CentralcategoriesController@update');
Route::get('centralcategories/{id}/delete' , 'CentralcategoriesController@destroy');
Route::get('centralcategories/{id}/view' , 'CentralcategoriesController@getById');
#### newsletter control
Route::get('newsletter' , 'NewsletterController@index');
Route::get('newsletter/item/{id?}' , 'NewsletterController@show');
Route::post('newsletter/item' , 'NewsletterController@store');
Route::post('newsletter/item/{id}' , 'NewsletterController@update');
Route::get('newsletter/{id}/delete' , 'NewsletterController@destroy');
Route::get('newsletter/{id}/view' , 'NewsletterController@getById');
#### elibrary control
Route::get('elibrary' , 'ElibraryController@index');
Route::get('elibrary/item/{id?}' , 'ElibraryController@show');
Route::post('elibrary/item' , 'ElibraryController@store');
Route::post('elibrary/item/{id}' , 'ElibraryController@update');
Route::get('elibrary/{id}/delete' , 'ElibraryController@destroy');
Route::get('elibrary/{id}/view' , 'ElibraryController@getById');
#### fullnews control
Route::get('fullnews' , 'FullnewsController@index');
Route::get('fullnews/item/{id?}' , 'FullnewsController@show');
Route::post('fullnews/item' , 'FullnewsController@store');
Route::post('fullnews/item/{id}' , 'FullnewsController@update');
Route::get('fullnews/{id}/delete' , 'FullnewsController@destroy');
Route::get('fullnews/{id}/view' , 'FullnewsController@getById');
#### training control
Route::get('training' , 'TrainingController@index');
Route::get('training/item/{id?}' , 'TrainingController@show');
Route::post('training/item' , 'TrainingController@store');
Route::post('training/item/{id}' , 'TrainingController@update');
Route::get('training/{id}/delete' , 'TrainingController@destroy');
Route::get('training/{id}/view' , 'TrainingController@getById');
#### requestdata control
Route::get('requestdata' , 'RequestdataController@index');
Route::get('requestdata/item/{id?}' , 'RequestdataController@show');
Route::post('requestdata/item' , 'RequestdataController@store');
Route::post('requestdata/item/{id}' , 'RequestdataController@update');
Route::get('requestdata/{id}/delete' , 'RequestdataController@destroy');
Route::get('requestdata/{id}/view' , 'RequestdataController@getById');
#### servicesdetails control
Route::get('servicesdetails' , 'ServicesdetailsController@index');
Route::get('servicesdetails/item/{id?}' , 'ServicesdetailsController@show');
Route::post('servicesdetails/item' , 'ServicesdetailsController@store');
Route::post('servicesdetails/item/{id}' , 'ServicesdetailsController@update');
Route::get('servicesdetails/{id}/delete' , 'ServicesdetailsController@destroy');
Route::get('servicesdetails/{id}/view' , 'ServicesdetailsController@getById');
Route::get('votesystem' , 'VotesystemController@index');
Route::get('votesystem/item/{id?}' , 'VotesystemController@show');
Route::post('votesystem/item' , 'VotesystemController@store');
Route::post('votesystem/item/{id}' , 'VotesystemController@update');
Route::get('votesystem/{id}/delete' , 'VotesystemController@destroy');
Route::get('votesystem/{id}/view' , 'VotesystemController@getById');

#### surveyans control
Route::get('surveyans' , 'SurveyansController@index');
Route::get('surveyans/item/{id?}' , 'SurveyansController@show');
Route::post('surveyans/item' , 'SurveyansController@store');
Route::post('surveyans/item/{id}' , 'SurveyansController@update');
Route::get('surveyans/{id}/delete' , 'SurveyansController@destroy');
Route::get('surveyans/{id}/view' , 'SurveyansController@getById');

#### survey control
Route::get('survey' , 'SurveyController@index');
Route::get('survey/item/{id?}' , 'SurveyController@show');
Route::post('survey/item' , 'SurveyController@store');
Route::post('survey/item/{id}' , 'SurveyController@update');
Route::get('survey/{id}/delete' , 'SurveyController@destroy');
Route::get('survey/{id}/view' , 'SurveyController@getById');