<?php

#user
Route::post('users/login' , 'UserApi@login');
Route::get('users/getById/{id}', 'UserApi@getById');
Route::get('users/delete/{id}', 'UserApi@delete');
Route::post('users/add', 'UserApi@add');
Route::post('users/update', 'UserApi@update');
Route::get('users/{limit?}/{offset?}', 'UserApi@index');
Route::get('users/getUserByToken' , 'UserApi@getUserByToken');

#page
Route::get('page/getById/{id}/{lang?}', 'PageApi@getById');
Route::get('page/delete/{id}', 'PageApi@delete');
Route::post('page/add', 'PageApi@add');
Route::post('page/update/{id}', 'PageApi@update');
Route::get('page/{limit?}/{offset?}/{lang?}', 'PageApi@index');

#categorie
Route::get('categorie/getById/{id}/{lang?}', 'CategorieApi@getById');
Route::get('categorie/delete/{id}', 'CategorieApi@delete');
Route::post('categorie/add', 'CategorieApi@add');
Route::post('categorie/update/{id}', 'CategorieApi@update');
Route::get('categorie/{limit?}/{offset?}/{lang?}', 'CategorieApi@index');





























#webcategorie
Route::get('webcategorie/getById/{id}/{lang?}', 'WebcategorieApi@getById');
Route::get('webcategorie/delete/{id}', 'WebcategorieApi@delete');
Route::post('webcategorie/add', 'WebcategorieApi@add');
Route::post('webcategorie/update/{id}', 'WebcategorieApi@update');
Route::get('webcategorie/{limit?}/{offset?}/{lang?}', 'WebcategorieApi@index');

#webnews
Route::get('webnews/getById/{id}/{lang?}', 'WebnewsApi@getById');
Route::get('webnews/delete/{id}', 'WebnewsApi@delete');
Route::post('webnews/add', 'WebnewsApi@add');
Route::post('webnews/update/{id}', 'WebnewsApi@update');
Route::get('webnews/{limit?}/{offset?}/{lang?}', 'WebnewsApi@index');


#webslider
Route::get('webslider/getById/{id}/{lang?}', 'WebsliderApi@getById');
Route::get('webslider/delete/{id}', 'WebsliderApi@delete');
Route::post('webslider/add', 'WebsliderApi@add');
Route::post('webslider/update/{id}', 'WebsliderApi@update');
Route::get('webslider/{limit?}/{offset?}/{lang?}', 'WebsliderApi@index');

#webinfographic
Route::get('webinfographic/getById/{id}/{lang?}', 'WebinfographicApi@getById');
Route::get('webinfographic/delete/{id}', 'WebinfographicApi@delete');
Route::post('webinfographic/add', 'WebinfographicApi@add');
Route::post('webinfographic/update/{id}', 'WebinfographicApi@update');
Route::get('webinfographic/{limit?}/{offset?}/{lang?}', 'WebinfographicApi@index');





#webstate
Route::get('webstate/getById/{id}/{lang?}', 'WebstateApi@getById');
Route::get('webstate/delete/{id}', 'WebstateApi@delete');
Route::post('webstate/add', 'WebstateApi@add');
Route::post('webstate/update/{id}', 'WebstateApi@update');
Route::get('webstate/{limit?}/{offset?}/{lang?}', 'WebstateApi@index');

#aboutcentral
Route::get('aboutcentral/getById/{id}/{lang?}', 'AboutcentralApi@getById');
Route::get('aboutcentral/delete/{id}', 'AboutcentralApi@delete');
Route::post('aboutcentral/add', 'AboutcentralApi@add');
Route::post('aboutcentral/update/{id}', 'AboutcentralApi@update');
Route::get('aboutcentral/{limit?}/{offset?}/{lang?}', 'AboutcentralApi@index');

#webstatistics
Route::get('webstatistics/getById/{id}/{lang?}', 'WebstatisticsApi@getById');
Route::get('webstatistics/delete/{id}', 'WebstatisticsApi@delete');
Route::post('webstatistics/add', 'WebstatisticsApi@add');
Route::post('webstatistics/update/{id}', 'WebstatisticsApi@update');
Route::get('webstatistics/{limit?}/{offset?}/{lang?}', 'WebstatisticsApi@index');

#webstatisticsadd
Route::get('webstatisticsadd/getById/{id}/{lang?}', 'WebstatisticsaddApi@getById');
Route::get('webstatisticsadd/delete/{id}', 'WebstatisticsaddApi@delete');
Route::post('webstatisticsadd/add', 'WebstatisticsaddApi@add');
Route::post('webstatisticsadd/update/{id}', 'WebstatisticsaddApi@update');
Route::get('webstatisticsadd/{limit?}/{offset?}/{lang?}', 'WebstatisticsaddApi@index');

#versions
Route::get('versions/getById/{id}/{lang?}', 'VersionsApi@getById');
Route::get('versions/delete/{id}', 'VersionsApi@delete');
Route::post('versions/add', 'VersionsApi@add');
Route::post('versions/update/{id}', 'VersionsApi@update');
Route::get('versions/{limit?}/{offset?}/{lang?}', 'VersionsApi@index');

#cateversions
Route::get('cateversions/getById/{id}/{lang?}', 'CateversionsApi@getById');
Route::get('cateversions/delete/{id}', 'CateversionsApi@delete');
Route::post('cateversions/add', 'CateversionsApi@add');
Route::post('cateversions/update/{id}', 'CateversionsApi@update');
Route::get('cateversions/{limit?}/{offset?}/{lang?}', 'CateversionsApi@index');





#directory
Route::get('directory/getById/{id}/{lang?}', 'DirectoryApi@getById');
Route::get('directory/delete/{id}', 'DirectoryApi@delete');
Route::post('directory/add', 'DirectoryApi@add');
Route::post('directory/update/{id}', 'DirectoryApi@update');
Route::get('directory/{limit?}/{offset?}/{lang?}', 'DirectoryApi@index');

#directorycategories
Route::get('directorycategories/getById/{id}/{lang?}', 'DirectorycategoriesApi@getById');
Route::get('directorycategories/delete/{id}', 'DirectorycategoriesApi@delete');
Route::post('directorycategories/add', 'DirectorycategoriesApi@add');
Route::post('directorycategories/update/{id}', 'DirectorycategoriesApi@update');
Route::get('directorycategories/{limit?}/{offset?}/{lang?}', 'DirectorycategoriesApi@index');











#webproject
Route::get('webproject/getById/{id}/{lang?}', 'WebprojectApi@getById');
Route::get('webproject/delete/{id}', 'WebprojectApi@delete');
Route::post('webproject/add', 'WebprojectApi@add');
Route::post('webproject/update/{id}', 'WebprojectApi@update');
Route::get('webproject/{limit?}/{offset?}/{lang?}', 'WebprojectApi@index');

#centralcategories
Route::get('centralcategories/getById/{id}/{lang?}', 'CentralcategoriesApi@getById');
Route::get('centralcategories/delete/{id}', 'CentralcategoriesApi@delete');
Route::post('centralcategories/add', 'CentralcategoriesApi@add');
Route::post('centralcategories/update/{id}', 'CentralcategoriesApi@update');
Route::get('centralcategories/{limit?}/{offset?}/{lang?}', 'CentralcategoriesApi@index');

#newsletter
Route::get('newsletter/getById/{id}/{lang?}', 'NewsletterApi@getById');
Route::get('newsletter/delete/{id}', 'NewsletterApi@delete');
Route::post('newsletter/add', 'NewsletterApi@add');
Route::post('newsletter/update/{id}', 'NewsletterApi@update');
Route::get('newsletter/{limit?}/{offset?}/{lang?}', 'NewsletterApi@index');

#elibrary
Route::get('elibrary/getById/{id}/{lang?}', 'ElibraryApi@getById');
Route::get('elibrary/delete/{id}', 'ElibraryApi@delete');
Route::post('elibrary/add', 'ElibraryApi@add');
Route::post('elibrary/update/{id}', 'ElibraryApi@update');
Route::get('elibrary/{limit?}/{offset?}/{lang?}', 'ElibraryApi@index');

#fullnews
Route::get('fullnews/getById/{id}/{lang?}', 'FullnewsApi@getById');
Route::get('fullnews/delete/{id}', 'FullnewsApi@delete');
Route::post('fullnews/add', 'FullnewsApi@add');
Route::post('fullnews/update/{id}', 'FullnewsApi@update');
Route::get('fullnews/{limit?}/{offset?}/{lang?}', 'FullnewsApi@index');

#training
Route::get('training/getById/{id}/{lang?}', 'TrainingApi@getById');
Route::get('training/delete/{id}', 'TrainingApi@delete');
Route::post('training/add', 'TrainingApi@add');
Route::post('training/update/{id}', 'TrainingApi@update');
Route::get('training/{limit?}/{offset?}/{lang?}', 'TrainingApi@index');

#requestdata
Route::get('requestdata/getById/{id}/{lang?}', 'RequestdataApi@getById');
Route::get('requestdata/delete/{id}', 'RequestdataApi@delete');
Route::post('requestdata/add', 'RequestdataApi@add');
Route::post('requestdata/update/{id}', 'RequestdataApi@update');
Route::get('requestdata/{limit?}/{offset?}/{lang?}', 'RequestdataApi@index');

#servicesdetails
Route::get('servicesdetails/getById/{id}/{lang?}', 'ServicesdetailsApi@getById');
Route::get('servicesdetails/delete/{id}', 'ServicesdetailsApi@delete');
Route::post('servicesdetails/add', 'ServicesdetailsApi@add');
Route::post('servicesdetails/update/{id}', 'ServicesdetailsApi@update');
Route::get('servicesdetails/{limit?}/{offset?}/{lang?}', 'ServicesdetailsApi@index');

Route::get('votesystem/getById/{id}/{lang?}', 'VotesystemApi@getById');
Route::get('votesystem/delete/{id}', 'VotesystemApi@delete');
Route::post('votesystem/add', 'VotesystemApi@add');
Route::post('votesystem/update/{id}', 'VotesystemApi@update');
Route::get('votesystem/{limit?}/{offset?}/{lang?}', 'VotesystemApi@index');




Route::get('surveyans/getById/{id}/{lang?}', 'SurveyansApi@getById');
Route::get('surveyans/delete/{id}', 'SurveyansApi@delete');
Route::post('surveyans/add', 'SurveyansApi@add');
Route::post('surveyans/update/{id}', 'SurveyansApi@update');
Route::get('surveyans/{limit?}/{offset?}/{lang?}', 'SurveyansApi@index');



#survey
Route::get('survey/getById/{id}/{lang?}', 'SurveyApi@getById');
Route::get('survey/delete/{id}', 'SurveyApi@delete');
Route::post('survey/add', 'SurveyApi@add');
Route::post('survey/update/{id}', 'SurveyApi@update');
Route::get('survey/{limit?}/{offset?}/{lang?}', 'SurveyApi@index');