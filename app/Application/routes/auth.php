<?php
Route::get('/home', 'HomeController@index');

//
//#### webstate control
//Route::get('webstate' , 'WebstateController@index');
//Route::get('webstate/item/{id?}' , 'WebstateController@show');
//Route::get('webstate/{id}/view' , 'WebstateController@getById');
//
//#### aboutcentral control
//Route::get('aboutcentral' , 'AboutcentralController@index');
//Route::get('aboutcentral/item/{id?}' , 'AboutcentralController@show');
//Route::get('aboutcentral/{id}/view' , 'AboutcentralController@getById');
