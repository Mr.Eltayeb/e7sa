<?php



#### post comment
#### webcategorie control
Route::get('webcategorie' , 'WebcategorieController@index');
Route::get('webcategorie/item/{id?}' , 'WebcategorieController@show');
Route::post('webcategorie/item' , 'WebcategorieController@store');
Route::post('webcategorie/item/{id}' , 'WebcategorieController@update');
Route::get('webcategorie/{id}/delete' , 'WebcategorieController@destroy');
Route::get('webcategorie/{id}/view' , 'WebcategorieController@getById');

#### webnews control
Route::get('webnews' , 'WebnewsController@index');
Route::get('webnews/item/{id?}' , 'WebnewsController@show');
Route::post('webnews/item' , 'WebnewsController@store');
Route::post('webnews/item/{id}' , 'WebnewsController@update');
Route::get('webnews/{id}/delete' , 'WebnewsController@destroy');
Route::get('webnews/{id}/view' , 'WebnewsController@getById');


#### webslider control
Route::get('webslider' , 'WebsliderController@index');
Route::get('webslider/item/{id?}' , 'WebsliderController@show');
Route::post('webslider/item' , 'WebsliderController@store');
Route::post('webslider/item/{id}' , 'WebsliderController@update');
Route::get('webslider/{id}/delete' , 'WebsliderController@destroy');
Route::get('webslider/{id}/view' , 'WebsliderController@getById');

#### webinfographic control
Route::get('webinfographic' , 'WebinfographicController@index');
Route::get('webinfographic/item/{id?}' , 'WebinfographicController@show');
Route::post('webinfographic/item' , 'WebinfographicController@store');
Route::post('webinfographic/item/{id}' , 'WebinfographicController@update');
Route::get('webinfographic/{id}/delete' , 'WebinfographicController@destroy');
Route::get('webinfographic/{id}/view' , 'WebinfographicController@getById');





#### webstate control
Route::get('webstate' , 'WebstateController@index');
Route::get('webstate/item/{id?}' , 'WebstateController@show');
Route::post('webstate/item' , 'WebstateController@store');
Route::post('webstate/item/{id}' , 'WebstateController@update');
Route::get('webstate/{id}/delete' , 'WebstateController@destroy');
Route::get('webstate/{id}/view' , 'WebstateController@getById');

#### aboutcentral control
Route::get('aboutcentral' , 'AboutcentralController@index');
Route::get('aboutcentral/item/{id?}' , 'AboutcentralController@show');
Route::post('aboutcentral/item' , 'AboutcentralController@store');
Route::post('aboutcentral/item/{id}' , 'AboutcentralController@update');
Route::get('aboutcentral/{id}/delete' , 'AboutcentralController@destroy');
Route::get('aboutcentral/{id}/view' , 'AboutcentralController@getById');

#### webstatistics control
Route::get('webstatistics' , 'WebstatisticsController@index');
Route::get('webstatistics/item/{id?}' , 'WebstatisticsController@show');
Route::post('webstatistics/item' , 'WebstatisticsController@store');
Route::post('webstatistics/item/{id}' , 'WebstatisticsController@update');
Route::get('webstatistics/{id}/delete' , 'WebstatisticsController@destroy');
Route::get('webstatistics/{id}/view' , 'WebstatisticsController@getById');

#### webstatisticsadd control
Route::get('webstatisticsadd' , 'WebstatisticsaddController@index');
Route::get('webstatisticsadd/item/{id?}' , 'WebstatisticsaddController@show');
Route::post('webstatisticsadd/item' , 'WebstatisticsaddController@store');
Route::post('webstatisticsadd/item/{id}' , 'WebstatisticsaddController@update');
Route::get('webstatisticsadd/{id}/delete' , 'WebstatisticsaddController@destroy');
Route::get('webstatisticsadd/{id}/view' , 'WebstatisticsaddController@getById');

#### versions control
Route::get('versions' , 'VersionsController@index');
Route::get('versions/item/{id?}' , 'VersionsController@show');
Route::post('versions/item' , 'VersionsController@store');
Route::post('versions/item/{id}' , 'VersionsController@update');
Route::get('versions/{id}/delete' , 'VersionsController@destroy');
Route::get('versions/{id}/view' , 'VersionsController@getById');

#### cateversions control
Route::get('cateversions' , 'CateversionsController@index');
Route::get('cateversions/item/{id?}' , 'CateversionsController@show');
Route::post('cateversions/item' , 'CateversionsController@store');
Route::post('cateversions/item/{id}' , 'CateversionsController@update');
Route::get('cateversions/{id}/delete' , 'CateversionsController@destroy');
Route::get('cateversions/{id}/view' , 'CateversionsController@getById');





#### directory control
Route::get('directory' , 'DirectoryController@index');
Route::get('directory/item/{id?}' , 'DirectoryController@show');
Route::post('directory/item' , 'DirectoryController@store');
Route::post('directory/item/{id}' , 'DirectoryController@update');
Route::get('directory/{id}/delete' , 'DirectoryController@destroy');
Route::get('directory/{id}/view' , 'DirectoryController@getById');

#### directorycategories control
Route::get('directorycategories' , 'DirectorycategoriesController@index');
Route::get('directorycategories/item/{id?}' , 'DirectorycategoriesController@show');
Route::post('directorycategories/item' , 'DirectorycategoriesController@store');
Route::post('directorycategories/item/{id}' , 'DirectorycategoriesController@update');
Route::get('directorycategories/{id}/delete' , 'DirectorycategoriesController@destroy');
Route::get('directorycategories/{id}/view' , 'DirectorycategoriesController@getById');











#### webproject control
Route::get('webproject' , 'WebprojectController@index');
Route::get('webproject/item/{id?}' , 'WebprojectController@show');
Route::post('webproject/item' , 'WebprojectController@store');
Route::post('webproject/item/{id}' , 'WebprojectController@update');
Route::get('webproject/{id}/delete' , 'WebprojectController@destroy');
Route::get('webproject/{id}/view' , 'WebprojectController@getById');

#### centralcategories control
Route::get('centralcategories' , 'CentralcategoriesController@index');
Route::get('centralcategories/item/{id?}' , 'CentralcategoriesController@show');
Route::post('centralcategories/item' , 'CentralcategoriesController@store');
Route::post('centralcategories/item/{id}' , 'CentralcategoriesController@update');
Route::get('centralcategories/{id}/delete' , 'CentralcategoriesController@destroy');
Route::get('centralcategories/{id}/view' , 'CentralcategoriesController@getById');

#### newsletter control
Route::get('newsletter' , 'NewsletterController@index');
Route::get('newsletter/item/{id?}' , 'NewsletterController@show');
Route::post('newsletter/item' , 'NewsletterController@store');
Route::post('newsletter/item/{id}' , 'NewsletterController@update');
Route::get('newsletter/{id}/delete' , 'NewsletterController@destroy');
Route::get('newsletter/{id}/view' , 'NewsletterController@getById');

#### elibrary control
Route::get('elibrary' , 'ElibraryController@index');
Route::get('elibrary/item/{id?}' , 'ElibraryController@show');
Route::post('elibrary/item' , 'ElibraryController@store');
Route::post('elibrary/item/{id}' , 'ElibraryController@update');
Route::get('elibrary/{id}/delete' , 'ElibraryController@destroy');
Route::get('elibrary/{id}/view' , 'ElibraryController@getById');

#### fullnews control
Route::get('fullnews' , 'FullnewsController@index');
Route::get('fullnews/item/{id?}' , 'FullnewsController@show');
Route::post('fullnews/item' , 'FullnewsController@store');
Route::post('fullnews/item/{id}' , 'FullnewsController@update');
Route::get('fullnews/{id}/delete' , 'FullnewsController@destroy');
Route::get('fullnews/{id}/view' , 'FullnewsController@getById');



#### training control
Route::get('training' , 'TrainingController@index');
Route::get('training/item/{id?}' , 'TrainingController@show');
Route::post('training/item' , 'TrainingController@store');
Route::post('training/item/{id}' , 'TrainingController@update');
Route::get('training/{id}/delete' , 'TrainingController@destroy');
Route::get('training/{id}/view' , 'TrainingController@getById');

#### requestdata control
Route::get('requestdata' , 'RequestdataController@index');
Route::get('requestdata/item/{id?}' , 'RequestdataController@show');
Route::post('requestdata/item' , 'RequestdataController@store');
Route::post('requestdata/item/{id}' , 'RequestdataController@update');
Route::get('requestdata/{id}/delete' , 'RequestdataController@destroy');
Route::get('requestdata/{id}/view' , 'RequestdataController@getById');

#### servicesdetails control
Route::get('servicesdetails' , 'ServicesdetailsController@index');
Route::get('servicesdetails/item/{id?}' , 'ServicesdetailsController@show');
Route::post('servicesdetails/item' , 'ServicesdetailsController@store');
Route::post('servicesdetails/item/{id}' , 'ServicesdetailsController@update');
Route::get('servicesdetails/{id}/delete' , 'ServicesdetailsController@destroy');
Route::get('servicesdetails/{id}/view' , 'ServicesdetailsController@getById');

Route::get('votesystem' , 'VotesystemController@index');
Route::get('votesystem/item/{id?}' , 'VotesystemController@show');
Route::post('votesystem/item' , 'VotesystemController@store');
Route::post('votesystem/item/{id}' , 'VotesystemController@update');
Route::get('votesystem/{id}/delete' , 'VotesystemController@destroy');
Route::get('votesystem/{id}/view' , 'VotesystemController@getById');




#### surveyans control
Route::get('surveyans' , 'SurveyansController@index');
Route::get('surveyans/item/{id?}' , 'SurveyansController@show');
Route::post('surveyans/item' , 'SurveyansController@store');
Route::post('surveyans/item/{id}' , 'SurveyansController@update');
Route::get('surveyans/{id}/delete' , 'SurveyansController@destroy');
Route::get('surveyans/{id}/view' , 'SurveyansController@getById');



#### survey control
Route::get('survey' , 'SurveyController@index');
Route::get('survey/item/{id?}' , 'SurveyController@show');
Route::post('survey/item' , 'SurveyController@store');
Route::post('survey/item/{id}' , 'SurveyController@update');
Route::get('survey/{id}/delete' , 'SurveyController@destroy');
Route::get('survey/{id}/view' , 'SurveyController@getById');