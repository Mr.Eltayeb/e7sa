<?php
	return [
		'training'=>'training',
		'name'=>'Full Name',
		'university'=>'The University',
		'college'=>'The college',
		'specialty'=>'Specialty',
		'phone'=>'Phone',
		'email'=>'Email Address',
		'type_training'=>'Type training',
		'departments'=>'The Departments',
		'competence'=>'Competence',
		'edit'=>'edit',
		'show'=>'show',
		'delete'=>'delete',
		'type_college'=>'Type of college',
		'request_status'=>'request status',
	];
