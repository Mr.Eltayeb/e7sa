<?php
	return [
		'webstatisticsadd'=>'احصائية',
		'title'=>'العنوان',
		'file_ar'=>'عربي',
		'file_en'=>'انجليزي',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
		'body'=>'النص',
	];
