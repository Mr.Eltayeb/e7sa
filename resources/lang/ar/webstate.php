<?php
	return [
		'webstate'=>'الولايات',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'body'=>'النص',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
		'file'=>'الملفات',
	];
