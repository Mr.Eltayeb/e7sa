<?php
	return [
		'survey'=>'استطلاع',
		'title'=>'العنوان',
		'answers1'=>'الخيار الاول',
		'answers2'=>'الخيار الثاني',
		'answers3'=>'الخيار الثالث',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
