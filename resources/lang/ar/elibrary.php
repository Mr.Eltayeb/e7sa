<?php
	return [
		'elibrary'=>'المكتبة الإليكترونية',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'file'=>'عربي',
		'file_en'=>'انجليزي',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
