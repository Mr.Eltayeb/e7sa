<?php
	return [
		'centralcategories'=>'التصانيف',
		'title'=>'العنوان',
		'file'=>'عربي',
		'file_en'=>'انجليزي',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
		'body'=>'النص',
	];
