<?php
	return [
		'webinfographic'=>'رسومات توضيحية',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
