<?php
	return [
		'webnews'=>'الاخبار',
		'title'=>'العنوان',
		'url'=>'رابط الموضوع',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
