<?php
	return [
		'versions'=>'الاصدارات',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'file'=>'عربي',
		'file_en'=>'انجليزي',
		'year'=>'السنة',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
		'body'=>'النص',
	];
