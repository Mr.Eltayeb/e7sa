<?php
	return [
		'fullnews'=>'أخبار',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'body'=>'النص',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
