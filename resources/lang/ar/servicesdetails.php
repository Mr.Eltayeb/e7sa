<?php
	return [
		'servicesdetails'=>'الخدمات',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'body'=>'النص',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
		'url'=>'رابط طلب الخدمة',
	];
