<?php
	return [
		'webslider'=>'شريط متحرك',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'url'=>'رابط الموضوع',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
