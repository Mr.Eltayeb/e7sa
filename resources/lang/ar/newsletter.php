<?php
	return [
		'newsletter'=>'النشرة الاستراتجية',
		'title'=>'العنوان',
		'image'=>'الصورة',
		'file'=>'عربي',
		'file_en'=>'انجليزي',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
	];
