<?php
	return [
		'training'=>'تدريب',
		'name'=>'الاسم',
		'university'=>'الجامعة',
		'college'=>'الكلية',
		'specialty'=>'التخصص',
		'phone'=>'رقم الهاتف',
		'email'=>'البريد الالكتروني',
		'type_training'=>'نوع التدريب',
		'departments'=>'الاداراة التي يتدرب فيها',
		'competence'=>'تخصص معين ترغب بالتدرب فيه',
		'edit'=>'تعديل',
		'show'=>'عرض',
		'delete'=>'حذف',
		'type_college'=>'نوع الجامعة',
		'request_status'=>'حالة الطلب',
	];
