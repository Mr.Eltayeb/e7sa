# ZoolFlat
Advanced laravel system to build admin panel




## Requirements

Laravel => 5.5 , <br>
PHP >= 7 ,   <br>
PHP Curl extension   <br>


## Download The Files 

download all files



## Install  The Dependencies

now type this line on your console

```
composer install
```
```
php artisan key:generate
```




## Migrate

```
  php artisan migrate
```

## Seed Database 

```
  php artisan db:seed
```

## Start Server


```
  php artisan serve
```

## Login

```
  email : admin@gmail.com
  pass : admin
```

## Go To Admin Path

```
  http://127.0.0.1:8000/admin/home
```
