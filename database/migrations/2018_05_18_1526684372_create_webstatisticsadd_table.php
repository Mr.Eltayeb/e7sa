<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebstatisticsaddTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webstatisticsadd', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title")->nullable();
			$table->string("file_ar")->nullable();
			$table->string("file_en")->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webstatisticsadd');
    }
}
