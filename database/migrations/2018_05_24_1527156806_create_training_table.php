<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->nullable();
			$table->string("university")->nullable();
			$table->string("college")->nullable();
			$table->string("specialty")->nullable();
			$table->string("phone")->nullable();
			$table->string("email")->nullable();
			$table->string("type_training")->nullable();
			$table->string("departments")->nullable();
			$table->text("competence")->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training');
    }
}
