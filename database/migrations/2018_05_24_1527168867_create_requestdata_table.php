<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestdataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requestdata', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->nullable();
			$table->string("type_identity")->nullable();
			$table->string("Identity")->nullable();
			$table->string("occupation")->nullable();
			$table->string("qualification")->nullable();
			$table->string("phone")->nullable();
			$table->string("fax")->nullable();
			$table->string("email")->nullable();
			$table->string("data_usage")->nullable();
			$table->string("company_name")->nullable();
			$table->string("type_Company")->nullable();
			$table->string("Data_usage_area")->nullable();
			$table->text("data_details")->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requestdata');
    }
}
